//
//  BillingRideAddressCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 26/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class BillingRideAddressCell: UITableViewCell {
    @IBOutlet weak var pickUpAddressLabel: UILabel!
    @IBOutlet weak var dropAddressLabel: UILabel!
    @IBOutlet weak var dotedImageView: UIImageView!
    @IBOutlet weak var separatorImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        self.dotedImageView.image = UIImage.drawDottedImage(width: 3, height: dotedImageView.frame.size.height, color: UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 1.0))
        self.separatorImage.image = UIImage.drawDottedSeparatorImage(width: separatorImage.frame.size.width, height: 1, color: .lightGray)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
