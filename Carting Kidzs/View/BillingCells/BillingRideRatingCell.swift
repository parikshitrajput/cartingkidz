//
//  BillingRideRatingCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 26/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
protocol BillingRideRatingCellDelegate {
    func cell(cell: BillingRideRatingCell, didRate rating: Double)
}
class BillingRideRatingCell: UITableViewCell {
    var rating:Int = 3{
        didSet{
            self.setRating(rating: self.rating)
        }
    }

    var delegate: BillingRideRatingCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    @IBAction func onClickRating(_ sender:UIButton){
        let rating = sender.tag
        for index in 1...5{
            guard let button = self.viewWithTag(index) as? UIButton else{return}
            button.isSelected = (rating >= button.tag)
        }
        delegate?.cell(cell: self, didRate: Double(sender.tag))
    }

   private func setRating(rating:Int){
        var rate = (rating < 1) ? 1 : rating
        rate = (rating > 5) ? 5 : rating
        for index in 1...5{
            guard let button = self.viewWithTag(index) as? UIButton else{return}
            button.isSelected = (rate >= button.tag)
        }
    }
}
