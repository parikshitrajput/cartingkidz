//
//  BillingDriverDetailCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 26/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class BillingDriverDetailCell: UITableViewCell {
    @IBOutlet weak var driverIcon: UIImageView!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var separatorImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(driverIcon, borderColor: appColor.green, borderWidth: 1, cornerRadius: driverIcon.frame.size.width/6)
        self.separatorImage.image = UIImage.drawDottedSeparatorImage(width: separatorImage.frame.size.width, height: 1, color: .lightGray)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
