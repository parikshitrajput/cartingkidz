//
//  CardTableViewCell.swift
//  GPDock
//
//  Created by TecOrb on 28/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CardTableViewCell: UITableViewCell {
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var fundingTypeLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var defaultLabel: UILabel!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class SelectedCardTableViewCell: UITableViewCell {
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var fundingTypeLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var defaultLabel: UILabel!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        self.doneButton.applyGradient(withColours: [appColor.red,appColor.red], gradientOrientation: .horizontal, locations: [0.0,1.0])
        CommonClass.makeViewCircularWithCornerRadius(self.doneButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 2)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


class NewCardTableViewCell: UITableViewCell {
//    @IBOutlet weak var cardImageView: UIImage!
//    @IBOutlet weak var cardNumberLabel: UILabel!
//    @IBOutlet weak var fundingTypeLabel: UILabel!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}



class PayableAmountDescriptionCell: UITableViewCell {
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var payableTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

