//
//  GenderCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 16/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
enum Gender:String{
    case male = "male"
    case female = "female"

    init(rawValue:String){
        if rawValue == "male"{
            self = .male
        }else{
            self = .female
        }
    }

}

protocol GenderCellDelegate {
    func gender(genderCell cell: GenderCell, didSelectGender gender: Gender)
}

class GenderCell: UITableViewCell {
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var containner: UIView!
    var selectedGender : Gender = .male{
        didSet{
            if selectedGender == .male{
                self.maleButton.isSelected = true
                self.femaleButton.isSelected = false
            }else{
                self.maleButton.isSelected = false
                self.femaleButton.isSelected = true
            }
        }
    }

    var delegate: GenderCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let selected = UIImage(named: "radio_sel")
        let unselected = UIImage(named: "radio_unsel")

        self.maleButton.setImage(selected, for: .selected)
        self.femaleButton.setImage(selected, for: .selected)
        self.maleButton.setImage(unselected, for: .normal)
        self.femaleButton.setImage(unselected, for: .normal)
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)
    }

    @IBAction func onClickMale(_ sender: UIButton){
        self.selectedGender = .male
//        maleButton.isSelected = true
//        femaleButton.isSelected = false
        self.delegate?.gender(genderCell: self, didSelectGender: .male)
    }

    @IBAction func onClickFemale(_ sender: UIButton){
        self.selectedGender = .female
//        maleButton.isSelected = false
//        femaleButton.isSelected = true
        self.delegate?.gender(genderCell: self, didSelectGender: .female)
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
