//
//  AlergiesCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 16/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    @IBOutlet weak var popUpButton: UIButton!
    @IBOutlet weak var fragmentButton: UIButton!
    @IBOutlet weak var newScreenButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}


class AlergiesCell: UITableViewCell {
    @IBOutlet weak var textView: FloatLabelTextView!
    @IBOutlet weak var containner: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.textView.titleFont = fonts.OpenSans.regular.font(.small)
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
