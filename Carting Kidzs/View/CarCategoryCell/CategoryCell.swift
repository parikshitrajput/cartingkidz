//
//  CategoryCell.swift
//  TaxiApp
//
//  Created by TecOrb on 13/05/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    @IBOutlet weak var categoryNameLabel:UILabel!
    @IBOutlet weak var timeLabel:UILabel!
    @IBOutlet weak var seatsLabel:UILabel!
    @IBOutlet weak var categoryIcon:UIImageView!
    @IBOutlet weak var separatorView:UIView!
    var shouldShowSeparator:Bool = false
    override var bounds: CGRect{
        didSet{
            self.contentView.frame = bounds
        }
    }

    override func layoutSubviews() {
        self.separatorView.isHidden = !self.shouldShowSeparator
    }

}




