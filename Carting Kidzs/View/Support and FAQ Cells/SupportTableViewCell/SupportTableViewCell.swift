//
//  SupportTableViewCell.swift
//  GPDock
//
//  Created by TecOrb on 07/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class SupportTableViewCell: UITableViewCell {
    @IBOutlet weak var questionTextLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}



class SupportUserNameTableViewCell: UITableViewCell {
    @IBOutlet weak var userNameLabel : UILabel!
    //@IBOutlet weak var separatorView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}


class SupportNotListedTableViewCell: UITableViewCell {
    @IBOutlet weak var reachUsButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
//        CommonClass.makeViewCircularWithCornerRadius(self.reachUsButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 3)
//        self.reachUsButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}


