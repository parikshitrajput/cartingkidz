//
//  GeneralSettingCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 31/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class GeneralSettingCell: UITableViewCell {
    @IBOutlet weak var settingNameLabel:UILabel!
    @IBOutlet weak var settingIcon:UIImageView!
    @IBOutlet weak var accIcon:UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
