//
//  CancelReasonCell.swift
//  TaxiApp
//
//  Created by TecOrb on 16/05/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CancelReasonCell: UITableViewCell {
    @IBOutlet weak var reasonLabel : UILabel!
    @IBOutlet weak var button : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        button.setImage(#imageLiteral(resourceName: "radio_unsel"), for: .normal)
        button.setImage(#imageLiteral(resourceName: "radio_sel"), for: .selected)

    }
    
    override func prepareForReuse() {
        reasonLabel.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
