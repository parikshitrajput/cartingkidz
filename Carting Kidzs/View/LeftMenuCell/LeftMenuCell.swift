//
//  LeftMenuCell.swift
//  MyLaundryApp
//
//  Created by TecOrb on 15/12/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
class LeftMenuCell: UITableViewCell {
    @IBOutlet weak var cellIcon :UIImageView!
    @IBOutlet weak var nameLabel :UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class MenuProfileCell: UITableViewCell {
    @IBOutlet weak var profileIcon :UIImageView!
    @IBOutlet weak var nameLabel :UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircular(self.profileIcon, borderColor: appColor.green , borderWidth: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
