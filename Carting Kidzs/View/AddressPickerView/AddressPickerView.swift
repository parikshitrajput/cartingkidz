//
//  AddressPickerView.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 06/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import DGActivityIndicatorView
enum AddressType:String{
    case from = "greenPin"
    case to = "redPin"
}

let rippleGreen = UIColor(red: 133.0/255.0, green: 194.0/255.0, blue: 38.0/255.0, alpha: 1.0)
let rippleRed = UIColor(red: 221.0/255.0, green: 18.0/255.0, blue: 123.0/255.0, alpha: 1.0)

protocol AddressPickerViewDelegate {
    func addressPickerView(addressPickerView: AddressPickerView, didTapOnAddressType addressType: AddressType)
}

class AddressPickerView: UIView {
    @IBOutlet weak var fromAddressLabel: UILabel!
    @IBOutlet weak var toAddressLabel: UILabel!
    @IBOutlet weak var dotedImageView: UIImageView!
    @IBOutlet weak var fromDotImage: UIImageView!
    @IBOutlet weak var toDotImage: UIImageView!
    @IBOutlet weak var fromAddressButton: UIButton!
    @IBOutlet weak var toAddressButton: UIButton!

    var fromRippleView : DGActivityIndicatorView!
    var toRippleView : DGActivityIndicatorView!

    var delegate: AddressPickerViewDelegate?
    var selectedAddressType:AddressType = .from

    override func layoutSubviews() {
        self.dotedImageView.image = UIImage.drawDottedImage(width: 3, height: dotedImageView.frame.size.height, color: UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 1.0))
        self.setupRippleView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.initializeRippleViews()
    }


    //MARK: - Methods to setup ripple effect on address type
    func initializeRippleViews(){
        fromRippleView = DGActivityIndicatorView(type: .ballScaleRippleMultiple, tintColor: rippleGreen, size: self.fromDotImage.frame.size.width*6/4)
        self.addSubview(fromRippleView)
        fromRippleView.center = fromDotImage.center
        self.bringSubview(toFront: fromRippleView)

        toRippleView = DGActivityIndicatorView(type: .ballScaleRippleMultiple, tintColor: rippleRed, size: self.toDotImage.frame.size.width*6/4)
        self.addSubview(toRippleView)
        toRippleView.center = fromDotImage.center
        self.bringSubview(toFront: toRippleView)
        self.perform(#selector(setupRippleView), with: nil, afterDelay: 0.8)
    }

    @objc func setupRippleView(){
        if self.selectedAddressType == .from{
            self.setUpRippleOnFromDot()
        }else{
            self.setUpRippleOnToDot()
        }
    }


    func stopAnimatingRipples(){
        self.stopAnimatingFromRipples()
        self.stopAnimatingToRipples()
    }


    func stopAnimatingFromRipples(){
        guard let fromRipple = self.fromRippleView else {return}
        fromRipple.stopAnimating()
    }
    
    func stopAnimatingToRipples(){
        guard let toRipple = self.toRippleView else {return}
        toRipple.stopAnimating()
    }

    func setUpRippleOnFromDot(){
        guard let fromRipple = self.fromRippleView else {return}
        fromRipple.center = fromDotImage.center
        fromRipple.startAnimating()
        self.bringSubview(toFront: fromRipple)

        guard let toRipple = self.toRippleView else {return}
        toRipple.stopAnimating()
        self.sendSubview(toBack: toRipple)
    }


    func setUpRippleOnToDot(){
        guard let toRipple = self.toRippleView else {return}
        toRipple.center = toDotImage.center
        toRipple.startAnimating()
        self.bringSubview(toFront: toRipple)

        guard let fromRipple = self.fromRippleView else {return}
        fromRipple.stopAnimating()
        self.sendSubview(toBack: fromRipple)
    }


    @IBAction func onClickFromAddressButton(_ sender: UIButton){
        self.selectedAddressType = .from
        self.setUpRippleOnFromDot()
        self.delegate?.addressPickerView(addressPickerView: self, didTapOnAddressType: .from)
    }

    @IBAction func onClickToAddressButton(_ sender: UIButton){
        self.selectedAddressType = .to
        self.setUpRippleOnToDot()
        self.delegate?.addressPickerView(addressPickerView: self, didTapOnAddressType: .to)
    }

}
