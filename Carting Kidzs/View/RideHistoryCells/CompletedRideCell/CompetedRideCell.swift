//
//  CompetedRideCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 21/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
class CompletedRideCell: UITableViewCell {
    @IBOutlet weak var driverIcon: UIImageView!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var carCategoryLabel: UILabel!
    @IBOutlet weak var crnLabel: UILabel!
    @IBOutlet weak var pickUpAddressLabel: UILabel!
    @IBOutlet weak var dropAddressLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircular(driverIcon, borderColor: appColor.green, borderWidth: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
