//
//  NoDataAndReloadCell.swift
//  GPDock
//
//  Created by TecOrb on 08/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class NoDataCell: UITableViewCell {
    @IBOutlet weak var messageLabel: UILabel!
    //@IBOutlet weak var reloadButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
