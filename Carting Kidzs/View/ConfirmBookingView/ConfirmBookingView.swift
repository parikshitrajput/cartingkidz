//
//  ConfirmBookingView.swift
//  Carting Kidzs
//
//  Created by Parikshit on 16/12/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class ConfirmBookingView: UIView {
    
    @IBOutlet weak var totalFareLabel : UILabel!
    
    @IBOutlet weak var enterDropOffButton : UIButton!
    @IBOutlet weak var showFareBreakUpButton : UIButton!

    @IBOutlet weak var enterDropOffLabel : UILabel!
    @IBOutlet weak var confirmButton : UIButton!


    class func instanceFromNib() -> ConfirmBookingView {
        return UINib(nibName: "ConfirmBookingView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ConfirmBookingView
    }
    
}
