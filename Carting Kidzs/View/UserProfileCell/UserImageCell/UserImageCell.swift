//
//  UserImageCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 07/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class UserImageCell: UITableViewCell {
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        CommonClass.makeViewCircular(self.userImageView, borderColor: appColor.green, borderWidth: 1)
    }
    
    override func layoutSubviews() {
        CommonClass.makeViewCircular(self.userImageView, borderColor: appColor.green, borderWidth: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
