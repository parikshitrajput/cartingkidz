//
//  RideMapCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 30/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class RideMapCell: UITableViewCell {
    @IBOutlet weak var mapImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
