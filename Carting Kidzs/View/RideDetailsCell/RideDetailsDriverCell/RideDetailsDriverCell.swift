//
//  RideDetailsDriverCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 24/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class RideDetailsDriverCell: UITableViewCell {
    @IBOutlet weak var driverIcon: UIImageView!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var ratingView: NKFloatRatingView!
    @IBOutlet weak var cancelledLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircular(self.driverIcon, borderColor: appColor.green, borderWidth: 1)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
