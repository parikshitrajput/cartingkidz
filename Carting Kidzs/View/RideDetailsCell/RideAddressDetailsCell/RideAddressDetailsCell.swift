//
//  RideAddressDetailsCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 30/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class RideAddressDetailsCell: UITableViewCell {
    @IBOutlet weak var pickUpTimeLabel: UILabel!
    @IBOutlet weak var dropOffTimeLabel: UILabel!
    @IBOutlet weak var pickUpAddressLabel: UILabel!
    @IBOutlet weak var dropOffAddressLabel: UILabel!
    @IBOutlet weak var dotedImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        self.dotedImageView.image = UIImage.drawDottedImage(width: 3, height: dotedImageView.frame.size.height, color: UIColor.darkGray)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
