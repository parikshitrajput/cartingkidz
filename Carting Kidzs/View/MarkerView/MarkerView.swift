//
//  MarkerView.swift
//  GPDock
//
//  Created by TecOrb on 08/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class MarkerView: UIView {
    @IBOutlet weak var markerImage: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!

    class func instanceFromNib() -> MarkerView {
        return UINib(nibName: "MarkerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MarkerView
    }
    override func layoutSubviews() {
        self.priceLabel.adjustsFontSizeToFitWidth = true
    }


}
