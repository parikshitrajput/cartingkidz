//
//  OTPVerificationViewController.swift
//  TipNTap
//
//  Created by TecOrb on 29/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Firebase

enum appAction {
    case forgotPassword,login,signUp
}

class OTPVerificationViewController: UIViewController {
    @IBOutlet weak var otpView: VPMOTPView!
    @IBOutlet weak var donebutton: UIButton!
    @IBOutlet weak var resendbutton: UIButton!
    @IBOutlet weak var sentToLabel: UILabel!
   // @IBOutlet weak var didNotReceivedCodeLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!

    var verificationID : String!
    var otpString :String = ""
    var fullName:String?
    var contact:String!
    var countryCode:String!
    var password:String?
    var userAccessToken: String?
    var appAction:appAction!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupOTPView()
        self.resendbutton.isEnabled = true
        //self.didNotReceivedCodeLabel.alpha = 1.0
        self.resendbutton.alpha = 1.0

        self.sentToLabel.text = " (\(self.countryCode+self.contact))"

        self.toggleResend()
        self.perform(#selector(toggleResend), with: nil, afterDelay: 60)
    }

    func setupOTPView(){
        self.otpView.otpFieldFont = fonts.OpenSans.semiBold.font(.xXXLarge)
        otpView.otpFieldSize = self.otpView.frame.size.width * 1/6
        otpView.otpFieldSeparatorSpace = 10
        otpView.otpFieldsCount = 6
        otpView.otpFieldDefaultBorderColor = .darkGray
        otpView.otpFieldEnteredBorderColor = .darkGray
        otpView.otpFieldBorderWidth = 2
        otpView.shouldAllowIntermediateEditing = false
        otpView.otpFieldDisplayType = .square
        otpView.otpFieldDefaultBackgroundColor = UIColor.color(r: 20, g: 20, b: 20)
        otpView.otpFieldEnteredBackgroundColor = UIColor.color(r: 20, g: 20, b: 20)
        otpView.otpFieldTextColor = UIColor.white
        otpView.cursorColor = .darkGray
        otpView.delegate = self
        otpView.initalizeUI()
        CommonClass.makeViewCircularWithCornerRadius(self.submitButton, borderColor: .clear, borderWidth: 0, cornerRadius: 2)
    }

    @objc func toggleResend(){
        self.resendbutton.isEnabled = !self.resendbutton.isEnabled
       // self.didNotReceivedCodeLabel.alpha = (self.didNotReceivedCodeLabel.alpha == 1.0) ? 0.2 : 1.0
        self.resendbutton.alpha = (self.resendbutton.alpha == 1.0) ? 0.2 : 1.0
    }

    override func viewDidLayoutSubviews() {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickResend(_ sender: UIButton){
        self.otpView.initalizeUI()
        self.sendOTPOn(countryCode: self.countryCode, phoneNumber: self.contact)
    }


    func sendOTPOn(countryCode:String,phoneNumber:String){
        AppSettings.shared.showLoader(withStatus: "Resending..")
        self.otpString = ""
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: signOutError.localizedDescription, completionBlock: {
                self.navigationController?.pop(true)
            })
            return
        }
        let fullPhoneNumber = countryCode+phoneNumber
        PhoneAuthProvider.provider().verifyPhoneNumber(fullPhoneNumber, uiDelegate: nil) { (verificationID, error) in
            AppSettings.shared.hideLoader()
            if let error = error {
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {
                    self.navigationController?.pop(true)
                })
                return
            }

            self.toggleResend()
            self.perform(#selector(self.toggleResend), with: nil, afterDelay: 60)
            guard let verificationToken = verificationID else{
                return
            }
            self.verificationID = verificationToken
        }
    }


    @IBAction func onClickDoneButton(_ sender: UIButton){
        NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter 6 digit code")
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    func verifyOTP(otp:String,authID:String){
        if self.appAction == .signUp{
            guard let name = self.fullName else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter your full name")
                return
            }
            guard let myPassword = self.password else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter your password")
                return
            }
            AppSettings.shared.showLoader(withStatus: "Validating..")
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: authID,
                verificationCode: otp)

            Auth.auth().signIn(with: credential) { (user, error) in
                if let error = error {
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {
                        self.otpView.initalizeUI()
                    })
                    return
                }else{
                    //create user's account
                    AppSettings.shared.updateLoader(withStatus: "Creating..")
                    self.signUpUserWith(name: name, contact: self.contact, countryCode: self.countryCode, password: myPassword)
                }
            }
        }else if appAction == .forgotPassword{

            guard let accessToken = self.userAccessToken else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message:"Sorry!, we are having problem in order to recognize you. Please try later", completionBlock: {
                    self.navigationController?.pop(true)
                })
                return
            }
            AppSettings.shared.showLoader(withStatus: "Validating..")
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: authID,
                verificationCode: otp)

            Auth.auth().signIn(with: credential) { (user, error) in
                if let error = error {
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {
                        self.otpView.initalizeUI()
                    })
                    return
                }else{
                    AppSettings.shared.hideLoader()
                    self.openResetPasswordScreen(contact: self.contact, countryCode: self.countryCode, accessToken: accessToken)
                }
            }
        }
        self.otpView.initalizeUI()
    }

    func openResetPasswordScreen(contact: String, countryCode: String,accessToken:String){
        let resetPswdVC = AppStoryboard.Main.viewController(ResetPasswordViewController.self)
        resetPswdVC.countryCode = countryCode
        resetPswdVC.phoneNumber = contact
        resetPswdVC.accessToken = accessToken
        self.navigationController?.pushViewController(resetPswdVC, animated: true)
    }

    func signUpUserWith(name : String, contact : String, countryCode : String, password : String){
        LoginService.sharedInstance.registerUserWith(name, mobile: countryCode+contact, password: password) { (success, resUser, message) in
            if success{
                if let user = resUser{
                    FireBaseUser.registerUser(withName: user.fullName, email: user.contact+"_user@cartingkidz.com", password: "ck_\(user.contact)", completion: { (status) in
                        if status{
                            AppSettings.shared.hideLoader()
                            LoginService.sharedInstance.updateDeviceTokeOnServer()
                            AppSettings.shared.proceedToHome()
                        }else{
                            AppSettings.shared.hideLoader()
                            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Something went wrong!!")
                        }
                    })
                }else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }

    }
}


extension OTPVerificationViewController: VPMOTPViewDelegate {
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        if hasEntered{
            self.verifyOTP(otp: self.otpString, authID: self.verificationID)
        }
        return hasEntered
    }

    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }

    func enteredOTP(otpString: String) {
        self.otpString = otpString
    }
}


