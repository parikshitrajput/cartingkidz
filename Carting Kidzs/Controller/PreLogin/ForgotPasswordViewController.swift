//
//  ForgotPasswordViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 02/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import CountryPickerView
import Firebase


class ForgotPasswordViewController: UIViewController {
    @IBOutlet weak var countryCodeLabel : UILabel!
    @IBOutlet weak var phoneNumberTextField : UITextField!
    @IBOutlet weak var containner : UIView!
    @IBOutlet weak var countryPickerView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    var cpv : CountryPickerView!
    var selectedCountry: Country!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.countryPickerSetUp()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "FORGOT PASSWORD"
        CommonClass.sharedInstance.setPlaceHolder(self.phoneNumberTextField, placeHolderString: "Phone Number", withColor: .darkGray)
        self.setupNavigationViews()
        self.decorateViews()
    }
    override func viewWillLayoutSubviews() {
        self.decorateViews()
    }

    func decorateViews(){
        CommonClass.makeViewCircularWithCornerRadius(self.submitButton, borderColor: .clear, borderWidth: 0, cornerRadius: 2)
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: .clear, borderWidth: 0, cornerRadius: 10)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }


    override func viewDidAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.makeTransparent()
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickCountryCodeButton(_ sender: UIButton){
        cpv.showCountriesList(from: self)
    }

    @IBAction func onClickSignInButton(_ sender: UIButton){
        self.navigationController?.pop(true)
    }


    @IBAction func onClickSubmitButton(_ sender: UIButton){
        let countryCode = self.selectedCountry.phoneCode
        guard let phoneNumber = phoneNumberTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your contact", completionBlock: {})
            return
        }

        let validation = self.validateParams(countryCode: countryCode, phoneNumber: phoneNumber)
        if !validation.success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: validation.message, completionBlock: {})
            return
        }
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.networkIsNotConnected.rawValue, completionBlock: {})
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        self.getAccessToken(countryCode: countryCode, phoneNumber: phoneNumber)
    }

    func getAccessToken(countryCode:String,phoneNumber:String){
        LoginService.sharedInstance.getTokenToResetPasswordAfterForgot(contact: (countryCode+phoneNumber).removingWhitespaces()) { (success, isExists, resToken, message) in
            if success{
                if isExists{
                    if let token = resToken{
                        self.sendOTPOn(countryCode: countryCode, phoneNumber: phoneNumber,accessToken: token)
                    }else{
                        AppSettings.shared.hideLoader()
                        NKToastHelper.sharedInstance.showErrorAlert(self, message: "Something went wrong! please try later")
                    }
                }else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: "We didn't recognize the number you've entered")
                }
            }else{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }


    func validateParams(countryCode:String,phoneNumber: String) -> (success:Bool,message:String){
        if countryCode.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter country code")
        }

        if phoneNumber.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your phone number")
        }

        let phoneValidation = CommonClass.validatePhoneNumber(phoneNumber.trimmingCharacters(in: .whitespaces))
        if !phoneValidation{
            return (false,"Please enter a valid phone number")
        }

        if (phoneNumber.trimmingCharacters(in: .whitespaces).count < self.selectedCountry.minDigit){
            return (false,"Phone number should not be less than \(self.selectedCountry.minDigit) in length")
        }

        if  (phoneNumber.trimmingCharacters(in: .whitespaces).count > self.selectedCountry.maxDigit){
            return (false,"Phone number should not be more than \(self.selectedCountry.maxDigit) in length")
        }

        return (true,"")
    }


    func sendOTPOn(countryCode:String,phoneNumber:String, accessToken:String){
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: signOutError.localizedDescription, completionBlock: {
                self.navigationController?.pop(true)
            })
        }
        AppSettings.shared.showLoader(withStatus: "Sending OTP..")
        let fullPhoneNumber = countryCode+phoneNumber
        PhoneAuthProvider.provider().verifyPhoneNumber(fullPhoneNumber, uiDelegate: nil) { (verificationID, error) in
            AppSettings.shared.hideLoader()
            if let error = error {
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {})
                return
            }
            //proceed to otp varification
            guard let verificationToken = verificationID else{
                return
            }

            self.openOTPScreen(verificationID: verificationToken, accessToken: accessToken, contact: phoneNumber, countryCode: countryCode)
        }
    }

    func openOTPScreen(verificationID: String,accessToken:String, contact: String, countryCode: String, appAction: appAction = .forgotPassword){
        let otpVC = AppStoryboard.Main.viewController(OTPVerificationViewController.self)
        otpVC.appAction = appAction
        otpVC.verificationID = verificationID
        otpVC.contact = contact
        otpVC.countryCode = countryCode
        otpVC.userAccessToken = accessToken
        otpVC.fullName = nil
        otpVC.password = nil
        self.navigationController?.pushViewController(otpVC, animated: true)
    }

}


extension ForgotPasswordViewController: CountryPickerViewDataSource,CountryPickerViewDelegate{

    func countryPickerSetUp() {
        if cpv != nil{
            cpv.removeFromSuperview()
        }
        cpv = CountryPickerView(frame: self.countryPickerView.frame)
        self.countryPickerView.addSubview(cpv)
        cpv.countryDetailsLabel.font = fonts.OpenSans.regular.font(.medium)
        cpv.showPhoneCodeInView = true
        cpv.showCountryCodeInView = true
        //cpv.showCountryFlagInView = false
        cpv.dataSource = self
        cpv.delegate = self
        cpv.translatesAutoresizingMaskIntoConstraints = false
        let topConstraints = NSLayoutConstraint(item: cpv, attribute: .top, relatedBy: .equal, toItem: self.countryPickerView, attribute: .top, multiplier: 1, constant: 0)
        let bottomConstraints = NSLayoutConstraint(item: cpv, attribute: .bottom, relatedBy: .equal, toItem: self.countryPickerView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraints = NSLayoutConstraint(item: cpv, attribute: .leading, relatedBy: .equal, toItem: self.countryPickerView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraints = NSLayoutConstraint(item: cpv, attribute: .trailing, relatedBy: .equal, toItem: self.countryPickerView, attribute: .trailing, multiplier: 1, constant: 0)
        self.countryPickerView.addConstraints([topConstraints,leadingConstraints,trailingConstraints,bottomConstraints])
        self.selectedCountry = cpv.selectedCountry
        self.countryCodeLabel.text = "(\(self.selectedCountry.code))"+self.selectedCountry.phoneCode
    }
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return (AppSettings.shared.currentCountryCode.count != 0) ? "Current" : nil
    }

    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool {
        return false
    }

    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select Country"
    }

    func closeButtonNavigationItem(in countryPickerView: CountryPickerView) -> UIBarButtonItem? {
        return nil

        //        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: nil, action: nil)
        //        barButton.tintColor = appColor.blue
        //        return barButton
    }

    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        return .navigationBar
    }


    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.selectedCountry = country
        self.countryCodeLabel.text = "(\(self.selectedCountry.code))"+self.selectedCountry.phoneCode
        //self.phoneNumberTextField.becomeFirstResponder()
    }


    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
        if let currentCountry = countryPickerView.getCountryByPhoneCode(AppSettings.shared.currentCountryCode){
            return [currentCountry]
        }else{
            return [countryPickerView.selectedCountry]
        }
    }

    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }


}

