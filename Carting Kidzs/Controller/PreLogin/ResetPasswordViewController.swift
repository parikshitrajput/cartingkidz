//
//  ResetPasswordViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 03/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {
    @IBOutlet weak var passwordTextField : UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var containner : UIView!

    var phoneNumber:String!
    var countryCode:String!
    var accessToken:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "RESET PASSWORD"
        CommonClass.sharedInstance.setPlaceHolder(self.passwordTextField, placeHolderString: "New Password", withColor: .darkGray)
    }

    override func viewWillLayoutSubviews() {
        self.decorateViews()


    }

    func decorateViews(){
        CommonClass.makeViewCircularWithCornerRadius(self.submitButton, borderColor: .clear, borderWidth: 0, cornerRadius: 2)
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: .clear, borderWidth: 0, cornerRadius: 10)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }


    override func viewDidAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.makeTransparent()
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSubmitButton(_ sender: UIButton){
        guard let password = passwordTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter password", completionBlock: {})
            return
        }

        let validation = self.validateParams(password: password)
        if !validation.success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: validation.message, completionBlock: {})
            return
        }

        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        AppSettings.shared.showLoader(withStatus: "Resetting..")
        let contact = (self.countryCode+self.phoneNumber).removingWhitespaces()
        self.resetPassword(contact: contact, accessToken: self.accessToken, password: password)
    }

    func resetPassword(contact:String,accessToken:String,password:String){
        LoginService.sharedInstance.resetPassword(contact: contact, accessToken: accessToken, password: password) { (success, done, message) in
            AppSettings.shared.hideLoader()
            if success{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message, completionBlock: {
                    if let signInVC = self.navigationController?.viewControllers.filter({ (vc) -> Bool in
                        vc is SignInViewController
                    }).first{
                        self.navigationController?.popToViewController(signInVC, animated: false)
                    }
                })
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    func validateParams(password:String) -> (success:Bool,message:String){
        let passwordValidation = CommonClass.validatePassword(password)
        if !passwordValidation{
            return (false,"Please enter a valid password")
        }
        return (true,"")
    }

}
