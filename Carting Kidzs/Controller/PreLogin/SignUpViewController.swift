//
//  SignUpViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 26/06/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import CountryPickerView
import Firebase


class SignUpViewController: UIViewController {
    @IBOutlet weak var countryCodeLabel : UILabel!
    @IBOutlet weak var phoneNumberTextField : UITextField!
    @IBOutlet weak var fullNameTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!
    @IBOutlet weak var containner : UIView!
    @IBOutlet weak var countryPickerView: UIView!
    @IBOutlet weak var submitButton: UIButton!

    var cpv : CountryPickerView!
    var selectedCountry: Country!





    override func viewDidLoad() {
        super.viewDidLoad()
        self.countryPickerSetUp()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "SIGN UP"
        CommonClass.sharedInstance.setPlaceHolder(self.fullNameTextField, placeHolderString: "Full Name", withColor: .darkGray)
        CommonClass.sharedInstance.setPlaceHolder(self.phoneNumberTextField, placeHolderString: "Phone Number", withColor: .darkGray)
        CommonClass.sharedInstance.setPlaceHolder(self.passwordTextField, placeHolderString: "Password", withColor: .darkGray)
        self.setupNavigationViews()
        self.decorateViews()

    }
    override func viewWillLayoutSubviews() {
        self.decorateViews()


    }

    func decorateViews(){
        CommonClass.makeViewCircularWithCornerRadius(self.submitButton, borderColor: .clear, borderWidth: 0, cornerRadius: 2)
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: .clear, borderWidth: 0, cornerRadius: 10)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }


    override func viewDidAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.makeTransparent()
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickCountryCodeButton(_ sender: UIButton){
        cpv.showCountriesList(from: self)
    }

    @IBAction func onClickSignInButton(_ sender: UIButton){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickTermAndConditionsButton(_ sender: UIButton){
        self.openTermsOfUses()
    }
    func openTermsOfUses(){
        let termsOfUses = AppStoryboard.Settings.viewController(PrivacyPolicyViewController.self)
        termsOfUses.isPrivacyPolicy = false
        termsOfUses.isFromSignUp = true
        let nav = AppSettings.shared.getNavigation(vc: termsOfUses)
        nav.modalPresentationStyle = .overCurrentContext
        self.present(nav, animated: true, completion: nil)
    }

    @IBAction func onClickSignUpButton(_ sender: UIButton){
        guard let fullName = fullNameTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your full name", completionBlock: {})
            return
        }

        let countryCode = self.selectedCountry.phoneCode

        guard let phoneNumber = phoneNumberTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your contact", completionBlock: {})
            return
        }

        guard let password = passwordTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your password", completionBlock: {})
            return
        }


        let validation = self.validateParams(fullName: fullName, countryCode: countryCode, phoneNumber: phoneNumber, password: password)
        if !validation.success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: validation.message, completionBlock: {})
            return
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")

        //self.sendOTPOn(countryCode: countryCode, phoneNumber: phoneNumber, fullName: fullName, password: password)
        self.contactVerifyBeforeSignUP(countryCode: countryCode, phoneNumber: phoneNumber, fullName: fullName, password: password)    }

    ///////check contact before signup
    
    func contactVerifyBeforeSignUP(countryCode:String,phoneNumber:String,fullName:String,password:String){
        LoginService.sharedInstance.contactVerifyBeforeSinUp(contact: (countryCode+phoneNumber).removingWhitespaces()) { (success, isExists, resToken, message) in
            if success{
                if isExists{
                    AppSettings.shared.hideLoader()
                    self.sendOTPOn(countryCode: countryCode, phoneNumber: phoneNumber, fullName: fullName, password: password)

                }else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: "We didn't recognize the number you've entered")
                }
            }else{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                
            }
        }
    }
    
    
    
    

    func validateParams(fullName:String,countryCode:String,phoneNumber: String,password:String) -> (success:Bool,message:String){
        if fullName.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your full name")
        }

        if countryCode.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter country code")
        }

        if phoneNumber.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your phone number")
        }
        let phoneValidation = CommonClass.validatePhoneNumber(phoneNumber.trimmingCharacters(in: .whitespaces))
        if !phoneValidation{
            return (false,"Please enter a valid phone number")
        }

        if (phoneNumber.trimmingCharacters(in: .whitespaces).count < self.selectedCountry.minDigit){
            return (false,"Phone number should not be less than \(self.selectedCountry.minDigit) in length")
        }
        if  (phoneNumber.trimmingCharacters(in: .whitespaces).count > self.selectedCountry.maxDigit){
            return (false,"Phone number should not be more than \(self.selectedCountry.maxDigit) in length")
        }

        let passwordValidation = CommonClass.validatePassword(password)
        if !passwordValidation{
            return (false,"Please enter a valid password")
        }

        return (true,"")
    }


    func sendOTPOn(countryCode:String,phoneNumber:String, fullName:String,password:String){
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: signOutError.localizedDescription, completionBlock: {
                self.navigationController?.pop(true)
            })
        }
        AppSettings.shared.showLoader(withStatus: "Sending OTP..")
        let fullPhoneNumber = countryCode+phoneNumber
        PhoneAuthProvider.provider().verifyPhoneNumber(fullPhoneNumber, uiDelegate: nil) { (verificationID, error) in
            AppSettings.shared.hideLoader()
            if let error = error {
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {})
                return
            }
            //proceed to otp varification
            guard let verificationToken = verificationID else{
                return
            }

            self.openOTPScreen(verificationID: verificationToken, fullName: fullName, contact: phoneNumber, countryCode: countryCode, password: password)
        }
    }

    func openOTPScreen(verificationID: String, fullName: String, contact: String, countryCode: String, password: String, appAction: appAction = .signUp){
        let otpVC = AppStoryboard.Main.viewController(OTPVerificationViewController.self)
        otpVC.appAction = appAction
        otpVC.verificationID = verificationID
        otpVC.contact = contact
        otpVC.countryCode = countryCode

        otpVC.fullName = fullName
        otpVC.password = password
        self.navigationController?.pushViewController(otpVC, animated: true)

    }
}



extension SignUpViewController: CountryPickerViewDataSource,CountryPickerViewDelegate{

    func countryPickerSetUp() {
        if cpv != nil{
            cpv.removeFromSuperview()
        }
        cpv = CountryPickerView(frame: self.countryPickerView.frame)
        self.countryPickerView.addSubview(cpv)
        cpv.countryDetailsLabel.font = fonts.OpenSans.regular.font(.medium)
        cpv.showPhoneCodeInView = true
        cpv.showCountryCodeInView = true
        //cpv.showCountryFlagInView = false
        cpv.dataSource = self
        cpv.delegate = self
        cpv.translatesAutoresizingMaskIntoConstraints = false
        let topConstraints = NSLayoutConstraint(item: cpv, attribute: .top, relatedBy: .equal, toItem: self.countryPickerView, attribute: .top, multiplier: 1, constant: 0)
        let bottomConstraints = NSLayoutConstraint(item: cpv, attribute: .bottom, relatedBy: .equal, toItem: self.countryPickerView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraints = NSLayoutConstraint(item: cpv, attribute: .leading, relatedBy: .equal, toItem: self.countryPickerView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraints = NSLayoutConstraint(item: cpv, attribute: .trailing, relatedBy: .equal, toItem: self.countryPickerView, attribute: .trailing, multiplier: 1, constant: 0)
        self.countryPickerView.addConstraints([topConstraints,leadingConstraints,trailingConstraints,bottomConstraints])
        self.selectedCountry = cpv.selectedCountry
        self.countryCodeLabel.text = "(\(self.selectedCountry.code))"+self.selectedCountry.phoneCode
    }
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return (AppSettings.shared.currentCountryCode.count != 0) ? "Current" : nil
    }

    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool {
        return false
    }

    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select Country"
    }

    func closeButtonNavigationItem(in countryPickerView: CountryPickerView) -> UIBarButtonItem? {
        return nil

//        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: nil, action: nil)
//        barButton.tintColor = appColor.blue
//        return barButton
    }

    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        return .navigationBar
    }


    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.selectedCountry = country
        self.countryCodeLabel.text = "(\(self.selectedCountry.code))"+self.selectedCountry.phoneCode
        //self.phoneNumberTextField.becomeFirstResponder()
    }


    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
        if let currentCountry = countryPickerView.getCountryByPhoneCode(AppSettings.shared.currentCountryCode){
            return [currentCountry]
        }else{
            return [countryPickerView.selectedCountry]
        }
    }

    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }


}



