//
//  PreLoginViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 25/06/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import EAIntroView

class PreLoginViewController: UIViewController {
    @IBOutlet weak var introView: UIView!
    @IBOutlet weak var bookCartingKidzsButton: UIButton!


    var selectedIndex: UInt = 0
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.setupNavigationViews()
        self.navigationController?.makeTransparent()
        self.showIntro()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }


    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBookCartingKidzs(_ sender: UIButton){
        let signInVC = AppStoryboard.Main.viewController(SignInViewController.self)
        self.navigationController?.pushViewController(signInVC, animated: true)
    }

}




extension PreLoginViewController : EAIntroDelegate{
    func showIntro() {
        //AppSettings.shared.isIntroShown = true

        let sampleDescription1 = "Safety + Service = Piece of Mind!"
        let sampleDescription2 = "Service you can trust that delivers value for your money!"
        let sampleDescription3 = "Carting Kidz helps you get your kids where they need to go!"

        let page1 = EAIntroPage()
        page1.title = "Carting Kidz";
        page1.titleColor = UIColor.black
        page1.titleFont = fonts.OpenSans.semiBold.font(.xXXLarge)
        page1.descFont = fonts.OpenSans.regular.font(.medium)
        page1.desc = sampleDescription1;
        page1.descColor = UIColor.black
        page1.bgImage = #imageLiteral(resourceName: "tt1")

        let page2 = EAIntroPage()
        page2.title = "Carting Kidz";
        page2.titleColor = UIColor.black
        page2.titleFont = fonts.OpenSans.semiBold.font(.xXXLarge)
        page2.descFont = fonts.OpenSans.regular.font(.medium)
        page2.desc = sampleDescription2;
        page2.descColor = UIColor.black
        page2.bgImage = #imageLiteral(resourceName: "tt2")

        let page3 = EAIntroPage()
        page3.title = "Carting Kidz";
        page3.titleColor = UIColor.black
        page3.titleFont = fonts.OpenSans.semiBold.font(.xXXLarge)
        page3.descFont = fonts.OpenSans.regular.font(.medium)
        page3.desc = sampleDescription3;
        page3.descColor = UIColor.black
        page3.bgImage = #imageLiteral(resourceName: "tt3")

        let intro = EAIntroView(frame: self.introView.bounds, andPages: [page1,page2,page3])
        intro?.pageControl.currentPageIndicatorTintColor = appColor.red
        intro?.pageControl.pageIndicatorTintColor = appColor.gray
        intro?.tapToNext = false
        intro?.swipeToExit = false
        intro?.skipButton.setTitle("", for: .normal)
        intro?.delegate = self
        intro?.show(in: self.introView, animateDuration: 0.3, withInitialPageIndex: self.selectedIndex)
    }

    func intro(_ introView: EAIntroView!, pageEndScrolling page: EAIntroPage!, with pageIndex: UInt) {
//        let title = (pageIndex == 2) ? "Get Started" : "Skip"
//        introView.skipButton.setTitle(title, for: .normal)
        self.selectedIndex = pageIndex
    }

    func introDidFinish(_ introView: EAIntroView!, wasSkipped: Bool) {
//        AppSettings.shared.isIntroShown = true
    }

}

