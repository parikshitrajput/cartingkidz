//
//  PaymentResponseViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 31/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
protocol PaymentResponseViewControllerDelegate {
    func paymentResponseViewController(viewController:PaymentResponseViewController, didTapClose done:Bool)
}

class PaymentResponseViewController: UIViewController {
    var payment:Payment!
    var delegate: PaymentResponseViewControllerDelegate?
    @IBOutlet weak var amountLabel:UILabel!
    var user: User!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.amountLabel.text = String(format: "$ %.2f", payment.amount)
        self.setRating(rating: Int(self.payment.ride.rating))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickClose(_ sender:UIButton){
        self.delegate?.paymentResponseViewController(viewController: self, didTapClose: true)
    }

    @IBAction func onClickRating(_ sender:UIButton){
        let rating = sender.tag
        for index in 1...5{
            guard let button = self.view.viewWithTag(index) as? UIButton else{return}
            button.isSelected = (rating >= button.tag)
        }
        self.rateDriver(rideId: self.payment.ride.ID, rating: Double(rating), userID: self.user.ID)
    }

    private func setRating(rating:Int){
        var rate = (rating < 1) ? 1 : rating
        rate = (rating > 5) ? 5 : rating
        for index in 1...5{
            guard let button = self.view.viewWithTag(index) as? UIButton else{return}
            button.isSelected = (rate >= button.tag)
        }
    }

    func rateDriver(rideId:String,rating: Double,userID:String) -> Void {
        AppSettings.shared.showLoader(withStatus: "Sending..")
        RideService.sharedInstance.rateDriverWithRide(rideId, rating: rating, userID:userID) { (success,responseRide,message)  in
            AppSettings.shared.hideLoader()
            if success{
                if let aRide = responseRide{
                    self.payment.ride = aRide
                    self.setRating(rating: Int(aRide.rating))
                    NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":aRide])

                    NKToastHelper.sharedInstance.showErrorAlert(self, message: "Thanks for your response. It'll help us to improve your experience with us",completionBlock:{
                        self.delegate?.paymentResponseViewController(viewController: self, didTapClose: true)
                    })
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }
    
}
