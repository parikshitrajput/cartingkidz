//
//  BillViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 26/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class BillViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var payButton: UIButton!
    var fromHistory:Bool = false
    var ride:Ride!
    var user : User!
    let heights = [92,147,101,178]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.title = "Your Bill".uppercased()
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.setRightNavigationButton()
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }

    func setRightNavigationButton(){
        let button = UIButton()
        button.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        button.setImage(#imageLiteral(resourceName: "support_button"), for: .normal)
        button.addTarget(self, action: #selector(onClickSupport(_:)), for: .touchUpInside)
        let navbutton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = navbutton
    }

    override func viewDidAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }

    override func viewWillLayoutSubviews() {
        self.tableView.reloadData()
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(payButton, borderColor: appColor.green, borderWidth: 0, cornerRadius: 4)
    }

    @IBAction func onClickPay(_ sender: UIButton){
        self.proceedToPayFor(ride: self.ride)
    }

    func proceedToPayFor(ride:Ride){
        let payForRide = AppStoryboard.Billing.viewController(PaymentOptionViewController.self)
        payForRide.user = self.user
        payForRide.ride = ride
        self.navigationController?.pushViewController(payForRide, animated: true)
    }


    @IBAction func onClickSupport(_ sender: UIButton){
        let supportVC = AppStoryboard.Support.viewController(SupportsViewController.self)
        supportVC.isFromMenu = false
        supportVC.ride = self.ride
        self.navigationController?.pushViewController(supportVC, animated: true)
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        if fromHistory{
            self.navigationController?.pop(true)
        }else{
            self.navigationController?.popToRoot(true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension BillViewController: UITableViewDataSource, UITableViewDelegate,BillingRideRatingCellDelegate{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BillingPriceCell", for: indexPath) as! BillingPriceCell
            cell.amountLabel.text = String(format: "%0.2f",self.ride.cost.totalPrice)
            cell.dateTimeLabel.text = (ride.rideType == .instant) ? ride.createdAt : ride.scheduledAt
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BillingRideAddressCell", for: indexPath) as! BillingRideAddressCell
            cell.pickUpAddressLabel.text = ride.startLocation
            cell.dropAddressLabel.text = ride.dropLocation
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BillingDriverDetailCell", for: indexPath) as! BillingDriverDetailCell
            cell.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))
            cell.driverNameLabel.text = self.ride.driver.fullName.capitalized
            cell.carNameLabel.text = "\(self.ride.car.name) \(self.ride.car.number.uppercased())"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BillingRideRatingCell", for: indexPath) as! BillingRideRatingCell
            if self.ride.rating >= 1.0{
                cell.rating = Int(self.ride.rating)
            }
            cell.delegate = self
            return cell
        }
    }

    func cell(cell: BillingRideRatingCell, didRate rating: Double) {
        self.rateDriver(rideId: self.ride.ID, rating: rating, userID: self.user.ID)
    }

    func rateDriver(rideId:String,rating: Double,userID:String) -> Void {
        AppSettings.shared.showLoader(withStatus: "Sending..")
        RideService.sharedInstance.rateDriverWithRide(rideId, rating: rating, userID:userID) { (success,responseRide,message)  in
            AppSettings.shared.hideLoader()
            if success{
                if let aRide = responseRide{
                    self.ride = aRide
                    self.tableView.reloadData()
                    NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: "Thanks for your response. It'll help us to improve for experience with us")
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(self.heights[indexPath.row])
    }
}






