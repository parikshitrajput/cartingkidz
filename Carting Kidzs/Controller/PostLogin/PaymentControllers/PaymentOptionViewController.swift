//
//  PaymentOptionViewController.swift
//  GPDock
//
//  Created by TecOrb on 27/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Stripe

class PaymentOptionViewController: UIViewController {
    @IBOutlet weak var optionTableView : UITableView!
    var user:User!
    var cards = Array<Card>()
    var ride: Ride!
    var couponCode : String?
    var selectedIndex = -1
    var defaultCardIndex = -1


    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        optionTableView.dataSource = self
        optionTableView.delegate = self
        optionTableView.tableHeaderView = UIView(frame: CGRect.zero)
        self.optionTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.getUserCards()
    }



    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }


    func getUserCards() -> Void {
        AppSettings.shared.showLoader(withStatus: "Loading..")
        PaymentService.sharedInstance.getCardsForUser() { (success,resCards,message)  in
            AppSettings.shared.hideLoader()
            if let someCards = resCards{
                self.cards.removeAll()
                self.cards.append(contentsOf: someCards)
                self.optionTableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        NotificationCenter.default.post(name: .CHECK_BOOKING_VIEW_NOTIFICATION, object: nil, userInfo: nil)
        self.navigationController?.pop(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PaymentOptionViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = 72
        if indexPath.section == 1{
            if indexPath.row == selectedIndex{
                height = 72
            }
        }
        return height
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var height:CGFloat = 12

        if section == 0{
            height = 0
        }else if section == 1{
            if cards.count == 0{height = 0}else{height = 12}
        }else if section == 2 {
            height = 12
        }
        return height
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        var height:CGFloat = 0

        if section == 0{
            height = 0
        }else if section == 1{
            if cards.count == 0{height = 0}else{height = 0}
        }else if section == 2 {
            height = 0
        }
        return height
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        switch section {
        case 0:
            rows = 1
        case 1:
            rows = cards.count
        case 2:
            rows = 1
        default:
            rows = 0
        }
        return rows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PayableAmountDescriptionCell", for: indexPath) as! PayableAmountDescriptionCell
            cell.payableTitleLabel.text = ""
            let totalAmount = self.ride.cost.totalPrice //+ (self.ride.cost.totalPrice*10.0/100.0)
            cell.amountLabel.text = String(format: "$ %0.2lf", totalAmount)
            return cell
        }else if indexPath.section == 1{

            if selectedIndex == indexPath.row{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedCardTableViewCell", for: indexPath) as! SelectedCardTableViewCell
                let card = cards[indexPath.row]
                cell.cardImageView.image = UIImage(named:card.brand.lowercased()+"_")
                cell.cardNumberLabel.text = "xxxx xxxx xxxx \(card.last4)"
                cell.fundingTypeLabel.text = "Expiring \(card.expMonth)/\(card.expYear)"
                cell.defaultLabel.isHidden = !card.isDefault
                self.defaultCardIndex = indexPath.row

                cell.doneButton.addTarget(self, action: #selector(onClickPayButton(_:)), for: .touchUpInside)
                return cell

            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CardTableViewCell", for: indexPath) as! CardTableViewCell
                let card = cards[indexPath.row]
                cell.cardImageView.image = UIImage(named:card.brand.lowercased()+"_")
                cell.cardNumberLabel.text = "xxxx xxxx xxxx \(card.last4)"
                let expMonth = String(format: "%02d", card.expMonth)
                let expYear = String(format: "%02d", card.expYear)
                cell.fundingTypeLabel.text = "Expiring \(expMonth)/\(expYear)"
                cell.defaultLabel.isHidden = !card.isDefault

                cell.removeButton.addTarget(self, action: #selector(onClickRemoveButton(_:)), for: .touchUpInside)

                return cell
            }

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCardTableViewCell", for: indexPath) as! NewCardTableViewCell
            return cell

        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if self.selectedIndex == indexPath.row{
                self.selectedIndex = -1
            }else{
                self.selectedIndex = indexPath.row
                if self.defaultCardIndex != indexPath.row {
                    let card = cards[indexPath.row]
                    if !card.isDefault {
                        self.askToMakeDefualtCard(card: card, atIndexPath: indexPath)
                    }
                }
            }
            
            tableView.reloadData()
        }else if indexPath.section == 2{
            self.navigateToAddNewCard()
        }
    }


    func navigateToAddNewCard(){
        let addNewPaymentOptionVC = AppStoryboard.Billing.viewController(AddNewPaymentOptionViewController.self)
        addNewPaymentOptionVC.user = self.user
        addNewPaymentOptionVC.ride = self.ride
        addNewPaymentOptionVC.couponCode = self.couponCode
        self.navigationController?.pushViewController(addNewPaymentOptionVC, animated: true)
    }

    @IBAction func onTextDidChanged(_ textField: UITextField){
        //self.cvvForSelectedCard = textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    @IBAction func onClickRemoveButton(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.optionTableView) as IndexPath?{
            if indexPath.row == selectedIndex{self.selectedIndex = -1}
            let card = cards[indexPath.row]
            self.askToDeleteCard(card: card, atIndexPath: indexPath)
        }
    }

    func askToDeleteCard(card : Card,atIndexPath indexPath: IndexPath) {
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "Would you really want to delete this card?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Delete", style: .destructive){[weak card](action) in
            alert.dismiss(animated: true, completion: nil)
            self.removeCard(card, indexPath: indexPath)
        }

        let cancelAction = UIAlertAction(title: "Nope", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(okayAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }

    func removeCard(_ mycard:Card?,indexPath: IndexPath) -> Void {
        guard let card = mycard else {
            return
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        PaymentService.sharedInstance.removeCard(card.ID) { (success, message) in
            AppSettings.shared.hideLoader()
            if success{
                self.cards.remove(at: indexPath.row)
                self.optionTableView.deleteRows(at: [indexPath], with: .automatic)
            }
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
        }

    }

    func confirmByPaying(indexPath:IndexPath) {
        let card = cards[indexPath.row]
        let YY = CommonClass.sharedInstance.formattedDateWith(Date(), format: "YY")
        let MM = CommonClass.sharedInstance.formattedDateWith(Date(), format: "MM")

        let expValidation = STPCardValidator.validationState(forExpirationYear: YY, inMonth: MM)
        if expValidation != .valid{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "The card was declined. Please reenter the payment details")
            return
        }

        AppSettings.shared.showLoader(withStatus: "Paying...")
        let totalAmount = self.ride.cost.totalPrice //+ (self.ride.cost.totalPrice*10.0/100.0)
        self.payForRide(self.user, ride: self.ride, cardID: card.ID, amount: totalAmount, couponCode: self.couponCode)
    }

    func payForRide(_ user:User,ride:Ride,cardID:String, amount:Double, couponCode:String? = nil){
        PaymentService.sharedInstance.payForRide(userID: user.ID, cardID:cardID, stripeToken: "", rideID: ride.ID, amount: amount) { (success, resPayment, message) in
            if success{
                if let payment = resPayment{
                    self.ride.isPaid = true
                    self.ride.paymentStatus = true
                    self.ride = payment.ride
                    NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":payment.ride])
                    self.showPaymentSuccess(payment)
                }else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    @IBAction func onClickPayButton(_ sender: UIButton){

        if let indexPath = sender.tableViewIndexPath(self.optionTableView){
            let card = cards[indexPath.row]
            let YY = "\(card.expYear)".suffix(2) //let last4 = a.suffix(4)
            let MM = "\(card.expMonth)"
            let expValidation = STPCardValidator.validationState(forExpirationYear: String(YY), inMonth: MM)
            if expValidation != .valid{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "The card was declined. Please reenter the payment details")
                return
            }
            self.confirmByPaying(indexPath: indexPath)
        }
    }
    
    
    func askToMakeDefualtCard(card : Card,atIndexPath indexPath: IndexPath) {
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "Would you really want to make  this card default?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Yes", style: .default){[weak card](action) in
            alert.dismiss(animated: true, completion: nil)
            self.makeDefaultCard(card, indexPath: indexPath)
        }
        
        let cancelAction = UIAlertAction(title: "Nope", style: .cancel){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okayAction)
        alert.addAction(cancelAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    
    func makeDefaultCard(_ mycard:Card?,indexPath: IndexPath) -> Void {
        guard let card = mycard else {
            return
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        PaymentService.sharedInstance.defaultCard(card.ID) { (success,resCard, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let defaultcard = resCard{
                    for card in self.cards{
                        card.isDefault = (defaultcard.ID == card.ID)
                        self.optionTableView.reloadData()
                    }
                }else{
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
            }
        }
        
    }

}


extension PaymentOptionViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
       // self.cvvForSelectedCard = textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
       // self.cvvForSelectedCard = textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    func showPaymentSuccess(_ payment: Payment){
        AppSettings.shared.hideLoader()
        let paymentSuccessVC = AppStoryboard.Billing.viewController(PaymentResponseViewController.self)
        paymentSuccessVC.payment = payment
        paymentSuccessVC.delegate = self
        self.present(paymentSuccessVC, animated: true, completion: nil)
    }

}

extension PaymentOptionViewController:PaymentResponseViewControllerDelegate{
    func paymentResponseViewController(viewController: PaymentResponseViewController, didTapClose done: Bool) {
        viewController.dismiss(animated: false, completion: nil)
        NotificationCenter.default.post(name: .CHECK_BOOKING_VIEW_NOTIFICATION, object: nil, userInfo: nil)
        self.navigationController?.popToRoot(true)
    }
}








