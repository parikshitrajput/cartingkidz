//
//  ProfileViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 07/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    let placeholders = ["Full Name","Mobile","Email"]
    var user: User!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.registerNIbs()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.user = User.loadSavedUser()
        self.tableView.reloadData()
    }
    func registerNIbs(){
        tableView.register(UINib(nibName: "UserImageCell", bundle: nil), forCellReuseIdentifier: "UserImageCell")
        tableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "TextFieldCell")
        tableView.register(UINib(nibName: "AlergiesCell", bundle: nil), forCellReuseIdentifier: "AlergiesCell")
        tableView.register(UINib(nibName: "HeaderCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        tableView.register(UINib(nibName: "GenderCell", bundle: nil), forCellReuseIdentifier: "GenderCell")
        tableView.register(UINib(nibName: "ContactCell", bundle: nil), forCellReuseIdentifier: "ContactCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickMenu(_ sender:UIBarButtonItem){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }

    @IBAction func onClickEditProfileButton(_ sender:UIButton){
        let editProfileVC = AppStoryboard.Profile.viewController(EditProfileViewController.self)
        editProfileVC.delegate = self
        self.navigationController?.pushViewController(editProfileVC, animated: true)
    }

}

extension ProfileViewController:EditProfileViewControllerDelegate{

    func editProfileViewController(viewController: EditProfileViewController, didUpdateProfile user: User) {
        self.user = user
        self.tableView.reloadData()
        NotificationCenter.default.post(name: .USER_DID_UPDATE_PROFILE_NOTIFICATION, object: nil, userInfo: ["user":user])
    }
}

extension ProfileViewController: UITableViewDataSource,UITableViewDelegate{
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return placeholders.count
//    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeholders.count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = self.userCell(tableView, cellForRowAt: indexPath)
            return cell
 
    }

    func profileCell(tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UserImageCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserImageCell", for: indexPath) as! UserImageCell
        cell.userImageView.sd_setImage(with: URL(string:self.user.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:self.user.fullName))
        cell.userNameLabel.text = self.user.fullName.capitalized
        cell.editButton.addTarget(self, action: #selector(onClickEditProfileButton(_:)), for: .touchUpInside)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 68
        if indexPath.row == 0{
            height = 90
        }else{
            height = 68
        }
        return height
    }

    func userCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell{
        if indexPath.row == 0{
            let cell = self.profileCell(tableView: tableView, cellForRowAt: indexPath)
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.text = "+"+user.contact
            cell.textField.placeholder = placeholders[indexPath.row].capitalized
            cell.textField.isUserInteractionEnabled = false
            self.setUpTextColor(textField: cell.textField)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.text = user.email
            cell.textField.placeholder = placeholders[indexPath.row].capitalized
            cell.textField.isUserInteractionEnabled = false
            self.setUpTextColor(textField: cell.textField)
            return cell
        }
    }

    func setUpTextColor(textField:FloatLabelTextField){
        textField.titleTextColour = appColor.red
        textField.titleActiveTextColour = appColor.red
    }
    func setUpTextColor(textField:FloatLabelTextView){
        textField.titleTextColour = appColor.red
        textField.titleActiveTextColour = appColor.red
    }
    func parentCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = placeholders[indexPath.row].capitalized
            cell.textField.text = user.parentName.capitalized
            self.setUpTextColor(textField: cell.textField)
            cell.textField.isUserInteractionEnabled = false
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.text = user.email
            cell.textField.placeholder = placeholders[indexPath.row].capitalized
            cell.textField.isUserInteractionEnabled = false
            self.setUpTextColor(textField: cell.textField)

            return cell

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.text = "+"+user.parentCountryCode+user.parentContact
            cell.textField.placeholder = placeholders[indexPath.row].capitalized
            cell.textField.isUserInteractionEnabled = false
            self.setUpTextColor(textField: cell.textField)
            return cell

        }
        
}

}









