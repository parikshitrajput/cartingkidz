//
//  EditProfileViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 07/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
protocol EditProfileViewControllerDelegate {
    func editProfileViewController(viewController: EditProfileViewController, didUpdateProfile user:User)
}

class EditProfileViewController: UIViewController {
    var imagePickerController : UIImagePickerController!
    var userImage: UIImage?
    var user: User!
    var delegate: EditProfileViewControllerDelegate?
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var emailTextField: FloatLabelTextField!
    @IBOutlet weak var fullNameTextField: FloatLabelTextField!
    @IBOutlet weak var phoneNumberTextField: FloatLabelTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        self.user = User.loadSavedUser()
        self.setupTextFields()
        self.setupUserDetails()
        self.addGestureRecognizer()
    }
    func addGestureRecognizer(){
        let profileTapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(imageTapped(_:)))
        self.userIcon.isUserInteractionEnabled = true
        self.userIcon.addGestureRecognizer(profileTapGestureRecognizer)
    }

    @objc func imageTapped(_ tapGestureRecognizer: UITapGestureRecognizer){
        self.showAlertToChooseAttachmentOption()
    }
    func setupUserDetails(){
        self.userIcon.sd_setImage(with: URL(string:self.user.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:self.user.fullName))
        self.fullNameTextField.text = self.user.fullName.capitalized
        self.phoneNumberTextField.text = "+"+self.user.contact
        self.emailTextField.text = self.user.email
        CommonClass.makeViewCircular(self.userIcon,borderColor: appColor.red,borderWidth: 1)
    }
    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircular(self.userIcon,borderColor: appColor.red,borderWidth: 1)
    }
    override func viewWillAppear(_ animated: Bool) {
        CommonClass.makeViewCircular(self.userIcon,borderColor: appColor.red,borderWidth: 1)
    }
    override func viewDidAppear(_ animated: Bool) {
        CommonClass.makeViewCircular(self.userIcon,borderColor: appColor.red,borderWidth: 1)
    }


    func setupTextFields(){
        self.phoneNumberTextField.isUserInteractionEnabled = false
        self.fullNameTextField.titleFont = fonts.OpenSans.regular.font(.small)
        self.emailTextField.titleFont = fonts.OpenSans.regular.font(.small)
        self.phoneNumberTextField.titleFont = fonts.OpenSans.regular.font(.small)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSubmit(_ sender: UIButton){
//        self.userImage = self.userIcon.image
        guard let fullName = self.fullNameTextField.text else{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter full name")
            return
        }

        guard let email = self.emailTextField.text else{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter email")
            return
        }

        let validation = self.validateParams(fullName: fullName.trimmingCharacters(in: .whitespaces), email: email.removingWhitespaces())
        if !validation.success{
            NKToastHelper.sharedInstance.showErrorAlert(self, message:validation.message)
            return
        }

        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message:warningMessage.networkIsNotConnected.rawValue)
            return
        }
        self.updateUserProfile(fullName: fullName, email: email)
    }

    func validateParams(fullName: String, email: String) -> (success:Bool,message:String){
        if fullName.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your full name")
        }
        if !CommonClass.validateEmail(email){
            return (false,"Please enter a valid email")
        }
        return (true,"")
    }

    func updateUserProfile(fullName: String, email:String){
        AppSettings.shared.showLoader(withStatus: "Updating..")
        LoginService.sharedInstance.updateProfileWith(fullName, email: email, attachmentImage: self.userImage) { (success, resUser, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let aUser = resUser{
                    self.user = aUser
                    self.setupUserDetails()
                    self.delegate?.editProfileViewController(viewController: self, didUpdateProfile: aUser)
                    self.navigationController?.pop(false)
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

}

extension EditProfileViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showAlertToChooseAttachmentOption(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery", style: .default)
        { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)

        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String :Any]){
        if let tempImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.userImage = tempImage
            self.userIcon.image = self.userImage
        }else if let tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.userImage = tempImage
            self.userIcon.image = self.userImage
        }
        picker.dismiss(animated: true) {}
    }
}

