//
//  MyRidesViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 21/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import SDWebImage

class MyRidesViewController: UIViewController {
    @IBOutlet weak var historyTableView: UITableView!
    var user : User!
    var ridesArray = [Ride]()
    var pageNumber = 1
    var totalRecords = 0
    var recordsPerPage = 15
    var isNewDataLoading = false
    var canLoadMore = true

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor(red: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(rideDidUpdate(_:)), name: .RIDE_UPDATED_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(rideDidCancelledByDriver(_:)), name: .RIDE_CANCELLED_BY_DRIVER_NOTIFICATION, object: nil)


        self.user = User.loadSavedUser()
        self.historyTableView.addSubview(refreshControl)
        self.historyTableView.backgroundColor = UIColor(red: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        self.historyTableView.backgroundView?.backgroundColor = UIColor(red: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)

        self.historyTableView.register(UINib(nibName: "InCompletedRideCell", bundle: nil), forCellReuseIdentifier:"InCompletedRideCell")
        self.historyTableView.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier:"NoDataCell")

        self.historyTableView.register(UINib(nibName: "CompletedRideCell", bundle: nil), forCellReuseIdentifier:"CompletedRideCell")
        historyTableView.dataSource = self
        historyTableView.delegate = self
        self.loadRideHistoryFor(self.pageNumber, perPage: self.recordsPerPage)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadRideHistoryFor(_ pageNumber:Int, perPage:Int) {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }

        self.isNewDataLoading = true
        if pageNumber == 1{
            ridesArray.removeAll()
            historyTableView.reloadData()
        }
        if pageNumber > 1{
            self.historyTableView.addFooterSpinner()
        }
        RideService.sharedInstance.getRideHistoryFromServer(self.user.ID, pageNumber: pageNumber, perPage: perPage) { (success, resRides,resTotalRecords, message)  in
            self.isNewDataLoading = false
            self.totalRecords = resTotalRecords
            self.historyTableView.removeFooterSpinner()
            if success{
                if let newRideArray = resRides{
                    if newRideArray.count == 0{
                        if self.pageNumber > 1{
                            self.pageNumber = self.pageNumber - 1
                        }
                    }
                    self.ridesArray.append(contentsOf: newRideArray)
                }else{
                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
            self.historyTableView.reloadData()
        }
    }

    func loadNextBatch(indexPath:IndexPath) {
        if self.ridesArray.count >= self.totalRecords {
            self.historyTableView.tableFooterView = nil
            return
        }
        if ridesArray.count >= self.recordsPerPage && canLoadMore {
            if !isNewDataLoading && ((ridesArray.count - indexPath.row) <= self.recordsPerPage/2){
                if AppSettings.isConnectedToNetwork{
                    isNewDataLoading = true
                    self.pageNumber+=1
                    self.loadRideHistoryFor(self.pageNumber, perPage: self.recordsPerPage)
                }
            }
        }
    }

    func openRideTracking(ride:Ride){
        let rideTrackingVC = AppStoryboard.History.viewController(RideTrackingViewController.self)
        rideTrackingVC.ride = ride
        rideTrackingVC.fromHistory = true
        self.navigationController?.pushViewController(rideTrackingVC, animated: true)
    }

    func showBill(ride:Ride){
        let billingVC = AppStoryboard.Billing.viewController(BillViewController.self)
        billingVC.ride = ride
        billingVC.fromHistory = true
        self.navigationController?.pushViewController(billingVC, animated: true)
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if !AppSettings.isConnectedToNetwork{
            refreshControl.endRefreshing()
            return
        }
        self.pageNumber = 1
        self.isNewDataLoading = true
        self.ridesArray.removeAll()
        refreshControl.endRefreshing()
        self.historyTableView.reloadData()
        RideService.sharedInstance.getRideHistoryFromServer(self.user.ID, pageNumber: pageNumber, perPage: self.recordsPerPage) { (success, resRides,resTotalRecords, message)  in
            self.isNewDataLoading = false
            self.totalRecords = resTotalRecords
            self.ridesArray.removeAll()
            refreshControl.endRefreshing()
            if success{
                if let someRides = resRides{
                    self.ridesArray.append(contentsOf: someRides)
                }
                self.historyTableView.reloadData()
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                self.historyTableView.reloadData()
            }
        }

    }

}

extension MyRidesViewController:RideScheduledViewControllerDelegate{
    func close(viewController: RideScheduledViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }

//    func showRideScheduled() -> Void {
//        let rideScheduledVC = AppStoryboard.Booking.viewController(RideScheduledViewController.self)
//        rideScheduledVC.delegate = self
//        self.present(rideScheduledVC, animated: false, completion: {
//        })
//    }

    @IBAction func onClickMenu(_ sender: UIBarButtonItem){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
}




extension MyRidesViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.ridesArray.count == 0) ? 1 : self.ridesArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (self.ridesArray.count == 0) ? 120 : self.getHeight(forRowAt: indexPath)
    }

    func getHeight(forRowAt indexPath:IndexPath) -> CGFloat{
        let ride = ridesArray[indexPath.row]
        let currentStatus = ride.getCurrentStatus()
        var height: CGFloat = 103
        if currentStatus == .completed{
            height = 103
        }else{
            height = 118
        }
        return height
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.ridesArray.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = isNewDataLoading ? "Loading.." : "No ride found\nPull down to refresh"
            return cell
        }else{
            let ride = ridesArray[indexPath.row]
            let currentStatus = ride.getCurrentStatus()
            var cell : UITableViewCell!
            switch currentStatus{
            case .cancelled:
                cell = self.cancelledRideCell(tableView, rowForIndexPath: indexPath, withRide: ride)
            case .completed:
                cell = self.completedRideCell(tableView, rowForIndexPath: indexPath, withRide: ride)
            case .paymentPending:
                cell = self.paymentPendingRideCell(tableView, rowForIndexPath: indexPath, withRide: ride)
            case .onGoing:
                cell = self.onGoingRideCell(tableView, rowForIndexPath: indexPath, withRide: ride)
            case .scheduled:
                cell = self.scheduledRideCell(tableView, rowForIndexPath: indexPath, withRide: ride)
            }

            DispatchQueue.main.async {
                if self.ridesArray.count < self.totalRecords{
                    self.loadNextBatch(indexPath: indexPath)
                }
            }
            return cell
        }
    }


    func cancelledRideCell(_ tableView:UITableView, rowForIndexPath indexPath:IndexPath, withRide ride: Ride)-> InCompletedRideCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "InCompletedRideCell", for: indexPath) as! InCompletedRideCell
        cell.dateTimeLabel.text = (ride.rideType == .instant) ? ride.createdAt : ride.scheduledAt
        cell.carCategoryLabel.text = ride.carCategory.categoryName
        cell.crnLabel.text = "CRN\(ride.ID)"

        if ride.driver.ID != ""{
            cell.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))
        }else{
            cell.driverIcon.image = UIImage(named: ride.carCategory.categoryName.lowercased()+"_unsel")
        }
        cell.amountLabel.text = " "
        cell.pickUpAddressLabel.text = ride.startLocation
        cell.dropAddressLabel.text = ride.endLocation
        CommonClass.makeViewCircular(cell.driverIcon, borderColor: appColor.green, borderWidth: 1)
        cell.rideStatusLabel.text = "CANCELLED"
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }
    func onGoingRideCell(_ tableView:UITableView, rowForIndexPath indexPath:IndexPath, withRide ride: Ride)-> InCompletedRideCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "InCompletedRideCell", for: indexPath) as! InCompletedRideCell
        cell.dateTimeLabel.text = (ride.rideType == .instant) ? ride.createdAt : ride.scheduledAt
        cell.carCategoryLabel.text = ride.carCategory.categoryName
        cell.crnLabel.text = "CRN\(ride.ID)"

        if ride.driver.ID != ""{
            cell.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))
        }else{
            cell.driverIcon.image = UIImage(named: ride.carCategory.categoryName.lowercased()+"_unsel")
        }
        cell.amountLabel.text = "$"+String(format: "%.2f",ride.estimatedCost.totalPrice)
        cell.pickUpAddressLabel.text = ride.startLocation
        cell.dropAddressLabel.text = ride.endLocation
        CommonClass.makeViewCircular(cell.driverIcon, borderColor: appColor.green, borderWidth: 1)
        cell.rideStatusLabel.text = "ON GOING"
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }
    func paymentPendingRideCell(_ tableView:UITableView, rowForIndexPath indexPath :IndexPath, withRide ride: Ride)-> InCompletedRideCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "InCompletedRideCell", for: indexPath) as! InCompletedRideCell
        cell.dateTimeLabel.text = (ride.rideType == .instant) ? ride.createdAt : ride.scheduledAt
        cell.carCategoryLabel.text = ride.carCategory.categoryName
        cell.crnLabel.text = "CRN\(ride.ID)"
        if ride.driver.ID != ""{
            cell.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))
        }else{
            cell.driverIcon.image = UIImage(named: ride.carCategory.categoryName.lowercased()+"_unsel")
        }
        cell.amountLabel.text = "$"+String(format: "%.2f",ride.cost.totalPrice)
        cell.pickUpAddressLabel.text = ride.startLocation
        cell.dropAddressLabel.text = ride.dropLocation
        CommonClass.makeViewCircular(cell.driverIcon, borderColor: appColor.green, borderWidth: 1)
        cell.rideStatusLabel.text = "PAYMENT PENDING"
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    func scheduledRideCell(_ tableView:UITableView, rowForIndexPath indexPath :IndexPath, withRide ride: Ride)-> InCompletedRideCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "InCompletedRideCell", for: indexPath) as! InCompletedRideCell
        cell.dateTimeLabel.text = (ride.rideType == .instant) ? ride.createdAt : ride.scheduledAt
        cell.carCategoryLabel.text = ride.carCategory.categoryName
        cell.crnLabel.text = "CRN\(ride.ID)"
        if ride.driver.ID != ""{
            cell.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))
        }else{
            
            cell.driverIcon.image = UIImage(named: "placeholder")
        }
        cell.amountLabel.text = "$"+String(format: "%.2f",ride.estimatedCost.totalPrice)
        cell.pickUpAddressLabel.text = ride.startLocation
        cell.dropAddressLabel.text = ride.endLocation
        CommonClass.makeViewCircular(cell.driverIcon, borderColor: appColor.green, borderWidth: 1)
//        cell.rideStatusLabel.text = "SCHEDULED"
        
        if ride.scheduleTime < Date().toMillis() && ride.requestStatus == false{
            cell.rideStatusLabel.text = "NOT ASSIGNED DRIVER"
            
        }else{
            
            cell.rideStatusLabel.text = "SCHEDULED"
            
        }
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    func completedRideCell(_ tableView:UITableView, rowForIndexPath indexPath :IndexPath, withRide ride: Ride)-> CompletedRideCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompletedRideCell", for: indexPath) as! CompletedRideCell
        cell.dateTimeLabel.text = (ride.rideType == .instant) ? ride.createdAt : ride.scheduledAt
        cell.carCategoryLabel.text = ride.carCategory.categoryName
        cell.crnLabel.text = "CRN\(ride.ID)"
        if ride.driver.ID != ""{
            cell.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))
        }else{
            cell.driverIcon.image = UIImage(named: ride.carCategory.categoryName.lowercased()+"_unsel")
        }
        cell.amountLabel.text = "$"+String(format: "%.2f",ride.cost.totalPrice)
        cell.pickUpAddressLabel.text = ride.startLocation
        cell.dropAddressLabel.text = ride.dropLocation
        CommonClass.makeViewCircular(cell.driverIcon, borderColor: appColor.green, borderWidth: 1)
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ridesArray.count == 0{return}
        let ride = ridesArray[indexPath.row]
        let currentStatus = ride.getCurrentStatus()
        switch currentStatus{
        case .cancelled:
            if ride.rideType == .scheduled{
                self.openScheduledRideDetails(ride: ride)
            }else{
                self.openRideDetails(ride: ride)
            }
        case .completed:
            self.openRideDetails(ride: ride)
        case .paymentPending:
            self.showBill(ride: ride)
        case .onGoing:
            self.openRideTracking(ride: ride)
        case .scheduled:
            self.openScheduledRideDetails(ride: ride)
        } 
    }

    func openPaymentScreen(ride:Ride){
        let billingVC = AppStoryboard.Billing.viewController(BillViewController.self)
        billingVC.ride = ride
        billingVC.fromHistory = true
        self.navigationController?.pushViewController(billingVC, animated: true)
    }

    func openRideDetails(ride:Ride){
        let rideDetailsVC = AppStoryboard.History.viewController(RideDetailsViewController.self)
        rideDetailsVC.ride = ride
        self.navigationController?.pushViewController(rideDetailsVC, animated: true)
    }
    func openScheduledRideDetails(ride:Ride){
        let rideDetailsVC = AppStoryboard.History.viewController(ScheduledRideDetailsViewController.self)
        rideDetailsVC.ride = ride
        self.navigationController?.pushViewController(rideDetailsVC, animated: true)
    }

    @objc func rideDidUpdate(_ notification:Notification) {
        if let userInfo = notification.userInfo{
            if let ride = userInfo["ride"] as? Ride{
                for index in 0..<self.ridesArray.count{
                    if ride.ID == self.ridesArray[index].ID{
                        self.ridesArray[index] = ride
                        break
                    }
                }
                self.historyTableView.reloadData()
            }
        }
    }

    @objc func rideDidCancelledByDriver(_ notification:Notification) {
        if let userInfo = notification.userInfo{
            if let ride = userInfo["ride"] as? Ride{
                for index in 0..<self.ridesArray.count{
                    if ride.ID == self.ridesArray[index].ID{
                        self.ridesArray.remove(at: index)
                        break
                    }
                }
                self.historyTableView.reloadData()
            }
        }
    }
}







