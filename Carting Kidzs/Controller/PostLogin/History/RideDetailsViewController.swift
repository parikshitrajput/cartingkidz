//
//  RideDetailsViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 21/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import SDWebImage

class RideDetailsViewController: UIViewController {
    let heights:[CGFloat] = [190,75,70,70,95,190]
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mailButton: UIButton!
    @IBOutlet weak var mailLable: UILabel!

    var ride: Ride!
    var user : User!
    var navigationTitleView: RideDetailNavigationTitleView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.registerCells()
        self.setUpNavigationTitle()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
        let status = ride.getCurrentStatus()
        if status == .completed{
            mailLable.textColor = appColor.red
            mailButton.setImage(#imageLiteral(resourceName: "mail"), for: UIControlState())
        }else{
            mailLable.textColor = appColor.gray
            mailButton.setImage(#imageLiteral(resourceName: "mailicon"), for: UIControlState())
        }
    }

    func setUpNavigationTitle(){
        self.navigationTitleView = RideDetailNavigationTitleView.instanceFromNib()
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.width-80, height: 44)
        self.navigationTitleView.frame = frame
        self.navigationTitleView.crnLabel.text = "CRN"+self.ride.ID
        self.navigationTitleView.titleLabel.text = (self.ride.rideType == .instant) ? self.ride.createdAt : self.ride.scheduledAt
        self.navigationItem.titleView = self.navigationTitleView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func registerCells(){
        self.tableView.register(UINib(nibName: "RidePaymentDetailsCell", bundle: nil), forCellReuseIdentifier:"RidePaymentDetailsCell")
        self.tableView.register(UINib(nibName: "RideAddressDetailsCell", bundle: nil), forCellReuseIdentifier:"RideAddressDetailsCell")
        self.tableView.register(UINib(nibName: "RideDistanceAndFareCell", bundle: nil), forCellReuseIdentifier:"RideDistanceAndFareCell")
        self.tableView.register(UINib(nibName: "CarNameAndCategoryCell", bundle: nil), forCellReuseIdentifier:"CarNameAndCategoryCell")
        self.tableView.register(UINib(nibName: "RideDetailsDriverCell", bundle: nil), forCellReuseIdentifier:"RideDetailsDriverCell")
        self.tableView.register(UINib(nibName: "RideMapCell", bundle: nil), forCellReuseIdentifier:"RideMapCell")
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSupport(_ sender: UIButton){
        let supportVC = AppStoryboard.Support.viewController(SupportsViewController.self)
        supportVC.isFromMenu = false
        supportVC.ride = self.ride
        self.navigationController?.pushViewController(supportVC, animated: true)
    }

    @IBAction func onClickMailInvoice(_ sender: UIButton){
        let status = ride.getCurrentStatus()
        if status != .completed{return}
        if self.user.email.count == 0{
            self.showAlertForEmail()
        }else{
            self.sendInvoice(ride: self.ride, email: self.user.email)
        }
    }

    func showAlertForEmail(){
        let alertController = UIAlertController(title: "Enter Email Address", message: "", preferredStyle: .alert)

        let sendAction = UIAlertAction(title: "SEND", style: .default) { (aciton) in

            guard let emailTextField = alertController.textFields?.first else{
                return
            }
            guard let email = emailTextField.text else{
                return
            }

            if !CommonClass.isValidEmailAddress(email.trimmingCharacters(in: .whitespaces)){
                NKToastHelper.sharedInstance.showErrorAlert(nil, message: warningMessage.invalidEmailAddress.rawValue)
                return
            }
            self.sendInvoice(ride: self.ride, email: email)

        }

        let cancelAction = UIAlertAction(title: "CANCEL", style: .cancel) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }

        alertController.addTextField { (textField) in
            textField.placeholder = "Email"
            textField.keyboardType = .emailAddress
        }

        alertController.addAction(sendAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }


    func sendInvoice(ride:Ride,email:String){
        AppSettings.shared.showLoader(withStatus: "Sending..")
        ride.mailInvoice(email){ (success, message) in
            AppSettings.shared.hideLoader()
            NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
        }
    }
}

extension RideDetailsViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ride.isUserCancelled ? self.heights.count-1 : self.heights.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.heights[indexPath.row]
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RideMapCell", for: indexPath) as! RideMapCell
                cell.mapImageView.setIndicatorStyle(.gray)
                cell.mapImageView.setShowActivityIndicator(true)
                let height = self.heights[indexPath.row]-4
                let width = self.view.frame.width
                cell.mapImageView.sd_setImage(with: ride.getRideMapUrl(CGSize(width:width,height:height)))
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RideDetailsDriverCell", for: indexPath) as! RideDetailsDriverCell
            cell.cancelledLabel.text = ride.isUserCancelled ? "CANCELLED" : "You Rated:"
            cell.cancelledLabel.textColor = ride.isUserCancelled ? appColor.red : UIColor.black
            cell.ratingView.isHidden = ride.isUserCancelled
            cell.ratingView.delegate = self
            cell.ratingView.isUserInteractionEnabled = (self.ride.rating == 0.0)
            
            cell.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))

            cell.driverName.text = ride.driver.fullName.capitalized
            cell.ratingView.rating = Float(ride.rating)
            CommonClass.makeViewCircular(cell.driverIcon, borderColor: appColor.green, borderWidth: 1)
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarNameAndCategoryCell", for: indexPath) as! CarNameAndCategoryCell

            cell.carIcon.sd_setImage(with: URL(string:ride.car.image) ?? URL(string: api.base.url())!, placeholderImage: UIImage(named: ride.carCategory.categoryName.lowercased()+"_unsel"))
            //cell.categoryLabel.text = self.ride.carCategory.categoryName.capitalized
            cell.categoryLabel.text = self.ride.car.name.capitalized
            cell.carName.text = self.ride.carCategory.categoryName.capitalized
            CommonClass.makeViewCircular(cell.carIcon, borderColor: appColor.green, borderWidth: 1)
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RideDistanceAndFareCell", for: indexPath) as! RideDistanceAndFareCell
//            cell.distanceLabel.text = self.ride.isUserCancelled ? " " : ((ride.cost.distance >= 1000.0) ? String(format: "%.2f Kms",Float(ride.cost.distance/1000)) : String(format: "%.0f Mtr",ride.cost.distance))
            cell.distanceLabel.text = self.ride.isUserCancelled ? " " : String(format: "%.2f miles",Float(ride.cost.distance/1609.34))

            cell.fareLabel.text = Rs+String(format: "%.2f", ride.cost.totalPrice)
            cell.timeLabel.text = self.ride.isUserCancelled ? " " : self.timeStringFromRideTime(seconds: ride.cost.time)
            return cell
        }else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RideAddressDetailsCell", for: indexPath) as! RideAddressDetailsCell
            cell.pickUpTimeLabel.text = (self.ride.startTime.count == 0) ? " " : self.ride.startTime
            cell.dropOffTimeLabel.text = (self.ride.endTime.count == 0) ? " " : self.ride.endTime
            cell.pickUpAddressLabel.text = self.ride.startLocation
            cell.dropOffAddressLabel.text = ride.isUserCancelled ? self.ride.endLocation : self.ride.dropLocation
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RidePaymentDetailsCell", for: indexPath) as! RidePaymentDetailsCell
            cell.paymentModeLabel.text = ride.paymentStatus ? "Online" : "Cash"
            cell.totalBillLabel.text = Rs+String(format: "%.2f", ride.cost.totalPrice)
            cell.yourTripLabel.text = Rs+String(format: "%.2f", ride.cost.totalPrice)
            let actualCost = (100*ride.cost.totalPrice)/(100+tax)
            let includingTaxes = ride.cost.totalPrice - actualCost
            cell.taxesLabel.text = Rs+String(format: "%.2f", includingTaxes)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let mycell = cell as? CarNameAndCategoryCell{
            mycell.setNeedsLayout()
            mycell.layoutIfNeeded()
        }else if let mycell = cell as? RideDetailsDriverCell{
            mycell.setNeedsLayout()
            mycell.layoutIfNeeded()
        }else if let mycell = cell as? RideAddressDetailsCell{
            mycell.setNeedsLayout()
            mycell.layoutIfNeeded()
            mycell.dotedImageView.image = UIImage.drawDottedImage(width: 3, height: mycell.dotedImageView.frame.size.height, color: UIColor.darkGray)
        }

    }




    func timeStringFromRideTime(seconds : Double) -> String {
        var timeStr = ""
        let secondsInAMinute : Int = 60
        let minutesInAnHour : Int = 60
        let secondsInAnHour : Int = 3600

        let secs = Int(seconds)
        if secs >= 3600{
            let hrs = Int(secs/secondsInAnHour)
            let remSec = secs%secondsInAnHour
            let mms = remSec%minutesInAnHour
            var hhStr = ""
            var mmStr = ""
            if hrs > 0{
                hhStr = "\(hrs) Hrs "
            }
            if mms > 0{
                mmStr = "\(mms) Min"
            }
            timeStr = hhStr+mmStr
        }else if secs >= 60{
            let mms = Int(secs/secondsInAMinute)
            let scs = secs%secondsInAMinute
            var mmStr = ""
            var scStr = ""
            if mms > 0{
                mmStr = "\(mms) Min "
            }
            if scs > 0{
                scStr = "\(scs) Sec"
            }
            timeStr = mmStr+scStr
        }else{
            timeStr = String(format: "%.0f Sec",seconds)
        }
        return timeStr
    }
}

extension RideDetailsViewController:NKFloatRatingViewDelegate{
    func floatRatingView(ratingView: NKFloatRatingView, didUpdate rating: Float) {
        self.rateDriver(rideId: self.ride.ID, rating: Double(rating), userID: self.user.ID)
    }
    func rateDriver(rideId:String,rating: Double,userID:String) -> Void {
        AppSettings.shared.showLoader(withStatus: "Sending..")
        RideService.sharedInstance.rateDriverWithRide(rideId, rating: rating, userID:userID) { (success,responseRide,message)  in
            AppSettings.shared.hideLoader()
            if success{
                if let aRide = responseRide{
                    self.ride = aRide
                    self.tableView.reloadData()
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: "Thanks for your response. It'll help us to improve for experience with us")
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }
}
