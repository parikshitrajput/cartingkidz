//
//  RideTrackingViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 21/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase
import Alamofire
import SwiftyJSON

class RideTrackingViewController: UIViewController {
    @IBOutlet weak var dottedImageView: UIImageView!
    @IBOutlet weak var fromAddressLabel: UILabel!
    @IBOutlet weak var toAddressLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!

    //outlets for driver view
    @IBOutlet weak var callDriverButton: UIButton!
    @IBOutlet weak var cancelRideButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var ratingView: NKFloatRatingView!

    @IBOutlet weak var otpLabel: UILabel!
    @IBOutlet weak var driverIcon: UIImageView!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var carNumberLabel: UILabel!
    @IBOutlet weak var driverRatingLabel: UILabel!
//    var valueAnimator: ValueAnimator!
    var ride: Ride!
    var fromHistory:Bool = false
    var driverMarker:DriverMarker!
    var timeMarker:TimeMarker!
//    var carGroupRef: DatabaseReference!
    var myTimer: Timer?
//    fileprivate lazy var storageRef: StorageReference = Storage.storage().reference(forURL: api.firebaseDatabase.url())
    
    var oldLocationCoordinate2D : CLLocationCoordinate2D!
    var trackRef: DatabaseReference?
    
    //handles for track group
    private var addedInTrackRefHandle: DatabaseHandle?
    private var updateInTrackRefHandle: DatabaseHandle?
    private var removeFromTrackRefHandle: DatabaseHandle?
    
    
    //handles for direct path
    private var startRefHandle: DatabaseHandle?
    private var cancelRefHandle: DatabaseHandle?
    private var reachedRefHandle: DatabaseHandle?
    private var endedRefHandle: DatabaseHandle?



    override func viewDidLoad() {
        super.viewDidLoad()
        
        let desiredViewController = self.navigationController?.viewControllers.last
        
        print(desiredViewController as Any)
        
        self.oldLocationCoordinate2D = CLLocationCoordinate2D(latitude: ride.driver.location.latitude, longitude: ride.driver.location.longitude)
        self.mapView.mapStyle(withFilename: "mapStyle", andType: "json")
        self.mapView.isTrafficEnabled = true
        self.setRideDetails(ride: self.ride)
        self.setTitleForTracking(ride: ride)
        self.decorateView()
        self.addDropOffMarker(ride: self.ride)
        self.resetBoundOfMap(ride: self.ride)
        self.addDriverMarker(on: oldLocationCoordinate2D,and: "Driver_\(ride.driver.ID)")
        self.upDateMarker(ride: self.ride)
        self.trackDriverReached()
        self.trackRideStarted()
        self.trackRideEnded()
        self.mapView.setMinZoom(mapZoomLevel.min, maxZoom: mapZoomLevel.max)
        if self.ride.isStarted || self.ride.isDriverReached{
            self.addPickUpMarker(ride: self.ride)
        }else{
            self.handleTimeUpdates()
//            self.timer = Timer.scheduledTimer(timeInterval: 59, target: self, selector: #selector(handleTimeUpdates), userInfo: nil, repeats: true)
        }
    }

    

    deinit {
        self.deleteTimer(timer: self.myTimer)
        NotificationCenter.default.removeObserver(self)
    }

    func deleteTimer(timer:Timer?){
        guard let myTimer = timer else {
            return
        }
        myTimer.invalidate()
        self.myTimer = nil
    }
    func showBill(ride:Ride){
        removeAllFirebaseHandles()
        AppSettings.shared.hideLoader()
        if (UIApplication.shared.keyWindow?.rootViewController?.topViewController is BillViewController ) {

            return
        }else{
            if self.navigationController == nil {
                let billingVC = AppStoryboard.Billing.viewController(BillViewController.self)
                billingVC.ride = ride
                billingVC.fromHistory = false
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: billingVC, withSlideOutAnimation: false, andCompletion: {
                    
                })
            }else{
                let billingVC = AppStoryboard.Billing.viewController(BillViewController.self)
                billingVC.ride = ride
                billingVC.fromHistory = false
                self.navigationController?.pushViewController(billingVC, animated: true)
            }
        }
    }
    
    @objc func getRideBookingDetailsAndProceedToBill(){
        if !AppSettings.isConnectedToNetwork {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        AppSettings.shared.showLoader(withStatus: "Getting Bill..")
        RideService.sharedInstance.getRideDetails(self.ride.ID) { (success,resRide,message)  in
            if success{
                if let ride = resRide{
                    self.ride = ride
                    NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":ride])
                    self.showBill(ride: ride)
                }else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
                }
            }else{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
            }
        }
    }
    func setRideDetails(ride:Ride){
        self.fromAddressLabel.text = self.ride.startLocation
        self.toAddressLabel.text = self.ride.endLocation

        //driver details
        self.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))
        self.driverNameLabel.text = ride.driver.fullName.capitalized
        self.carNameLabel.text = ride.car.name.capitalized
        self.carNumberLabel.text = ride.car.number.uppercased()
        self.otpLabel.text = "OTP : \(ride.OTP)"
        self.driverRatingLabel.text = String(format: "%0.1f",ride.driver.rating)
        self.ratingView.rating = Float(ride.driver.rating)
    }

    func setTitleForTracking(ride:Ride){
        if !ride.isDriverReached && !ride.isStarted{
            self.title = "Pick up is Arriving".uppercased()
        }
        if ride.isDriverReached{
            self.title = "Pick up Arrived".uppercased()
        }
        if ride.isStarted{
            self.title = "Enjoy Your Ride".uppercased()
        }

    }

    func decorateView(){
        CommonClass.makeViewCircularWithCornerRadius(otpLabel, borderColor: .clear, borderWidth: 0, cornerRadius: 4)
        CommonClass.makeViewCircularWithCornerRadius(driverIcon, borderColor: appColor.green, borderWidth: 1, cornerRadius: driverIcon.frame.size.width/6)
        CommonClass.makeViewCircularWithCornerRadius(callDriverButton, borderColor: .clear, borderWidth: 0, cornerRadius: 4)
        CommonClass.makeViewCircularWithCornerRadius(cancelRideButton, borderColor: .clear, borderWidth: 0, cornerRadius: 4)
        CommonClass.makeViewCircularWithCornerRadius(moreButton, borderColor: .clear, borderWidth: 0, cornerRadius: 4)

    }
    
    @IBAction func onClickCurrentLocation(_ sender: UIButton){
        guard let location = self.mapView.myLocation else {return}
        self.mapView.animate(toLocation: location.coordinate)
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.deleteTimer(timer: self.myTimer)
        if fromHistory{
            self.navigationController?.pop(true)
        }else{
            self.navigationController?.popToRoot(true)
        }
    }

    override func viewDidLayoutSubviews() {
        self.dottedImageView.image = UIImage.drawDottedImage(width: 3, height: dottedImageView.frame.size.height, color: UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 1.0))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        if !self.ride.isDriverReached{
            self.myTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(handleTimeUpdatesWithTimer), userInfo: nil, repeats: true)
        }
        
        self.trackCancelledByDriver()
        guard let vcs = self.navigationController?.viewControllers else{return}
        let filteredVC = vcs.filter { (vc) -> Bool in
            return (vc is HomeViewController)
        }
        
        if let homeVC = filteredVC.first as? HomeViewController{
            homeVC.removeFirebaseHandlers()
        }
    }
    
    func removeAllFirebaseHandles(){
        if let removeFromTrackHandle = self.removeFromTrackRefHandle{
            self.trackRef?.removeObserver(withHandle: removeFromTrackHandle)
        }
        if let addToTrackHandle = self.addedInTrackRefHandle{
            self.trackRef?.removeObserver(withHandle: addToTrackHandle)
        }
        if let updateInTrackHandle = self.updateInTrackRefHandle{
            self.trackRef?.removeObserver(withHandle: updateInTrackHandle)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
//        self.removePath()
        self.deleteTimer(timer: self.myTimer)
        self.removeAllFirebaseHandles()
        guard let timer = self.myTimer else {
            return
        }
        timer.invalidate()
        self.myTimer = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickCallDriver(_ sender: UIButton){
        self.dialPhone(phoneNumber: "+"+self.ride.driver.contact)
    }

    func dialPhone(phoneNumber:String){
        let dial = "telprompt://\(phoneNumber)"
        self.showPhoneApp(dial: dial)
    }

    func showPhoneApp(dial: String)  {
        let url = URL(string:dial)!
        if (UIApplication.shared.canOpenURL(url)){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }else{
            NKToastHelper.sharedInstance.showErrorAlert(nil, message: "Cann't able to call right now\r\nPlease try later!")
        }
    }

    @IBAction func onClickCancelRide(_ sender: UIButton){
        if self.ride.isStarted{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Sorry! You cann't cancel ride once started. If you wish to end the ride, please ask driver to end the ride.")
            return
        }
        self.openSelectCancelReason()
    }

    func openSelectCancelReason() {
        let reasonVC = AppStoryboard.History.viewController(SelectCancelReasonViewController.self)
        reasonVC.delegate = self
        reasonVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        let nav = UINavigationController(rootViewController: reasonVC)
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }

    @IBAction func onClickMore(_ sender: UIButton){
        NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.functionalityPending.rawValue)
    }

    func cancelRequest(rideID:String,reason:String,completionBlock: @escaping (_ finished : Bool, _ message:String)->Void){
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        RideService.sharedInstance.cancelRide(rideID, reason: reason) { (success, resRide, message) in
            AppSettings.shared.hideLoader()
            if success{
                self.ride.isUserCancelled = true
                NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
            }
            completionBlock(success,message)
        }
    }
}
//MARK:- makers and mapview handling
extension RideTrackingViewController: SelectCancelReasonViewControllerDelegate{
    func cancelRide(_ viewController: SelectCancelReasonViewController, didCancelledRideWithReason reason: String) {
        viewController.dismiss(animated: false, completion: nil)
        self.deleteTimer(timer: self.myTimer)
        self.cancelRequest(rideID: self.ride.ID, reason: reason) { (cancelled,message) in
            NKToastHelper.sharedInstance.showErrorAlert(nil, message: message, completionBlock: {
                self.deleteTimer(timer: self.myTimer)
                if self.fromHistory{
                    self.navigationController?.pop(true)
                }else{
                    self.navigationController?.popToRoot(true)
                }
            })
        }
    }
    func addPickUpMarker(ride:Ride) -> Void {
        let coordinate = CLLocationCoordinate2D(latitude: ride.startLatitude, longitude: ride.startLongitude)
        self.addMarkerforCoordinate(coordinate: coordinate, image: #imageLiteral(resourceName: "greenPin"), address: ride.startLocation)
        self.removeTimeMarker()
    }
    func addDropOffMarker(ride:Ride) -> Void {
        let coordinate = CLLocationCoordinate2D(latitude: ride.endLatitude, longitude: ride.endLongitude)
        self.addMarkerforCoordinate(coordinate: coordinate, image: #imageLiteral(resourceName: "redPin"), address: ride.endLocation)
    }

    func addMarkerforCoordinate(coordinate : CLLocationCoordinate2D?,image: UIImage,address: String) {
        if let position = coordinate{
            let driverMarker = GMSMarker(position: position)
            driverMarker.icon = image
            driverMarker.isTappable = true
            driverMarker.title = address
            driverMarker.map = self.mapView;
        }
    }


    func resetBoundOfMap(ride:Ride) -> Void {
        let path = GMSMutablePath()
        let pickCoordinate = CLLocationCoordinate2D(latitude: ride.startLatitude, longitude: ride.startLongitude)
        let dropCoordinate = CLLocationCoordinate2D(latitude: ride.endLatitude, longitude: ride.endLongitude)
        path.add(pickCoordinate)
        path.add(dropCoordinate)
        let bounds = GMSCoordinateBounds(path: path)
        let camera = self.mapView.camera(for: bounds, insets:UIEdgeInsets(top: 40, left: 15, bottom: 15, right: 15))
        self.mapView.camera = camera!;
        self.mapView.setMinZoom(mapZoomLevel.min, maxZoom: mapZoomLevel.max)
    }
}

//MARK:- Notification Handling
extension RideTrackingViewController{
    
    @objc func handleTimeUpdates(){
        //        if self.ride.isDriverReached || self.ride.isStarted{
        //            self.deleteTimer(timer: self.myTimer)
        //            return
        //        }
        let fromCoordinate = CLLocationCoordinate2D(latitude: ride.startLatitude, longitude: ride.startLongitude)
        self.calculateDriverArrivingTime(from: self.oldLocationCoordinate2D, to: fromCoordinate)
    }
    
    @objc func handleTimeUpdatesWithTimer(){
        if self.ride.isDriverReached || self.ride.isStarted{
            guard let timer = self.myTimer else {
                return
            }
            timer.invalidate()
            self.myTimer = nil
            return
        }
        let fromCoordinate = CLLocationCoordinate2D(latitude: ride.startLatitude, longitude: ride.startLongitude)
        self.calculateDriverArrivingTime(from: self.oldLocationCoordinate2D, to: fromCoordinate)
    }
    
    
    func calculateDriverArrivingTime(from driverLocation:CLLocationCoordinate2D,to pickUpCoordinate: CLLocationCoordinate2D){
        self.getTravelTimeAndDistance(from: pickUpCoordinate, to: driverLocation) { (duration) in
            self.addTimeMarker(time: duration, position: pickUpCoordinate)
        }
    }
    
    func addTimeMarker(time:String,position:CLLocationCoordinate2D){
        if timeMarker != nil{
            timeMarker.map = nil
        }
        self.timeMarker = TimeMarker(time: time)
        self.timeMarker.position = position
        if isDebugEnabled{
            self.timeMarker.title = "Pick location"
        }
        self.timeMarker.zIndex = 10
        self.timeMarker.map = self.mapView
    }
    
    func removeTimeMarker(){
        if timeMarker != nil{
            timeMarker.map = nil
            timeMarker = nil
        }
    }
    
    func getTravelTimeAndDistance(from driverLocation:CLLocationCoordinate2D,to pickUpCoordinate: CLLocationCoordinate2D,completion:@escaping (_ time: String)->Void)->Void{
        
        let origin = "\(driverLocation.latitude),\(driverLocation.longitude)"
        let destination = "\(pickUpCoordinate.latitude),\(pickUpCoordinate.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(origin)&destinations=\(destination)&key=\(GOOGLE_API_KEY)"
        Alamofire.request(url).responseJSON { response in
            do{
                let json = try JSON(data: response.data!)
                let rows = json["rows"].arrayValue
                if rows.count > 0{
                    let arrValues = rows.first!["elements"].arrayValue.first!
                    var duration = "..."
                    if let durationDict = arrValues["duration"].dictionaryObject as [String:AnyObject]?{
                        if let durationString = durationDict["text"] as? String{
                            duration = durationString
                        }
                    }
                    completion(duration)
                }
            }catch{}
        }
    }
    
    //here we goes for firebase handling
    
    func trackCancelledByDriver(){
        let path = "Ride_\(ride.ID)/\(RideTrackingKeys.isCancelledByDriver.rawValue)"
        let ref = Database.database().reference(withPath: path)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            if !snapshot.exists() {
//                self.removePath()
                self.title = "Ride Ended".uppercased()
                self.ride.isDriverReject = true
                self.ride.isUserCancelled = true
                NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "This ride did not find in your on going ride. Please go to your ride history and check the status of this ride", completionBlock: {
                    if let handle = self.cancelRefHandle{
                        ref.removeObserver(withHandle:handle)
                    }
                    if self.fromHistory{
                        self.navigationController?.popToRoot(true)
                    }else{
                        self.navigationController?.popToRoot(true)
                    }
                    guard let timer = self.myTimer else {
                        return
                    }
                    timer.invalidate()
                    self.myTimer = nil
                })
                return
            }
            
            self.cancelRefHandle = ref.observe(.value, with: { (response) in
                if let status = response.value as? Int{
                    if status == 1{
//                        self.removePath()
                        self.title = "Ride Cancelled".uppercased()
                        self.ride.isDriverReject = true
                        self.ride.isUserCancelled = true
                        NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
                        NKToastHelper.sharedInstance.showErrorAlert(self, message: "Your ride is cancelled by driver\nYou can book another ride", completionBlock: {
                            if let handle = self.cancelRefHandle{
                                ref.removeObserver(withHandle:handle)
                            }
                            if self.fromHistory{
                                self.navigationController?.pop(true)
                            }else{
                                self.navigationController?.popToRoot(true)
                            }
                            guard let timer = self.myTimer else {
                                return
                            }
                            timer.invalidate()
                            self.myTimer = nil
                        })
                    }
                }
            })
            
        })
    }
    
    func trackDriverReached(){
        let path = "Ride_\(ride.ID)/\(RideTrackingKeys.isReached.rawValue)"
        let ref = Database.database().reference(withPath: path)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            if !snapshot.exists() { return }
            self.reachedRefHandle = ref.observe(.value, with: { (response) in
                if let status = response.value as? Int{
                    if status == 1{
//                        self.removePath()
                        self.deleteTimer(timer: self.myTimer)
                        self.addPickUpMarker(ride: self.ride)
                        if !self.ride.isDriverReached{
                            AppDelegate.getAppDelegate().playSound()
                        }
                        self.ride.isDriverReached = true
//                        self.removePath()
                        self.setTitleForTracking(ride: self.ride)
                        self.resetBoundOfMap(ride: self.ride)
                        NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
                        if let handle = self.reachedRefHandle{
                            ref.removeObserver(withHandle:handle)
                        }
                        guard let timer = self.myTimer else {
                            return
                        }
                        timer.invalidate()
                        self.myTimer = nil
                    }
                }
            })
        })
    }
    
    func trackRideEnded(){
        let path = "Ride_\(ride.ID)/\(RideTrackingKeys.isEnded.rawValue)"
        let ref = Database.database().reference(withPath: path)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            if !snapshot.exists() { return }
            self.endedRefHandle = ref.observe(.value, with: { (response) in
                if let status = response.value as? Int{
                    self.deleteTimer(timer: self.myTimer)
                    if status == 1{
//                        self.removePath()
                        
                        
//                        let desiredViewController = self.navigationController?.viewControllers.last
//                        print(desiredViewController as Any)
//                        print(self.navigationController?.viewControllers.first as Any)
//                        print(self.navigationController?.visibleViewController as Any)

//                        if (desiredViewController as? BillViewController) != nil{
//                            return
//
//                        }
                        
                        if AppSettings.shared.isPaymentViewController{
                            AppSettings.shared.isPaymentViewController = false
                        }
                        else{
                            AppSettings.shared.isPaymentViewController = false
                        self.perform(#selector(self.getRideBookingDetailsAndProceedToBill), with: nil, afterDelay: 1.5)
                        }
                        if let handle = self.endedRefHandle{
                            ref.removeObserver(withHandle:handle)
                        }
                        self.removeAllFirebaseHandles()
                        guard let timer = self.myTimer else {
                            return
                        }
                        timer.invalidate()
                        self.myTimer = nil
                    }
                }
            })
        })
    }
    
    func trackRideStarted(){
        let path = "Ride_\(ride.ID)/\(RideTrackingKeys.isStarted.rawValue)"
        let ref = Database.database().reference(withPath: path)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            if !snapshot.exists() { return }
            self.startRefHandle = ref.observe(.value, with: { (response) in
                if let status = response.value as? Int{
                    if status == 1{
//                        self.removePath()
                        self.ride.isDriverReached = true
                        self.ride.isStarted = true
                        self.setTitleForTracking(ride: self.ride)
                        self.resetBoundOfMap(ride: self.ride)
                        NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
                        if let handle = self.startRefHandle{
                            ref.removeObserver(withHandle:handle)
                        }
                        guard let timer = self.myTimer else {
                            return
                        }
                        timer.invalidate()
                        self.myTimer = nil
                    }
                }
            })
        })
    }
    
    func upDateMarker(ride:Ride) {
        let path = "Track_\(ride.driver.ID)"
        self.trackRef = Database.database().reference(withPath: path)
        guard let trackFirebaseRef = self.trackRef else {
            return
        }
        
        self.removeAllFirebaseHandles()
        
        self.updateInTrackRefHandle = trackFirebaseRef.observe(.childChanged, with: { (response) in
            if !response.key.lowercased().contains("driver_"){return}
            let driverLocation = response.childSnapshot(forPath: "l")
            let position = self.getPosition(from: driverLocation)
            self.updateDriverMarker(key: response.key, newPosition: position)
            if !self.ride.isDriverReached{
//                self.updateTravelledPath(currentLoc: position)
            }
        })
        self.removeFromTrackRefHandle = trackFirebaseRef.observe(.childRemoved, with: { (response) in
            if !response.key.lowercased().contains("driver_"){return}
            self.removeDriverMarker(key: response.key)
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Something went wrong!", completionBlock: {
                self.removeAllFirebaseHandles()
                
                guard let timer = self.myTimer else {
                    return
                }
                timer.invalidate()
                self.myTimer = nil
                if self.fromHistory{
                    self.navigationController?.pop(true)
                }else{
                    self.navigationController?.popToRoot(true)
                }
            })
        })
        
        self.addedInTrackRefHandle = trackFirebaseRef.observe(.childAdded, with: { (response) in
            if !response.key.lowercased().contains("driver_"){return}
            let driverLocation = response.childSnapshot(forPath: "l")
            let position = self.getPosition(from: driverLocation)
            self.addDriverMarker(on: position,and: response.key)
            if !(self.ride.isDriverReached || self.ride.isStarted){
                self.handleTimeUpdates()
            }
        })
    }
    
    func getPosition(from snapShot: DataSnapshot) -> CLLocationCoordinate2D{
        let lat = snapShot.childSnapshot(forPath: "0")
        let long = snapShot.childSnapshot(forPath: "1")
        var driverLat:Double = 0.0
        var driverLong:Double = 0.0
        
        if let _driverLat = lat.value as? Double{
            driverLat = _driverLat
        }else if let _driverLat = lat.value as? String{
            driverLat = Double(_driverLat) ?? 0.0
        }
        
        if let _driverLong = long.value as? Double{
            driverLong = _driverLong
        }else if let _driverLong = long.value as? String{
            driverLong = Double(_driverLong) ?? 0.0
        }
        
        let position = CLLocationCoordinate2D(latitude: driverLat, longitude: driverLong)
        return position
    }
    
    func addDriverMarker(on position:CLLocationCoordinate2D, and driverId: String){
        let aDriver = Driver()
        aDriver.id = driverId
        aDriver.location.latitude = position.latitude
        aDriver.location.longitude = position.longitude
        aDriver.pLocation.latitude = position.latitude
        aDriver.pLocation.longitude = position.longitude
        if self.driverMarker != nil{
            self.driverMarker.map = nil
        }
        self.oldLocationCoordinate2D = position
        self.driverMarker = DriverMarker(driver: aDriver)
        self.driverMarker.position = position
        self.driverMarker.appearAnimation = .pop
        self.driverMarker.map = self.mapView
        self.driverMarker.zIndex = 10
        self.driverMarker.rotation = self.getInitailBearing()
    }
    
    func getInitailBearing() -> Double{
        let strat = CLLocationCoordinate2D(latitude: self.ride.driver.location.previousLatitude, longitude: self.ride.driver.location.previousLongitude)
        let end = CLLocationCoordinate2D(latitude: self.ride.driver.location.latitude, longitude: self.ride.driver.location.longitude)
        let brng = self.bearingFromCoordinate(from: strat, to: end)
        return brng
    }
    
    func setTitleForRideTrackingArrival(response: DataSnapshot, ride:Ride){
        let isReachedSnapShot = response.childSnapshot(forPath: RideTrackingKeys.isReached.rawValue)
        let isStartedSnapShot = response.childSnapshot(forPath: RideTrackingKeys.isReached.rawValue)
        guard let isStarted = isStartedSnapShot.value as? Int else{return}
        guard let isReached = isReachedSnapShot.value as? Int else{return}
        self.ride.isStarted = (isStarted == 1)
        self.ride.isDriverReached = (isReached == 1)
        self.setTitleForTracking(ride: ride)
    }
    
    func removeDriverMarker(key:String){
        guard let marker = driverMarker else{return}
        if key == marker.driver.id{
            self.driverMarker.map = nil
        }
    }
    
    func updateDriverMarker(key:String,newPosition:CLLocationCoordinate2D){
        guard let marker = self.driverMarker else{
            self.addDriverMarker(on: newPosition, and: key)
            return
        }
        
        if key == marker.driver.id{
            guard let oldLocation = self.oldLocationCoordinate2D else{
                return
            }
            let oldLocationOfMarker = CLLocation(latitude: oldLocation.latitude, longitude: oldLocation.latitude)
            let newLocationOfMarker = CLLocation(latitude: newPosition.latitude, longitude: newPosition.latitude)
            let distance = oldLocationOfMarker.distance(from: newLocationOfMarker)
            //            if distance >= 200.0{
            //                print_debug("distance Tracking: \(distance)")
            //                let newBearing = self.bearingFromCoordinate(from: oldLocation, to: newPosition)
            //                self.resetMarker(markerID: key, to: newPosition, bearing: newBearing)
            //            }else{
            self.updateDriverMarkerLocation(marker: marker, from: oldLocation, to: newPosition)
            //}
        }
    }
    
    func updateDriverMarkerLocation(marker:DriverMarker, from oldLocation: CLLocationCoordinate2D,to newLocation: CLLocationCoordinate2D) -> Void {
        self.moveMarker(marker, to: newLocation)
    }
    
    func resetMarker(markerID: String, to destination: CLLocationCoordinate2D, bearing:Double){
        let aDriver = Driver()
        aDriver.id = markerID
        aDriver.location.latitude = destination.latitude
        aDriver.location.longitude = destination.longitude
        aDriver.pLocation.latitude = destination.latitude
        aDriver.pLocation.longitude = destination.longitude
        removeDriverMarker(key: markerID)
        
        let driverMarker = DriverMarker(driver: aDriver)
        driverMarker.position = destination
        driverMarker.appearAnimation = .pop
        driverMarker.rotation = bearing
        if self.mapView != nil{
            driverMarker.map = self.mapView
        }
        self.driverMarker = driverMarker
    }
    
    func moveMarker(_ marker : GMSMarker, to destination:CLLocationCoordinate2D){
        guard let start = self.oldLocationCoordinate2D else{return}
        let startRotation = marker.rotation
        let animator = ValueAnimator.animate("some", from: 0, to: 1, duration: 1.5) { (prop, value) in
            let v = value.value
            let newPosition = self.getInterpolation(fraction: v, startPoint: start, endPoint: destination)
            marker.position = newPosition
            let newBearing = self.bearingFromCoordinate(from: start, to: newPosition)
            let rotation = self.getRotation(fraction: v, start: startRotation, end: newBearing)
            marker.rotation = rotation
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.isFlat = true
            self.oldLocationCoordinate2D = newPosition
        }
        //self.oldLocationCoordinate2D = newPosition
        animator.resume()
        self.makeRideCenter(position: marker.position)
        
        
    }
    
    
    private func angleFromCoordinate(from first : CLLocationCoordinate2D, to seccond:CLLocationCoordinate2D) -> Double {
        let deltaLongitude = seccond.longitude - first.longitude
        let deltaLatitude = seccond.latitude - first.latitude
        let angle = (.pi * 0.5) - atan(deltaLatitude/deltaLongitude)
        if deltaLongitude > 0 {return angle}
        else if deltaLongitude < 0 {return angle + .pi}
        else if deltaLatitude == 0 {return .pi}
        return 0.0
    }
    
    
    func bearingFromCoordinate(from first : CLLocationCoordinate2D, to seccond:CLLocationCoordinate2D) -> Double {
        let pi = Double.pi
        let lat1 = first.latitude*pi/180.0
        let long1 = first.longitude*pi/180.0
        let lat2 = seccond.latitude*pi/180.0
        let long2 = seccond.longitude*pi/180.0
        
        let diffLong = long2 - long1
        let y = sin(diffLong)*cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(diffLong)
        var bearing = atan2(y, x)
        bearing = bearing.toDegree(fromRadian: bearing)
        bearing = (bearing+360).truncatingRemainder(dividingBy: 360)
        return bearing
    }
    
    
    private func getRotation(fraction:Double, start: Double, end: Double) -> Double{
        let normailizedEnd = end - start
        let normailizedEndAbs = ((normailizedEnd+360)).truncatingRemainder(dividingBy: 360)
        let direction = (normailizedEndAbs > 180) ? -1 : 1 //-1 for anticlockwise and 1 for closewise
        let rotation = (direction > 0) ? normailizedEndAbs : (normailizedEndAbs-360)
        let result = fraction*rotation+start
        let finalResult = (result+360).truncatingRemainder(dividingBy: 360)
        return finalResult
    }
    
    private func getInterpolation(fraction:Double, startPoint:CLLocationCoordinate2D, endPoint:CLLocationCoordinate2D) -> CLLocationCoordinate2D{
        let latitude = (endPoint.latitude - startPoint.latitude) * fraction + startPoint.latitude
        var longDelta = endPoint.longitude - startPoint.longitude
        if abs(longDelta) > 180{
            longDelta -= longDelta*360
        }
        let longitude = (longDelta*fraction) + startPoint.longitude
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    func makeRideCenter(position:CLLocationCoordinate2D){
        if !self.mapView.projection.contains(position){
            self.mapView.animate(toLocation: position)
        }
    }
    
    
}


