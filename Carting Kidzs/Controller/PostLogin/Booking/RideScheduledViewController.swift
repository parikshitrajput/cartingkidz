//
//  RideScheduledViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 09/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
protocol RideScheduledViewControllerDelegate {
    func close(viewController:RideScheduledViewController)
}

class RideScheduledViewController: UIViewController {
    var delegate:RideScheduledViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onClickClose(_ sender: UIButton){
        NotificationCenter.default.post(name: .CHECK_BOOKING_VIEW_NOTIFICATION, object: nil, userInfo: nil)
        delegate?.close(viewController: self)
    }

}
