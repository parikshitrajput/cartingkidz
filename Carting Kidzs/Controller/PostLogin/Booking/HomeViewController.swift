//
//  HomeViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 04/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import GooglePlaces
import Firebase
import Alamofire
import SwiftyJSON

let kMapZoomLevel:Float = 14.0

class HomeViewController: UIViewController, AddressPickerViewDelegate,ARCarMovementDelegate,SelectChildViewControllerDelegate{
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var centerPinImage: UIImageView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressPickerView: AddressPickerView!
    
    @IBOutlet weak var driverIcon: UIImageView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var containner: UIView!
    @IBOutlet weak var rideTrackView: UIView!
    @IBOutlet weak var currentLocationButton: UIButton!

    

    var carCategories = Array<CarCategory>()
    var selectedCategoryIndex: Int = 0
    var cars = Array<Car>()
    var selectedAddressType: AddressType = .from


    var locationManager = CLLocationManager()
    var currentLocation : CLLocation!
    var selectedCategory : CarCategory!
    var pickUpCoordinates: CLLocationCoordinate2D?
    var dropOffCoordinates: CLLocationCoordinate2D?
    var pickUpAddress: String?
    var dropOffAddress: String?
    var ridingDate : Date?
    var mapViewWillMove = true
    var mapViewWillMoveByLocationButton = false
    var mapViewMovedByGesture = false
    var isLocationCalled = false
    var shouldOpenAddChild = false

    var dateTextField : UITextField!
    var datePicker : UIDatePicker!
    
    private var updateChildHandle: DatabaseHandle?
    private var addChildHandle: DatabaseHandle?
    private var removedChildHandle: DatabaseHandle?
    var ref: DatabaseReference?
    var refreshTime : Double!

    weak var refreshTimer: Timer?
//    var onGoingRideView: OnGoingRideView!
    var currentRide:Ride?
    var shouldAddShadow:Bool = true
    
    
    
    var time:Double = 0.0
    var distance: Double = 0.0
    var rideFare : Double?
   var estimateCost : String?
    var bookingView: ConfirmBookingView!
    var ride = Ride()
    var isPresentBookingView: Bool = false
    
    var bookingPickUpCoordinates: CLLocationCoordinate2D?
    var bookingDropOffCoordinates: CLLocationCoordinate2D?
    var bookingPickUpAddress: String?
    var bookingDropOffAddress: String?
    
    var pickUpBookMarker: AddressMarker!
    var dropBookMarker: AddressMarker!

    
//    var carGroupRef: DatabaseReference!
    
    fileprivate lazy var storageRef: StorageReference = Storage.storage().reference(forURL: api.firebaseDatabase.url())

    //    var driverArray = [Driver]()
    var driverMarkers = [DriverMarker]()
    var moveMent: ARCarMovement!
    var selectedChildren = [Child]()
    var user : User!
    override func viewDidLoad() {
        super.viewDidLoad()
     print("bsvinotuwcrpvy,")
        self.containner.isHidden = true
        self.user = User.loadSavedUser()
        NotificationCenter.default.addObserver(self, selector: #selector(rideDidUpdate(_:)), name: .RIDE_UPDATED_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkBookingViewStatus(_:)), name: .CHECK_BOOKING_VIEW_NOTIFICATION, object: nil)
        self.addressPickerView.delegate = self
        self.addressPickerView.setNeedsLayout()
        self.addressPickerView.layoutIfNeeded()

        let nib = UINib(nibName: "CategoryCell", bundle: nil)
        self.categoryCollectionView.register(nib, forCellWithReuseIdentifier: "CategoryCell")
        let catFlowLayout = UICollectionViewFlowLayout()
        catFlowLayout.minimumLineSpacing = 1
        catFlowLayout.minimumInteritemSpacing = 1
        catFlowLayout.scrollDirection = .horizontal
        categoryCollectionView.collectionViewLayout = catFlowLayout
        categoryCollectionView.dataSource = self
        categoryCollectionView.delegate = self
        moveMent = ARCarMovement()
        moveMent.delegate = self
        self.locationManagerSetup()
        dateTextField = UITextField()
        self.view.addSubview(dateTextField)
        self.setUpDatePicker()

//        if self.user.isParentAddedd(){
//
//            self.openAddParentVC(fromSignUp: true)
//        }

        self.selectedChildren.removeAll()
        moveMent = ARCarMovement()
        moveMent.delegate = self
        self.mapView.mapStyle(withFilename: "mapStyle", andType: "json")
        //          self.getCarCategoriesFromServer()
        NotificationCenter.default.addObserver(self, selector:  #selector(dropOffAddressDidSelected(_:)) , name: NSNotification.Name.DROP_OFF_ADDRESS_DID_SET_NOTIFICATION, object: nil)
        self.mapView.isTrafficEnabled = true
    }

    override func viewWillAppear(_ animated: Bool) {
        footerView.isHidden = (self.carCategories.count == 0)
        self.getParents()
        self.getOnGoingRides()

    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        self.removeRideView()
        self.refreshTimer?.invalidate()
        self.refreshTimer = nil
        self.removeFirebaseHandlers()
        if let removeHandle = self.removedChildHandle{
            self.ref?.removeObserver(withHandle: removeHandle)
        }
    }
    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: .clear, borderWidth: 0, cornerRadius: 4)
        CommonClass.makeViewCircular(driverIcon, borderColor: appColor.red, borderWidth: 1)
    }
    
    
    
    func removeRideView() {
        if self.containner.isHidden != true{
            self.containner.isHidden = true
        }
    }
    
        func setupRideDetails(ride:Ride){

            if ride.driver.ID != ""{
                self.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: #imageLiteral(resourceName: "driver_placeholder"))
            }else{
                self.driverIcon.image = UIImage(named: ride.carCategory.categoryName.lowercased())
            }

            CommonClass.makeViewCircular(self.driverIcon, borderColor: appColor.red, borderWidth: 1)

        }
    
    
    //ongoing ride start


    @objc func rideDidUpdate(_ notification:Notification) {
        self.removeRideView()
        self.getOnGoingRides()
    }


    func getOnGoingRides(){
//        let appDelegate = AppDelegate.getAppDelegate()
//        if !(appDelegate.window?.rootViewController?.topViewController is HomeViewController) {
//            return
//        }

        RideService.sharedInstance.getOnGoingRides { (success, resRides, count, message) in
            if var rides = resRides,rides.count > 0{
                rides.sort(by: { (ride1, ride2) -> Bool in
                    return ride1.ID < ride2.ID
                })
                guard let currentRide = rides.last else{return}
                self.addRideView(with: currentRide)
            }
        }
    }


    func addRideView(with ride: Ride){
        self.currentRide = ride
        if self.containner.isHidden != true{
            self.containner.isHidden = true
        }

        self.setupRideDetails(ride: ride)
        self.cancelButton.addTarget(self, action: #selector(onClickCandelOfCurrentRideview(_:)), for: .touchUpInside)
        self.containner.isHidden = false
        self.addGestureRecognizerOnOnGoingRideView()
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: .clear, borderWidth: 0, cornerRadius: 4)
        self.containner.addshadow(top: true, left: true, bottom: true, right: true, shadowRadius: 2, shadowOpacity: 0.5, shadowColor: self.shouldAddShadow ? .clear : .lightGray)
          self.animateCurrentRideView()
    }
    
    @objc func animateCurrentRideView(){
        UIView.animate(withDuration: 2, animations: {
            self.rideTrackView.alpha = 0.8
            self.rideTrackView.backgroundColor = UIColor.init(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0)
        }) { (done) in
            UIView.animate(withDuration: 0.5) {
                self.rideTrackView.alpha = 1.0
                self.rideTrackView.backgroundColor = UIColor.white
            }
        }
        self.perform(#selector(animateCurrentRideView), with: nil, afterDelay: 6)
    }


    @IBAction func onClickCandelOfCurrentRideview(_ sender: UIButton){
        self.removeRideView()
    }
    
    func addGestureRecognizerOnOnGoingRideView(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(currentRideViewTapped(_:)))
        self.containner.isUserInteractionEnabled = true
        self.containner.addGestureRecognizer(tapGestureRecognizer)
    }
    @objc func currentRideViewTapped(_ tapGestureRecognizer: UITapGestureRecognizer){
        guard let ride = self.currentRide else {
            return
        }
        self.openRideTracking(ride: ride)
    }

    func openRideTracking(ride:Ride){
        let rideTrackingVC = AppStoryboard.History.viewController(RideTrackingViewController.self)
        rideTrackingVC.ride = ride
        rideTrackingVC.fromHistory = false
        self.navigationController?.pushViewController(rideTrackingVC, animated: true)
    }

    //ongoing ride end

    
    @objc func refreshCategories(){
        if self.refreshTime == nil{
            self.refreshTime = Date().toMillis()
        }
        if (Date().toMillis()-59999) < self.refreshTime{
            return
        }
        
        guard let pickLocation = self.pickUpCoordinates else {return}
        let userLocation = CLLocation(latitude: pickLocation.latitude, longitude: pickLocation.longitude)
        self.getCarCategoriesFromServer(userLocation, shouldRefreshOnlyCategories: true)
    }
    
    func invalidateTimerAndSetItAgain(){
        self.refreshTime = Date().toMillis()
        self.refreshTimer?.invalidate()
        self.refreshTimer = nil
        self.refreshTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(refreshCategories), userInfo: nil, repeats: true)
    }
    
    
//    func resetBounds(){
//        let path = GMSMutablePath()
//        guard let pickCoordinates = self.pickUpCoordinates else {
//            return
//        }
//        guard let dropCoordinates = self.dropOffCoordinates else {
//            return
//        }
//        path.add(pickCoordinates)
//        path.add(dropCoordinates)
//
//        for driver in driverMarkers{
//            path.add(driver.position)
//        }
//
//        let bounds = GMSCoordinateBounds(path: path)
//        let camera = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 80, left: 40, bottom: self.footerView.frame.size.height+50, right: 40))
//        self.mapView.animate(with: camera)
//    }
    

    func getParents(){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue){self.dismiss(animated: true, completion: nil)}
            return
        }

        //AppSettings.shared.showLoader(withStatus: "Loading..")
        LoginService.sharedInstance.getParent { (success, resParents, message) in
           // AppSettings.shared.hideLoader()
            if success{
                if let someParents = resParents{
                    if someParents.count == 0
                    {
                        self.openAddParentVC(fromSignUp: true)
                    }
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    deinit {
        for carCat in carCategories{
            let path = "\(carCat.ID)_Search_\(self.user.ID)"
            Database.database().reference(withPath: path).removeAllObservers()
        }
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- AddressType selection handling
    func addressPickerView(addressPickerView: AddressPickerView, didTapOnAddressType addressType: AddressType) {
        self.selectedAddressType = addressType
        self.centerPinImage.image = UIImage(named: addressType.rawValue)
        self.centerPinImage.animateImage(duration:0.5)

        self.showGooglePlaceAutocomplete()

        if self.selectedAddressType == .from{
            guard let from = self.pickUpCoordinates else{return}
            self.refreshCustomerLocationWithCoordinate(from)
        }else{
            guard let to = self.dropOffCoordinates else{return}
            self.refreshCustomerLocationWithCoordinate(to)
        }
    }

    //MARK:- CoreLocation handling
    func locationManagerSetup() {
        locationManager.delegate = self;
        mapView.delegate = self
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.mapView.isMyLocationEnabled = true
    }





    func setUpDatePicker() -> Void {
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
        self.datePicker.minimumDate = Date.init(timeInterval: (2*60*60 + 10*60), since: Date())
        self.datePicker.date =  Date.init(timeInterval: (2*60*60 + 10*60), since: Date())
        self.datePicker.maximumDate =  Date.init(timeInterval: 30*24*60*60, since: Date())
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.barTintColor = appColor.green
        toolBar.sizeToFit()
        toolBar.tintColor = UIColor.white
        toolBar.backgroundColor = appColor.green
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(HomeViewController.doneClick))
        doneButton.tintColor = UIColor.white
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(HomeViewController.cancelClick))
        cancelButton.tintColor = UIColor.white

        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        dateTextField.inputView = self.datePicker
        dateTextField.inputAccessoryView = toolBar
    }

    @objc func doneClick() {
        let dateFormatter1 = DateFormatter()
        self.ridingDate = datePicker.date//dateFormatter1.string(from: datePicker.date)
        dateTextField.text = dateFormatter1.string(from: datePicker.date)
        dateTextField.resignFirstResponder()
        self.showChildrenSelection()
    }

    @objc func cancelClick() {
        dateTextField.resignFirstResponder()
    }

    //MARK:- RideNow and RideLate actions

    @IBAction func onClickRideLater(_ sender: UIButton){
        self.ridingDate = nil
        if self.pickUpCoordinates == nil {
            self.addressPickerView.onClickFromAddressButton(self.addressPickerView.fromAddressButton)
            return
        }
        if self.dropOffCoordinates == nil {
            self.addressPickerView.onClickToAddressButton(self.addressPickerView.toAddressButton)
            return
        }

        self.datePicker.minimumDate = Date.init(timeInterval: (2*60*60 + 10*60), since: Date())
        self.datePicker.date =  Date.init(timeInterval: (2*60*60 + 10*60), since: Date())
        self.datePicker.maximumDate =  Date.init(timeInterval: 30*24*60*60, since: Date())
        dateTextField.becomeFirstResponder()
    }

    @IBAction func onClickCurrentLocation(_ sender: UIButton){
        guard let location = self.mapView.myLocation else {return}
        self.mapViewWillMoveByLocationButton = true
        self.mapView.animate(toLocation: location.coordinate)
    }

    @IBAction func onClickRideNow(_ sender: UIButton){
        self.ridingDate = nil
        if self.pickUpCoordinates == nil {
            self.addressPickerView.onClickFromAddressButton(self.addressPickerView.fromAddressButton)
//            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter pick up location")
            return
        }
        if self.dropOffCoordinates == nil {
            self.addressPickerView.onClickToAddressButton(self.addressPickerView.toAddressButton)
//            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter drop location")
            return
        }

        self.showChildrenSelection()
    }

    func child(_ viewController: SelectChildViewController, didSelectChildren children: Array<Child>) {
        self.selectedChildren.append(contentsOf: children)
        viewController.dismiss(animated: false, completion: nil)
        self.updateMarkerWithNewHandler()
        if self.selectedChildren.count == 0{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please select children")
            return
        }
        self.bookingPickUpAddress = self.pickUpAddress
        self.bookingDropOffAddress = self.dropOffAddress
        self.bookingPickUpCoordinates = self.pickUpCoordinates
        self.bookingDropOffCoordinates = self.dropOffCoordinates
        self.centerPinImage.isHidden = true
        self.isPresentBookingView = true
        
        self.currentLocationButton.isHidden = true
        self.checkPointForBooking()
        self.addressPickerView.isUserInteractionEnabled = false
//        self.currentLocationButton.isHidden = true
        
        self.perform(#selector(HomeViewController.stopAnimation), with: nil, afterDelay: 0.2)
    }

    func showChildrenSelection(){
        self.selectedChildren.removeAll()
        let selectChildrenVC = AppStoryboard.Booking.viewController(SelectChildViewController.self)
        selectChildrenVC.delegate = self
        let nav = UINavigationController(rootViewController: selectChildrenVC)
        nav.navigationBar.isHidden = false
        nav.modalPresentationStyle = .overCurrentContext
        nav.navigationItem.title = "Select Child"
        self.present(nav, animated: true, completion: nil)
    }

    func checkPointForBooking(){
        guard let pickCoordinates = self.bookingPickUpCoordinates else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter pick up location")
            self.removeBookingView()
            return
        }
        guard let dropCoordinates = self.bookingDropOffCoordinates else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter drop location")
            self.removeBookingView()

            return
        }
        guard let pickAddress = self.bookingPickUpAddress else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter pick up location")
            self.removeBookingView()

            return
        }

        guard let dropAddress = self.bookingDropOffAddress else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter drop location")
            self.removeBookingView()

            return
        }

        if self.selectedChildren.count == 0{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please select at least 1 or maximum 4 children")
            self.removeBookingView()
            return
        }
        
        
         self.viewSetup()
        if self.ridingDate != nil{
        self.showBookingView(rideDate: self.ridingDate, pickAddress: pickAddress, dropAddress: dropAddress, pickCoordinates: pickCoordinates, dropCoordinate: dropCoordinates)
        }else{
            self.showBookingView(rideDate: self.ridingDate, pickAddress: pickAddress, dropAddress: dropAddress, pickCoordinates: pickCoordinates, dropCoordinate: dropCoordinates)

        }
        //        if self.ridingDate != nil{
//            self.navigateToBookingScreen(rideDate: self.ridingDate, pickAddress: pickAddress, dropAddress: dropAddress, pickCoordinates: pickCoordinates, dropCoordinate: dropCoordinates)
//        }else{
//            self.navigateToBookingScreen(rideDate: self.ridingDate, pickAddress: pickAddress, dropAddress: dropAddress, pickCoordinates: pickCoordinates, dropCoordinate: dropCoordinates)
//        }
    }

    func navigateToBookingScreen(rideDate: Date?, pickAddress: String, dropAddress: String, pickCoordinates: CLLocationCoordinate2D, dropCoordinate: CLLocationCoordinate2D){
        let bookingVC = AppStoryboard.Booking.viewController(BookingViewController.self)
        bookingVC.modalPresentationStyle = .overFullScreen
        
        bookingVC.pickUpAddress = pickAddress
        bookingVC.dropOffAddress = dropAddress
        bookingVC.pickUpCoordinates = pickCoordinates
        bookingVC.dropOffCoordinates = dropCoordinate
        bookingVC.rideDate = self.ridingDate
        bookingVC.childrens = self.selectedChildren
        bookingVC.selectedCategory = self.selectedCategory
        
        bookingVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        let nav = AppSettings.shared.getNavigation(vc:bookingVC)
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = false
         self.present(nav, animated: false, completion: nil)
//        self.navigationController?.pushViewController(bookingVC, animated: false)
    }

}




extension HomeViewController:GMSAutocompleteViewControllerDelegate{

    func showGooglePlaceAutocomplete(){
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        self.present(acController, animated: true, completion: nil)
    }

    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        viewController.dismiss(animated: true, completion: nil)
        var fullAddress = ""
        fullAddress = place.name
        if let formattedAddress = place.formattedAddress{
            if fullAddress.isEmpty{
                fullAddress = "\(formattedAddress)"
            }else{
                fullAddress = fullAddress + ", \(formattedAddress)"
            }
        }

        if selectedAddressType == .from{
            self.pickUpAddress = fullAddress
            self.pickUpCoordinates = place.coordinate
            if !self.isPresentBookingView {

            self.addressPickerView.fromAddressLabel.text = fullAddress
            }
            self.getAllCarsByCategoryFromServer(catetegoryID: selectedCategory.ID, latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        }else{
            self.dropOffAddress = fullAddress
            self.dropOffCoordinates = place.coordinate
            if !self.isPresentBookingView {
            self.addressPickerView.toAddressLabel.text = fullAddress
            }
        }
        //        self.getAllCarsByCategoryFromServer(catetegoryID: selectedCategory.ID, latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        refreshCustomerLocationWithCoordinate(place.coordinate)
    }

    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        NKToastHelper.sharedInstance.showErrorAlert(nil, message: error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

    func formattedAddress(from place:GMSPlace){

    }

}


//MARK:- CollectionView handling
extension HomeViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return carCategories.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        let category = carCategories[indexPath.item]

        let imageName = category.ID+(indexPath.item == selectedCategoryIndex ? "_sel" : "_unsel")
        cell.categoryIcon.image = UIImage(named: imageName)
        cell.categoryNameLabel.text = category.categoryName
        cell.seatsLabel.text = "(\(category.capecity) Seats)"
        cell.shouldShowSeparator = true

        if category.time == ""{
            cell.timeLabel.text = "..."
        }else if category.time.contains("no".lowercased()){
            cell.timeLabel.text = "No "+category.categoryName
        }else{
            cell.timeLabel.text = category.time
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/2 - 1
        let height = collectionView.frame.size.height - 2
        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if selectedCategoryIndex == indexPath.item{
            self.showCategoryFare(category: self.selectedCategory)
            return
        }
        self.selectedCategory = self.carCategories[indexPath.item]
        self.selectedCategoryIndex = indexPath.item
        guard let pickCoordinates = self.pickUpCoordinates else {
            self.getAllCarsByCategoryFromServer(catetegoryID: self.selectedCategory.ID, latitude: self.currentLocation.coordinate.latitude, longitude: self.currentLocation.coordinate.longitude)
            return
        }
        self.getAllCarsByCategoryFromServer(catetegoryID: self.selectedCategory.ID, latitude: pickCoordinates.latitude, longitude: pickCoordinates.longitude)
//        self.upDateMarker()
        self.updateMarkerWithNewHandler()

        collectionView.reloadData()
    }


    func showCategoryFare(category:CarCategory){
        let categoryFareVC = AppStoryboard.Booking.viewController(CategoryFareViewController.self)
        categoryFareVC.category = category
        categoryFareVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        let nav = UINavigationController(rootViewController: categoryFareVC)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        self.present(nav, animated: true, completion: nil)
    }


}



//MARK:- LocationManagerDelegate and GMSMapViewDelegate
extension HomeViewController: CLLocationManagerDelegate, GMSMapViewDelegate{

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways{
            self.locationManager.startUpdatingLocation()
        }else if status == .denied || status == .restricted{
            self.showGotoLocationSettingAlert()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        if !isLocationCalled{
            isLocationCalled = true
            if let location = locations.last{
                currentLocation = location
                self.getCarCategoriesFromServer(location)
                self.refreshCustomerLocationWithCoordinate(location.coordinate)
                self.nameOfPlaceWithCoordinates(coordinate: location.coordinate, complition: { (address) in
                    if self.selectedAddressType == .from{
                        if !self.isPresentBookingView {

                        self.addressPickerView.fromAddressLabel.text = "Getting address.."
                        }
                        self.pickUpCoordinates = location.coordinate
                        self.nameOfPlaceWithCoordinates(coordinate: location.coordinate, complition: { (address) in
                            self.pickUpAddress = address
                            if !self.isPresentBookingView {

                            self.addressPickerView.fromAddressLabel.text = address
                            }
                        })
                    }else{
                        if !self.isPresentBookingView {
                        self.addressPickerView.toAddressLabel.text = "Getting address.."
                        }
                        self.dropOffCoordinates = location.coordinate
                        self.nameOfPlaceWithCoordinates(coordinate: location.coordinate, complition: { (address) in
                            self.dropOffAddress = address
                            if !self.isPresentBookingView {

                            self.addressPickerView.toAddressLabel.text = address
                            }
                        })
                    }
                })
                // self.getAllCarsFromServer()
            }
        }
    }

    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        let fromLocation = mapViewWillMoveByLocationButton
        mapViewWillMove = gesture || fromLocation
        animateHeaderAndRideView(true)
        self.mapViewWillMoveByLocationButton = false
    }


    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if !self.mapViewWillMove{
            self.mapViewWillMove = true
            return
        }
        if self.selectedAddressType == .to{
            dropOffCoordinates = position.target
            if !self.isPresentBookingView {

            self.addressPickerView.toAddressLabel.text = "Getting address.."
            }
            self.nameOfPlaceWithCoordinates(coordinate: position.target, complition: { (address) in
                self.dropOffAddress = address
                if !self.isPresentBookingView {

                self.addressPickerView.toAddressLabel.text = address
                }
            })

        }else{
            pickUpCoordinates = position.target
            //reset time to empty in categories array
            if carCategories.count > 0{
                for carCat in carCategories{
                    carCat.time = ""
                }
                categoryCollectionView.reloadData()
            }
            if !self.isPresentBookingView {

            self.addressPickerView.fromAddressLabel.text = "Getting address.."
            }
            self.nameOfPlaceWithCoordinates(coordinate: position.target, complition: { (address) in
                self.pickUpAddress = address
                if !self.isPresentBookingView {
                self.addressPickerView.fromAddressLabel.text = address
                }
            })

            //self.driverArray.removeAll()
            self.getCarCategoriesFromServer(CLLocation(latitude: position.target.latitude, longitude: position.target.longitude))
        }
        animateHeaderAndRideView(false)
    }

    func animateHeaderAndRideView(_ isHide:Bool) {
        if !mapViewWillMove{
            return
        }
        if isHide{

        }else{

        }
    }

    //    func refreshMarkers() {
    //        //        self.googleMapView.clear()
    //        for driver in self.driverArray{
    //            self.addMarkerForDriver(driver: driver)
    //        }
    //    }

    func refreshCustomerLocationWithCoordinate(_ coordinate:CLLocationCoordinate2D){
        let camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: kMapZoomLevel)
        self.mapView.mapType = .normal
        self.mapView.camera = camera
    }

    func nameOfPlaceWithCoordinates(coordinate : CLLocationCoordinate2D,complition:@escaping (_ address:String)->Void) {
        let geocoder = GMSGeocoder()
        var currentAddress = ""
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                let filteredLines = lines.filter({ (line) -> Bool in
                    return !line.isEmpty
                })
                currentAddress = filteredLines.joined(separator: ",")
                complition(currentAddress)
            }
        }
    }


    func getCarCategoriesFromServer(_ location: CLLocation, shouldRefreshOnlyCategories:Bool = false) -> Void {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        self.invalidateTimerAndSetItAgain()

        RideService.sharedInstance.getAllCategoryFromServer(location.coordinate.latitude, longitude: location.coordinate.longitude) { (success, resCategories, message) in
            if success{
                if let someCategories = resCategories{
                    self.carCategories.removeAll()
                    self.carCategories.append(contentsOf: someCategories)
                    self.carCategories.sort(by: { (cat1, cat2) -> Bool in
                        return cat1.categoryName.lowercased() < cat2.categoryName.lowercased()
                    })
                    if self.selectedCategory == nil{
                        self.selectedCategory = self.carCategories.first ?? CarCategory()
                    }
                    self.prepareRequestToTravelTime()
                    self.categoryCollectionView.reloadData()
                    if !shouldRefreshOnlyCategories{
                        self.getAllCarsByCategoryFromServer(catetegoryID: self.selectedCategory.ID, latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                    }
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }

            if self.carCategories.count == 0{
                self.footerView.isHidden = true
            }else{
                self.footerView.isHidden = false
            }
        }
    }

    func getAllCarsByCategoryFromServer(catetegoryID: String,latitude:Double,longitude:Double) -> Void {
        //        AppSettings.sharedInstance.clearAllPendingRequests()
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }

        RideService.sharedInstance.newSearchCarsByCategory(catetegoryID, latitude: latitude, longitude: longitude, userID: self.user.ID) { (success, resCars, message) in
            if success{
//                self.upDateMarker()
                self.updateMarkerWithNewHandler()
            }
        }
    }

    func prepareRequestToTravelTime() -> Void {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        var destinations = [CLLocationCoordinate2D]()
        for carCategory in self.carCategories{
            destinations.append(CLLocationCoordinate2D(latitude: carCategory.nearestCarLatitude, longitude: carCategory.nearestCarLongitude))
        }
        if destinations.count==0{
            return
        }
        guard let pickCoordnates = self.pickUpCoordinates else {
            return
        }
        self.getArrivalTimeOfAllCategory(from: pickCoordnates, to: destinations)
    }

    func showGotoLocationSettingAlert(){
        let alert = UIAlertController(title: "Oops", message: "Location access seems disabled\r\nGo to settings to enabled", preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }

        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            let url = UIApplicationOpenSettingsURLString
            if UIApplication.shared.canOpenURL(URL(string: url)!){
                UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            }
        }

        alert.addAction(okayAction)
        alert.addAction(settingsAction)

        self.present(alert, animated: true, completion: nil)
    }

}

//pari
extension HomeViewController
{

    func getArrivalTimeOfAllCategory(from userLocationCoordinate: CLLocationCoordinate2D, to destinations: [CLLocationCoordinate2D]) -> Void {
        var destinationsStr = ""
        let sourceStr = "\(userLocationCoordinate.latitude),\(userLocationCoordinate.longitude)"
        for dest in destinations{
            let destStr = "\(dest.latitude)%2C\(dest.longitude)"
            if destinationsStr == ""{
                destinationsStr = destStr
            }else{
                destinationsStr = "\(destinationsStr)%7C\(destStr)"
            }
        }

        getTravelTime(from: sourceStr, to: destinationsStr) { (timeArray) in
            for t in 0..<timeArray.count{
                self.carCategories[t].time = timeArray[t]
                if self.carCategories[t].ID == self.selectedCategory.ID{
                    self.selectedCategory.time = timeArray[t]
                }
            }
            self.categoryCollectionView.reloadData()
        }
    }

    func getTravelTime(from source:String,to destination: String,completion:@escaping (_ times: [String])->Void)->Void{
        let url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(source)&destinations=\(destination)&key=\(GOOGLE_API_KEY)"

        Alamofire.request(url).responseJSON { response in
            do{
                let json =  try JSON(data: response.data!)
                print_debug("traver time json : \n \(json)")
                let rows = json["rows"].arrayValue
                var timeArray = [String]()
                if rows.count > 0{
                    if let arrValues = rows.first?["elements"].array as [JSON]?{
                        for dict in arrValues{
                            if let notFound = dict["status"].string as String?,notFound != "OK"{
                                timeArray.append("no car")
                            }else if let durationDict = dict["duration"].dictionaryObject as [String:AnyObject]?{
                                if let durationf = durationDict["text"] as? String{
                                    timeArray.append(durationf)
                                }
                            }
                        }
                    }
                }
                completion(timeArray)
            }catch{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Error occurred while calculating time")
            }
        }
    }
}



extension HomeViewController {
    
    //here we goes for firebase handling
    
    func removeAllDriverMarker(){
        for marker in self.driverMarkers{
            self.removeDriverMarker(key: marker.driver.id)
        }
    }
    
    
    func removeFirebaseHandlers(){
        if let updateHandle = self.updateChildHandle{
            self.ref?.removeObserver(withHandle: updateHandle)
        }
        if let addHandle = self.addChildHandle{
            self.ref?.removeObserver(withHandle: addHandle)
        }
        if let removeHandle = self.removedChildHandle{
            self.ref?.removeObserver(withHandle: removeHandle)
        }
    }
    
    func updateMarkerWithNewHandler(){
        self.removeAllDriverMarker()
        let path = "\(self.selectedCategory.ID)_Search_\(self.user.ID)"
        self.ref = Database.database().reference(withPath: path)
        guard let firRef = ref else{
            return
        }
        self.removeFirebaseHandlers()
        firRef.keepSynced(false)
        
        self.updateChildHandle = firRef.observe(.childChanged, with: { (response) in
            if !response.key.lowercased().contains("driver_"){return}
            
            if self.isDriverMarkersContains(key: response.key){
                let driverLocation = response.childSnapshot(forPath: "l")
                let position = self.getPosition(from: driverLocation)
                self.updateDriverMarker(key: response.key, newPosition: position)
            }else{
                let driverLocation = response.childSnapshot(forPath: "l")
                let position = self.getPosition(from: driverLocation)
                
                let aDriver = Driver()
                aDriver.id = response.key
                aDriver.location.latitude = position.latitude
                aDriver.location.longitude = position.longitude
                aDriver.pLocation.latitude = position.latitude
                aDriver.pLocation.longitude = position.longitude
                
                let driverMarker = DriverMarker(driver: aDriver)
                driverMarker.position = position
                driverMarker.appearAnimation = .pop
                if isDebugEnabled{
                    driverMarker.title = aDriver.id
                    driverMarker.isTappable = true
                }
                if self.mapView != nil{
                    driverMarker.map = self.mapView
                }
                self.driverMarkers.append(driverMarker)
                self.resetBounds()
            }
        })
        
        
        self.removedChildHandle = firRef.observe(.childRemoved, with: { (response) in
            // guard let blockWeakSelf = weakSelf else{return}
            if !response.key.lowercased().contains("driver_"){
                self.removeAllDriverMarker()
            }else{
                if self.isDriverMarkersContains(key: response.key){
                guard let pickCoordinate =  self.pickUpCoordinates else {
                    return
                }
                    self.getCarCategoriesFromServer(CLLocation(latitude: pickCoordinate.latitude, longitude: pickCoordinate.longitude), shouldRefreshOnlyCategories: true)
                    self.removeDriverMarker(key: response.key)
                    self.resetBounds()
                }
            }
        })
        
        self.addChildHandle = firRef.observe(.childAdded, with: { (response) in
            //guard let blockWeakSelf = weakSelf else{return}
            if !response.key.lowercased().contains("driver_"){return}
            if !self.isDriverMarkersContains(key: response.key){
                guard let pickCoordinate = self.pickUpCoordinates else{
                            return
                                }
            self.getCarCategoriesFromServer(CLLocation(latitude: pickCoordinate.latitude, longitude: pickCoordinate.longitude), shouldRefreshOnlyCategories: true)
                
                let driverLocation = response.childSnapshot(forPath: "l")
                let position = self.getPosition(from: driverLocation)
                
                let aDriver = Driver()

                aDriver.id = response.key
                aDriver.location.latitude = position.latitude
                aDriver.location.longitude = position.longitude
                aDriver.pLocation.latitude = position.latitude
                aDriver.pLocation.longitude = position.longitude
                
                let driverMarker = DriverMarker(driver: aDriver)
                driverMarker.position = position
                driverMarker.appearAnimation = .pop
                if isDebugEnabled{
                    driverMarker.title = aDriver.id
                    driverMarker.isTappable = true
                }
                if self.mapView != nil{
                    driverMarker.map = self.mapView
                }
                self.driverMarkers.append(driverMarker)
                self.resetBounds()
            }else{
                let driverLocation = response.childSnapshot(forPath: "l")
                let position = self.getPosition(from: driverLocation)
                self.updateDriverMarker(key: response.key, newPosition: position)
            }
        })
        
    }
    
    
    
    func getPosition(from snapShot: DataSnapshot) -> CLLocationCoordinate2D{
        let lat = snapShot.childSnapshot(forPath: "0")
        let long = snapShot.childSnapshot(forPath: "1")
        
        var driverLat:Double = 0.0
        var driverLong:Double = 0.0
        
        if let _driverLat = lat.value as? Double{
            driverLat = _driverLat
        }else if let _driverLat = lat.value as? String{
            driverLat = Double(_driverLat) ?? 0.0
        }
        
        if let _driverLong = long.value as? Double{
            driverLong = _driverLong
        }else if let _driverLong = long.value as? String{
            driverLong = Double(_driverLong) ?? 0.0
        }
        
        let position = CLLocationCoordinate2D(latitude: driverLat, longitude: driverLong)
        return position
    }
    
    
    func isDriverMarkersContains(key:String) -> Bool{
        let filteredArray = self.driverMarkers.filter { (driverMarker) -> Bool in
            return driverMarker.driver.id == key
        }
        return filteredArray.count != 0
    }
    
    func indexForDriverMarker(driverMarker:DriverMarker) -> Int?{
        var result : Int?
        for (index,dM) in self.driverMarkers.enumerated(){
            if dM.driver.id == driverMarker.driver.id{
                result = index
                break;
            }
        }
        return result
    }
    
    func removeDriverMarker(key:String){
        for (index,dM) in self.driverMarkers.enumerated(){
            if key == dM.driver.id{
                dM.map = nil
                self.driverMarkers.remove(at: index)
                break;
            }
        }
    }
    
    func updateDriverMarker(key:String,newPosition:CLLocationCoordinate2D){
        for dM in self.driverMarkers{
            if key == dM.driver.id{
                let oldLocation = dM.position
                let oldLocationOfMarker = CLLocation(latitude: oldLocation.latitude, longitude: oldLocation.latitude)
                let newLocationOfMarker = CLLocation(latitude: newPosition.latitude, longitude: newPosition.latitude)
                let distance = oldLocationOfMarker.distance(from: newLocationOfMarker)
                if distance >= 200.0{
                    print_debug("distance in home : \(distance)")
                    let newBearing = self.bearingFromCoordinate(from: oldLocation, to: newPosition)
                    self.resetMarker(markerID: key, to: newPosition, bearing: newBearing)
                }else{
                    self.updateDriverMarkerLocation(marker: dM, from: oldLocation, to: newPosition)
                }
                break;
            }
        }
    }
    
    
    
    
    
    func updateDriverMarkerLocation(marker:DriverMarker, from oldLocation: CLLocationCoordinate2D,to newLocation: CLLocationCoordinate2D) -> Void {
        self.moveMarker(marker, fromSource: oldLocation, to: newLocation) { (updatedLocation) in}
    }
    
 
    
    func resetMarker(markerID: String, to destination: CLLocationCoordinate2D, bearing:Double){
        let aDriver = Driver()
        aDriver.id = markerID
        aDriver.location.latitude = destination.latitude
        aDriver.location.longitude = destination.longitude
        aDriver.pLocation.latitude = destination.latitude
        aDriver.pLocation.longitude = destination.longitude
        removeDriverMarker(key: markerID)
        
        let driverMarker = DriverMarker(driver: aDriver)
        driverMarker.position = destination
        driverMarker.appearAnimation = .pop
        driverMarker.rotation = bearing
        if self.mapView != nil{
            driverMarker.map = self.mapView
        }
        self.driverMarkers.append(driverMarker)
        self.resetBounds()
    }
    
    
    func resetBounds() {

    }

    
}





extension HomeViewController{
    @IBAction func onClickMenu(_ sender: UIBarButtonItem){
//        self.removeBookingView()
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
}


class PlaceMark: GMSMarker {
    var place:Car
    init(place: Car) {
        self.place = place
        super.init()
        self.position = CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude)
        groundAnchor = CGPoint(x: 0.5, y: 1)
    }
}

extension HomeViewController{
    func moveMarker(_ marker : GMSMarker,fromSource source:CLLocationCoordinate2D, to destination:CLLocationCoordinate2D,completionBlock:@escaping (_ movedMarkerPosition:CLLocationCoordinate2D) -> Void){
        let start = source
        let startRotation = marker.rotation
        let animator = ValueAnimator.animate("", from: 0, to: 1, duration: 1.0) { (prop, value) in
            let v = value.value
            let newPosition = self.getInterpolation(fraction: v, startPoint: start, endPoint: destination)
            marker.position = newPosition
            let newBearing = self.bearingFromCoordinate(from: start, to: newPosition)
            let rotation = self.getRotation(fraction: v, start: startRotation, end: newBearing)
            marker.rotation = rotation
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.isFlat = true
            completionBlock(start)
        }
        animator.resume()

    }


    private func angleFromCoordinate(from first : CLLocationCoordinate2D, to seccond:CLLocationCoordinate2D) -> Double {
        let deltaLongitude = seccond.longitude - first.longitude
        let deltaLatitude = seccond.latitude - first.latitude
        let angle = (.pi * 0.5) - atan(deltaLatitude/deltaLongitude)
        if deltaLongitude > 0 {return angle}
        else if deltaLongitude < 0 {return angle + .pi}
        else if deltaLatitude == 0 {return .pi}
        return 0.0
    }


    func bearingFromCoordinate(from first : CLLocationCoordinate2D, to seccond:CLLocationCoordinate2D) -> Double {
        let pi = Double.pi
        let lat1 = first.latitude*pi/180.0
        let long1 = first.longitude*pi/180.0
        let lat2 = seccond.latitude*pi/180.0
        let long2 = seccond.longitude*pi/180.0

        let diffLong = long2 - long1
        let y = sin(diffLong)*cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(diffLong)
        var bearing = atan2(y, x)
        bearing = bearing.toDegree(fromRadian: bearing)
        bearing = (bearing+360).truncatingRemainder(dividingBy: 360)
        return bearing
    }


    private func getRotation(fraction:Double, start: Double, end: Double) -> Double{
        let normailizedEnd = end - start
        let normailizedEndAbs = ((normailizedEnd+360)).truncatingRemainder(dividingBy: 360)
        let direction = (normailizedEndAbs > 180) ? -1 : 1 //-1 for anticlockwise and 1 for closewise
        let rotation = (direction > 0) ? normailizedEndAbs : (normailizedEndAbs-360)
        let result = fraction*rotation+start
        let finalResult = (result+360).truncatingRemainder(dividingBy: 360)
        return finalResult
    }

    private func getInterpolation(fraction:Double, startPoint:CLLocationCoordinate2D, endPoint:CLLocationCoordinate2D) -> CLLocationCoordinate2D{
        let latitude = (endPoint.latitude - startPoint.latitude) * fraction + startPoint.latitude
        var longDelta = endPoint.longitude - startPoint.longitude
        if abs(longDelta) > 180{
            longDelta -= longDelta*360
        }
        let longitude = (longDelta*fraction) + startPoint.longitude
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }

}
extension HomeViewController{
//    func addParents(viewController: AddParentsViewController, didUpdateUser user: User) {
//
//    }

    func openAddChildVC(fromSignUp isFromSignUp:Bool){
        let addChildVC = AppStoryboard.Booking.viewController(AddChildViewController.self)
        addChildVC.isFromSignUp = true
        let nav = AppSettings.shared.getNavigation(vc: addChildVC)
        nav.modalPresentationStyle = .overCurrentContext
        nav.navigationItem.title = "ADD CHILD"
        self.present(nav, animated: true, completion: nil)
    }

    func openAddParentVC(fromSignUp isFromSignUp:Bool){
        let addParentVC = AppStoryboard.Booking.viewController(AddParentsViewController.self)
        addParentVC.isFromSignUp = true
        //addParentVC.delegate = self
        let nav = AppSettings.shared.getNavigation(vc: addParentVC)
        nav.modalPresentationStyle = .overCurrentContext
        nav.navigationItem.title = "ADD PARENTS"
        self.present(nav, animated: true, completion: nil)
    }


    //MARK:- Drop address Hanlding
    @objc func dropOffAddressDidSelected( _ notification : Notification) {
        if notification.name == .DROP_OFF_ADDRESS_DID_SET_NOTIFICATION {
            if let userInfo = notification.userInfo as? [String:AnyObject]{
                if let dropAddress = userInfo["dropOffAddress"] as? String{
                    self.dropOffAddress = dropAddress
                    self.addressPickerView.toAddressLabel.text = dropAddress
                }
                if let dropCoord = userInfo["dropOffCoordinate"] as? CLLocationCoordinate2D{
                    self.dropOffCoordinates = dropCoord
                }
            }
        }
    }
}
extension GMSMapView {
    func mapStyle(withFilename name: String, andType type: String) {
        do {
            if let styleURL = Bundle.main.url(forResource: name, withExtension: type) {
                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
}

class DriverModel {

    var id: String?
    var latitude: Double?
    var longitude: Double?

    init(id: String?, latitude: Double?, longitude: Double?){
        self.id = id
        self.latitude = latitude
        self.longitude = longitude
    }
}

extension Double {
    // Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}



////////////////Booking View Controller Code
extension HomeViewController {
    
    
  @objc  func fromRipple() {
         self.addressPickerView.stopAnimatingToRipples()
          self.addressPickerView.stopAnimatingFromRipples()
            self.addressPickerView.setUpRippleOnFromDot()
    }
  @objc  func toRipple() {
    self.addressPickerView.stopAnimatingToRipples()
    self.addressPickerView.stopAnimatingFromRipples()
        self.addressPickerView.setUpRippleOnToDot()
    }
    
    @objc func checkBookingViewStatus(_ notification:Notification) {
        self.removeBookingView()
    }
    
    func removeBookingView() {
        if self.bookingView != nil{
            self.bookingView.removeFromSuperview()
            self.bookingView = nil
            self.removePickUpBookMarker()
            self.removeDropBookMarker()
            self.currentLocationButton.isHidden = false
            self.isPresentBookingView = false
            self.title = "BOOK YOUR RIDE"
            self.addressPickerView.isUserInteractionEnabled = true
            self.centerPinImage.isHidden = false
            self.resetAddressPickViewRipple()
            self.updateMarkerWithNewHandler()
        }
//        self.locationManagerSetup()


    }
    
    
    func resetAddressPickViewRipple() {
        if self.selectedAddressType == .from{
            self.perform(#selector(HomeViewController.fromRipple), with: nil, afterDelay: 0.8)
            guard let from = self.pickUpCoordinates else{return}
            self.refreshCustomerLocationWithCoordinate(from)
        }else{
            self.perform(#selector(HomeViewController.toRipple), with: nil, afterDelay: 0.8)
            guard let to = self.dropOffCoordinates else{return}
            self.refreshCustomerLocationWithCoordinate(to)
        }


    }
        
    
    
    func showBookingView(rideDate: Date?, pickAddress: String, dropAddress: String, pickCoordinates: CLLocationCoordinate2D, dropCoordinate: CLLocationCoordinate2D) {
        self.bookingView = ConfirmBookingView.instanceFromNib()
        let eframe = CGRect(x: 0, y: self.footerView.frame.origin.y, width: self.view.frame.size.width, height: self.footerView.frame.height)
        self.bookingView.frame = eframe
        self.bookingView.showFareBreakUpButton.addTarget(self, action: #selector(onShowFareBreakUps(_:)), for: .touchUpInside)
        self.bookingView.confirmButton.addTarget(self, action: #selector(onClicConfirmBookingButton(_:)), for: .touchUpInside)
//        self.bookingView.confirmButton.isUserInteractionEnabled = false
        self.bookingView.confirmButton.isEnabled = false
        self.view.addSubview(self.bookingView)
        self.view.bringSubview(toFront: bookingView)
        self.resetEnterDropOffButtonTarget()
        self.refreshMarkers()
        self.getEstimatedPrice()


    }
    
    func viewSetup(){
        var titleH = self.selectedCategory.categoryName
        if self.selectedCategory.time != ""{
            titleH = "\(titleH), \(self.selectedCategory.time)"
        }
        self.navigationItem.title = titleH
    }
    
    @IBAction func onClicConfirmBookingButton(_ sender: UIButton){
        
        let validation = self.validateParams()
        if !validation.isValidate{
            return
        }
        if (self.rideFare as Double?) != nil{
            self.confirmRideRequest {(resRide) in
                if let ride = resRide{
                    if ride.ID != ""{
                        //self.showChekingNearByDrivers()
                    }
                }
            }

        }
        //hit service for booking
    }
    
    
        func refreshMarkers() -> Void {
            self.addPickUpMarker()
            self.addDropOffMarker()
            self.resetBoundOfMap()
    //        self.drawPath(startLocation: self.pickUpCoordinates, endLocation: self.dropOffCoordinates)
        }
        func addPickUpMarker() -> Void {
            
            if self.pickUpBookMarker != nil{
                self.pickUpBookMarker.map = nil
            }
            let pickUpMarkerImg: UIImage = UIImage(named: "greenPin")!

            if let position = self.bookingPickUpCoordinates{
                self.pickUpBookMarker = AddressMarker(image: pickUpMarkerImg)
                self.pickUpBookMarker.position = position
                self.pickUpBookMarker.isTappable = true
                self.pickUpBookMarker.title = self.bookingPickUpAddress
                self.pickUpBookMarker.map = self.mapView
            }

        }
        func addDropOffMarker() -> Void {
            
            if self.dropBookMarker != nil{
                self.dropBookMarker.map = nil
            }
            let dropMarkerImg: UIImage = UIImage(named: "redPin")!

            if let position = self.bookingDropOffCoordinates{
                self.dropBookMarker = AddressMarker(image: dropMarkerImg)
                self.dropBookMarker.position = position
                self.dropBookMarker.isTappable = true
                self.dropBookMarker.title = self.bookingDropOffAddress
                self.dropBookMarker.map = self.mapView;
            }
            
        }
    
        func resetBoundOfMap() -> Void {
            let path = GMSMutablePath()
            path.add(self.pickUpCoordinates!)
            path.add(self.dropOffCoordinates!)
            let bounds = GMSCoordinateBounds(path: path)
            let camera = mapView.camera(for: bounds, insets:UIEdgeInsets(top: 55, left: 25, bottom: 50, right: 25))
            mapView.camera = camera!;
        }

//
//    func addMarkerforCoordinate(marker: AddressMarker,coordinate : CLLocationCoordinate2D?,image: UIImage,address: String) {
//            if let position = coordinate{
//
//                marker.position = position
//                marker.icon = image
//                marker.isTappable = true
//                marker.title = address
//                marker.map = self.mapView;
//            }
//        }
    
    
    func removePickUpBookMarker(){
        if pickUpBookMarker != nil{
            pickUpBookMarker.map = nil
            pickUpBookMarker = nil
        }
    }
    
    func removeDropBookMarker(){
        if dropBookMarker != nil{
            dropBookMarker.map = nil
            dropBookMarker = nil
        }
    }
    
    
    func getEstimatedPrice() -> Void {
        guard let pickCoordinates = self.bookingPickUpCoordinates else {
            return
        }
        guard let dropCoordinates = self.bookingDropOffCoordinates else {
            return
        }
        self.bookingView.totalFareLabel.text = Rs+"..."
        self.bookingView.enterDropOffLabel.text = "Estimating fare.."
        
        self.getTravelTimeAndDistance(from: pickCoordinates, to: dropCoordinates) { (time, distance) in
            self.time = time
            self.distance = distance
            
            if distance == 0.0 && time == 0.0{
                self.bookingView.totalFareLabel.text = "Fare couldn't calculated!"
            }else{
                self.calculateFare(distance: distance, time: time, category: self.selectedCategory, completionBlock: {(responseCost) in
                    if let estcost = responseCost{
                        let totalCost = estcost
                        let str = String(format: "%.2f", totalCost)
//                        if str != ""{
                        if self.bookingView != nil {
                        self.bookingView.totalFareLabel.text = "$"+str
                        }
//                    }
                        self.rideFare = estcost
                        self.estimateCost = Rs+str
                        self.bookingView.enterDropOffLabel.text = "Total fare"
                        self.bookingView.confirmButton.isEnabled = true
//                        self.bookingView.confirmButton.isUserInteractionEnabled = true
                        //                        self.bookingView.resetEnterDropOffButtonTarget()
                    }else{
                        self.bookingView.totalFareLabel.text = "Fare couldn't calculated!"
                    }
                })
            }
        }
    }
    
    func calculateFare(distance: Double,time:Double,category: CarCategory, completionBlock: @escaping (Double?)->Void){
        RideService.sharedInstance.getEstimatedCostForCategory(category.ID, distance: distance, time: time) { (success, responseCost, message) in
            if success{
                if let cost = responseCost{
                    completionBlock(cost)
                }else{
                    completionBlock(nil)
                }
            }else{
                completionBlock(nil)
            }
        }
    }
    
    func getTravelTimeAndDistance(from pickUpCoordinate:CLLocationCoordinate2D,to dropOffCoordinate: CLLocationCoordinate2D,completion:@escaping (_ time: Double,_ distance: Double)->Void)->Void{
        
        let origin = "\(pickUpCoordinate.latitude),\(pickUpCoordinate.longitude)"
        let destination = "\(dropOffCoordinate.latitude),\(dropOffCoordinate.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(origin)&destinations=\(destination)&key=\(GOOGLE_API_KEY)"
        Alamofire.request(url).responseJSON { response in
            do{
                let json = try JSON(data: response.data!)
                let rows = json["rows"].arrayValue
                if rows.count > 0{
                    let arrValues = rows.first!["elements"].arrayValue.first!
                    var distance = 0.0
                    if let distanceDict = arrValues["distance"].dictionaryObject as [String:AnyObject]?{
                        if let distancef = distanceDict["value"] as? Double{
                            distance = distancef
                        }
                    }
                    var duration = 0.0
                    if let durationDict = arrValues["duration"].dictionaryObject as [String:AnyObject]?{
                        if let durationf = durationDict["value"] as? Double{
                            duration = durationf
                        }
                    }
                    completion(duration,distance)
                }
            }catch{NKToastHelper.sharedInstance.showErrorAlert(self,message:"Error occurred while calculating time")}
        }
    }
    
    
    func resetEnterDropOffButtonTarget()-> Void {
        
        self.bookingView.enterDropOffButton.removeTarget(nil, action: nil, for: .allEvents)
        if estimateCost == "" {
            self.bookingView.enterDropOffButton.addTarget(self, action: #selector(HomeViewController.onEnterDroppOffAddressButton(_:)), for: .touchUpInside)
        }else{
            self.bookingView.enterDropOffButton.addTarget(self, action: #selector(HomeViewController.onShowFareBreakUps(_:)), for: .touchUpInside)
        }
    }
    
    @IBAction func onEnterDroppOffAddressButton(_ sender: UIButton){
        self.getEstimatedPrice()
        
    }
    
    @IBAction func onShowFareBreakUps(_ sender: UIButton){
        if let rFare = self.rideFare as Double?{
            self.showFareBreakUpsDialog(rideFare: rFare)
        }else{
                self.getEstimatedPrice()
        }
    }
    
    
    
    func showFareBreakUpsDialog(rideFare: Double) -> Void {
        let fareBreakUpsVC = AppStoryboard.Booking.viewController(FareBreakUpsViewController.self)
        fareBreakUpsVC.fare = rideFare
        fareBreakUpsVC.category = self.selectedCategory
        fareBreakUpsVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        let nav = UINavigationController(rootViewController: fareBreakUpsVC)
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: false, completion: nil)
    }
    
    @objc func stopAnimation(){
        self.addressPickerView.stopAnimatingToRipples()
        self.addressPickerView.stopAnimatingFromRipples()
    }
    
    
    func validateParams() -> (isValidate: Bool,message:String) {
        return(true,"")
    }
    
    
    
    func confirmRideRequest(completion:@escaping (Ride?)->Void) {
        
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        //
        AppSettings.shared.showLoader(withStatus: "Sending..")
        RideService.sharedInstance.rideRequest(self.user.ID,carCategory:self.selectedCategory.ID, children: self.selectedChildren, pickUpAddress: self.bookingPickUpAddress!, pickUpCoordinates: self.bookingPickUpCoordinates!, dropOffAddress: self.bookingDropOffAddress!, dropOffCoordinates: self.bookingDropOffCoordinates!, time: self.time, distance: distance,scheduledDate: self.ridingDate) { (errorCode,resRide,message) in
            
            AppSettings.shared.hideLoader()
            if errorCode == .success{
                if let aRide = resRide{
                    self.ride = aRide
                    if self.ride.rideType == .scheduled{
                        self.showRideScheduled()
                    }else{
                        self.showChekingNearByDrivers(rideId: self.ride.ID)
                    }
                    completion(aRide)
                }else{
                    completion(nil)
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                    self.removeBookingView()

                }
            }else if errorCode == .previousDues{
                guard let aRide = resRide else{
                    completion(nil)
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                    self.removeBookingView()

                    return
                }
                self.ride = aRide
                let amountDue = String(format: "$ %0.2f", self.ride.cost.totalPrice)
                let paymentMessage = "You have previous dues of \(amountDue), please proceed to pay that"
                NKToastHelper.sharedInstance.showErrorAlert(self, message: paymentMessage,completionBlock: {
                    let status = self.ride.getCurrentStatus()
                    if status == .paymentPending{
                        self.proceedToPayFor(ride: self.ride)
                    }
                })
                completion(nil)
                
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                self.removeBookingView()
            }
        }
    }
}




extension HomeViewController: CheckingRidesViewControllerDelegate,RideScheduledViewControllerDelegate,AddCardViewControllerDelegate{
    
    
    
    func addCard(viewController: AddCardViewController, didAddedCard card: Card) {
        viewController.dismiss(animated: true, completion: nil)
        self.confirmRideRequest { (resRide) in
            
        }
    }
    func checkingRidesViewController(viewController: CheckingRidesViewController, shouldGotoTrackRideWithRide ride: Ride) {
        viewController.dismiss(animated: false, completion: nil)
        self.openRideTracking(ride: ride)
    }
    
    func close(viewController: RideScheduledViewController) {
        viewController.dismiss(animated: false, completion: nil)
        self.navigationController?.popToRoot(true)
    }
    
    func navigateToAddNewCard(){
        let addNewPaymentOptionVC = AppStoryboard.MyCards.viewController(AddCardViewController.self)
        addNewPaymentOptionVC.shouldBackToBooking = true
        addNewPaymentOptionVC.delegate = self
        let nav = AppSettings.shared.getNavigation(vc: addNewPaymentOptionVC)
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = false
        self.present(nav, animated: true, completion: nil)
    }
    
    func proceedToPayFor(ride:Ride){
        let payForRide = AppStoryboard.Billing.viewController(PaymentOptionViewController.self)
        payForRide.user = self.user
        payForRide.ride = ride
        self.navigationController?.pushViewController(payForRide, animated: true)
    }
//
//    func openRideTracking(ride:Ride){
//        let rideTrackingVC = AppStoryboard.History.viewController(RideTrackingViewController.self)
//        rideTrackingVC.ride = ride
//        rideTrackingVC.fromHistory = false
//        self.navigationController?.pushViewController(rideTrackingVC, animated: true)
//    }
    
    func cancelRide(rideID: String,reason: String) -> Void {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        
        RideService.sharedInstance.cancelRide(self.ride.ID, reason: reason) { (success,resRide,message)  in
            if success{
                if let aRide = resRide{
                    self.ride = aRide
                    NotificationCenter.default.post(name: .RIDE_CANCELLED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
                    self.ride = Ride()
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
            
            
        }
    }
    
    func showChekingNearByDrivers(rideId:String) -> Void {

        let checkingVC = AppStoryboard.Booking.viewController(CheckingRidesViewController.self)
        checkingVC.delegate = self
        checkingVC.rideID = rideId
        checkingVC.pickupAddress = self.pickUpAddress ?? ""
        self.present(checkingVC, animated: false, completion: {
        })
    }
    
    func showRideScheduled() -> Void {
        let rideScheduledVC = AppStoryboard.Booking.viewController(RideScheduledViewController.self)
        rideScheduledVC.delegate = self
        self.present(rideScheduledVC, animated: false, completion: {
        })
    }
}







