//
//  BookingViewController.swift
//  TaxiApp
//
//  Created by TecOrb on 15/05/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//
let Rs = "$ "//"₹"

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON

class BookingViewController: UIViewController{//,CouponViewControllerDelegate {
//    @IBOutlet weak var googleMap : GMSMapView!
//    @IBOutlet weak var addressPickerView : AddressPickerView!


    @IBOutlet weak var totalFareLabel : UILabel!

    @IBOutlet weak var enterDropOffButton : UIButton!
    @IBOutlet weak var enterDropOffLabel : UILabel!


    @IBOutlet weak var personalButton : UIButton!
    @IBOutlet weak var paymentButton : UIButton!
    @IBOutlet weak var couponButton : UIButton!
    @IBOutlet weak var confirmButton : UIButton!

    var selectedCategory : CarCategory!
    var childrens: Array<Child>!
    var pickUpCoordinates: CLLocationCoordinate2D!
    var dropOffCoordinates: CLLocationCoordinate2D!
    var pickUpAddress: String!
    var dropOffAddress: String!

    var rideDate : Date?
    var rideDateStr : String?
    var estimateCost : String?


    var rideID :String!
    var ride = Ride()

    var couponCode : String?
    var time:Double = 0.0
    var distance: Double = 0.0
    var rideFare : Double?
    var user: User!
    
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.addressPickerView.isHidden = true
//        self.googleMap.isHidden = true
        self.user = User.loadSavedUser()
//        self.googleMap.isMyLocationEnabled = true
        self.getEstimatedPrice()
        self.viewSetup()
        self.setUpLeftBarButton()
//        self.addressPickerView.isUserInteractionEnabled = false
//        self.perform(#selector(BookingViewController.stopAnimation), with: nil, afterDelay: 0.8)
    }


//    @objc func stopAnimation(){
//        self.addressPickerView.stopAnimatingToRipples()
//        self.addressPickerView.stopAnimatingFromRipples()
//    }

    func viewSetup(){
        var titleH = self.selectedCategory.categoryName
        if self.selectedCategory.time != ""{
            titleH = "\(titleH), \(self.selectedCategory.time)"
        }
        self.navigationItem.title = titleH
       // addressPickerView.fromAddressLabel.text = pickUpAddress
        //self.addPickUpMarker()
       // addressPickerView.toAddressLabel.text = dropOffAddress
       // self.addDropOffMarker()
       // refreshMarkers()
        //self.resetEnterDropOffButtonTarget()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpLeftBarButton() {
        let leftbutton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(onClickBackButton(_:)))
        leftbutton.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = leftbutton
    }
    
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
        dismiss(animated: false, completion: nil)

    }
//
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        guard let touch = touches.first else{return}
//        guard let touchedView = touch.view else{return}
//        if touchedView == self.view{
////            self.navigationController?.pop(false)
//
//            dismiss(animated: false, completion: nil)
//        }
//    }

    func getEstimatedPrice() -> Void {
        self.totalFareLabel.text = Rs+"..."
        self.enterDropOffLabel.text = "Estimating fare.."

        self.getTravelTimeAndDistance(from: self.pickUpCoordinates, to: self.dropOffCoordinates) { (time, distance) in
            self.time = time
            self.distance = distance

            if distance == 0.0 && time == 0.0{
                self.totalFareLabel.text = "Fare couldn't calculated!"
            }else{
               self.calculateFare(distance: distance, time: time, category: self.selectedCategory, completionBlock: {(responseCost) in
                if let estcost = responseCost{
                    let totalCost = estcost
                    let str = String(format: "%.2f", totalCost)
                    self.totalFareLabel.text = "$"+str
                    self.rideFare = estcost
                    self.estimateCost = Rs+str
                    self.enterDropOffLabel.text = "Total fare"
                    self.resetEnterDropOffButtonTarget()
                }else{
                self.totalFareLabel.text = "Fare couldn't calculated!"
                }
                })
            }
        }
    }
    
//    func drawPath(startLocation: CLLocationCoordinate2D, endLocation: CLLocationCoordinate2D)
//    {
//        let origin = "\(startLocation.latitude),\(startLocation.longitude)"
//        let destination = "\(endLocation.latitude),\(endLocation.longitude)"
//        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving"
//
//        Alamofire.request(url).responseJSON { response in
//            let json = JSON(response.data!)
//            let routes = json["routes"].arrayValue
//
//            for route in routes
//            {
//                let routeOverviewPolyline = route["overview_polyline"].dictionary
//                let points = routeOverviewPolyline?["points"]?.stringValue
//                self.path = GMSPath.init(fromEncodedPath: points!)!
//                let polyline = GMSPolyline.init(path: self.path)
//                polyline.strokeWidth = 4
//                polyline.strokeColor =  UIColor.black
//                polyline.map = self.googleMap
//            }
//        }
//    }
    


    func calculateFare(distance: Double,time:Double,category: CarCategory, completionBlock: @escaping (Double?)->Void){
        RideService.sharedInstance.getEstimatedCostForCategory(category.ID, distance: distance, time: time) { (success, responseCost, message) in
            if success{
                if let cost = responseCost{
                    completionBlock(cost)
                }else{
                    completionBlock(nil)
                }
            }else{
                completionBlock(nil)
            }
        }
    }

    func getTravelTimeAndDistance(from pickUpCoordinate:CLLocationCoordinate2D,to dropOffCoordinate: CLLocationCoordinate2D,completion:@escaping (_ time: Double,_ distance: Double)->Void)->Void{

                let origin = "\(pickUpCoordinate.latitude),\(pickUpCoordinate.longitude)"
                let destination = "\(dropOffCoordinate.latitude),\(dropOffCoordinate.longitude)"

                let url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(origin)&destinations=\(destination)&key=\(GOOGLE_API_KEY)"
                Alamofire.request(url).responseJSON { response in
                    do{
                    let json = try JSON(data: response.data!)
                    let rows = json["rows"].arrayValue
                    if rows.count > 0{
                        let arrValues = rows.first!["elements"].arrayValue.first!
                        var distance = 0.0
                        if let distanceDict = arrValues["distance"].dictionaryObject as [String:AnyObject]?{
                            if let distancef = distanceDict["value"] as? Double{
                                distance = distancef
                            }
                        }
                        var duration = 0.0
                        if let durationDict = arrValues["duration"].dictionaryObject as [String:AnyObject]?{
                            if let durationf = durationDict["value"] as? Double{
                                duration = durationf
                            }
                        }
                        completion(duration,distance)
                    }
                    }catch{NKToastHelper.sharedInstance.showErrorAlert(self,message:"Error occurred while calculating time")}
                }
    }

    func resetEnterDropOffButtonTarget()-> Void {

        self.enterDropOffButton.removeTarget(nil, action: nil, for: .allEvents)
        if estimateCost == "" {
            self.enterDropOffButton.addTarget(self, action: #selector(BookingViewController.onEnterDroppOffAddressButton(_:)), for: .touchUpInside)
        }else{
            self.enterDropOffButton.addTarget(self, action: #selector(BookingViewController.onShowFareBreakUps(_:)), for: .touchUpInside)
        }
    }

//    func refreshMarkers() -> Void {
//        googleMap.clear()
//        self.addPickUpMarker()
//        self.addDropOffMarker()
//        self.resetBoundOfMap()
////        self.drawPath(startLocation: self.pickUpCoordinates, endLocation: self.dropOffCoordinates)
//    }
//    func addPickUpMarker() -> Void {
//        self.addMarkerforCoordinate(coordinate: self.pickUpCoordinates, image: #imageLiteral(resourceName: "greenPin"), address: pickUpAddress )
//    }
//    func addDropOffMarker() -> Void {
//        self.addMarkerforCoordinate(coordinate: self.dropOffCoordinates, image: #imageLiteral(resourceName: "redPin"), address: dropOffAddress )
//    }
//    func addMarkerforCoordinate(coordinate : CLLocationCoordinate2D?,image: UIImage,address: String) {
//        if let position = coordinate{
//            let driverMarker = GMSMarker(position: position)
//            driverMarker.icon = image
//            driverMarker.isTappable = true
//            driverMarker.title = address
//            driverMarker.map = self.googleMap;
//        }
//    }
//
//    func resetBoundOfMap() -> Void {
//        let path = GMSMutablePath()
//        path.add(self.pickUpCoordinates)
//        path.add(self.dropOffCoordinates)
//        let bounds = GMSCoordinateBounds(path: path)
//        let camera = googleMap.camera(for: bounds, insets:UIEdgeInsets(top: 55, left: 15, bottom: 15, right: 15))
//        googleMap.camera = camera!;
//    }

    @IBAction func onEnterDroppOffAddressButton(_ sender: UIButton){
        self.getEstimatedPrice()
    }

    @IBAction func onShowFareBreakUps(_ sender: UIButton){
        if let rFare = self.rideFare as Double?{
            self.showFareBreakUpsDialog(rideFare: rFare)
        }else{
            self.getEstimatedPrice()
        }
    }

    func showFareBreakUpsDialog(rideFare: Double) -> Void {
        let fareBreakUpsVC = AppStoryboard.Booking.viewController(FareBreakUpsViewController.self)
        fareBreakUpsVC.fare = rideFare
        fareBreakUpsVC.category = self.selectedCategory
        fareBreakUpsVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        let nav = UINavigationController(rootViewController: fareBreakUpsVC)
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: false, completion: nil)
    }

    func showCouponDialog() -> Void {
        NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.functionalityPending.rawValue)
    }

//    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
//        self.navigationController?.pop(true)
//    }

    @IBAction func onClickPersonalButton(_ sender: UIButton){
    }

    @IBAction func onClickCouponButton(_ sender: UIButton){
        showCouponDialog()
    }

    @IBAction func onClicConfirmBookingButton(_ sender: UIButton){
//        self.verifyPaymentMethodsAdded()

        //validate pickup and drop off coordinate
            let validation = self.validateParams()
            if !validation.isValidate{
                return
            }
            //hit service for booking
            self.confirmRideRequest {(resRide) in
                if let ride = resRide{
                    if ride.ID != ""{
                        //self.showChekingNearByDrivers()
                    }
                }
            }
    }
    
    
    func verifyPaymentMethodsAdded(){
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        PaymentService.sharedInstance.getCardsForUser() { (success,resCards,message)  in
            if success{
                if let someCards = resCards{
                    if someCards.count == 0{
                        AppSettings.shared.hideLoader()
                        self.navigateToAddNewCard()
                    }else{
                        self.confirmRideRequest {(resRide) in
                            //nothing to do
                        }
                    }
                }else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    func validateParams() -> (isValidate: Bool,message:String) {
        return(true,"")
    }


    
    func confirmRideRequest(completion:@escaping (Ride?)->Void) {
        
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        //
        AppSettings.shared.showLoader(withStatus: "Sending..")
        RideService.sharedInstance.rideRequest(self.user.ID,carCategory:self.selectedCategory.ID, children: self.childrens, pickUpAddress: self.pickUpAddress!, pickUpCoordinates: self.pickUpCoordinates!, dropOffAddress: dropOffAddress!, dropOffCoordinates: self.dropOffCoordinates!, time: self.time, distance: distance,scheduledDate: self.rideDate) { (errorCode,resRide,message) in
            
            AppSettings.shared.hideLoader()
            if errorCode == .success{
                if let aRide = resRide{
                    self.ride = aRide
                    if self.ride.rideType == .scheduled{
                        self.showRideScheduled()
                    }else{
                        self.showChekingNearByDrivers(rideId: self.ride.ID)
                    }
                    completion(aRide)
                }else{
                    completion(nil)
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else if errorCode == .previousDues{
                guard let aRide = resRide else{
                    completion(nil)
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                    return
                }
                self.ride = aRide
                let amountDue = String(format: "$ %0.2f", self.ride.cost.totalPrice)
                let paymentMessage = "You have previous dues of \(amountDue), please proceed to pay that"
                NKToastHelper.sharedInstance.showErrorAlert(self, message: paymentMessage,completionBlock: {
                    let status = self.ride.getCurrentStatus()
                    if status == .paymentPending{
                        self.proceedToPayFor(ride: self.ride)
                    }
                })
                completion(nil)
                
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }


    
    
//    func confirmRideRequest(completion:@escaping (Ride?)->Void) {
//
//        if !AppSettings.isConnectedToNetwork{
//            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
//            return
//        }
//        AppSettings.shared.showLoader(withStatus: "Sending..")
//        RideService.sharedInstance.rideRequest(self.user.ID,carCategory:self.selectedCategory.ID, children: self.children, pickUpAddress: self.pickUpAddress!, pickUpCoordinates: self.pickUpCoordinates!, dropOffAddress: dropOffAddress!, dropOffCoordinates: self.dropOffCoordinates!, time: self.time, distance: distance,scheduledDate: self.rideDate) { (success,resRide,message) in
//            AppSettings.shared.hideLoader()
//            if success{
//                if let aRide = resRide{
//                    self.ride = aRide
//                    if self.ride.rideType == .scheduled{
//                        self.showRideScheduled()
//                    }else{
//                        self.showChekingNearByDrivers(rideId: self.ride.ID)
//                    }
//                    completion(aRide)
//                }else{
//                    completion(nil)
//                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
//                }
//            }else{
//                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
//            }
//        }
//    }
//
    

}


extension BookingViewController: CheckingRidesViewControllerDelegate,RideScheduledViewControllerDelegate,AddCardViewControllerDelegate{
    

    
    func addCard(viewController: AddCardViewController, didAddedCard card: Card) {
        viewController.dismiss(animated: true, completion: nil)
        self.confirmRideRequest { (resRide) in
            
        }
    }
    func checkingRidesViewController(viewController: CheckingRidesViewController, shouldGotoTrackRideWithRide ride: Ride) {
        viewController.dismiss(animated: false, completion: nil)
        self.openRideTracking(ride: ride)
    }

    func close(viewController: RideScheduledViewController) {
        viewController.dismiss(animated: false, completion: nil)
        self.navigationController?.popToRoot(true)
    }
    
    func navigateToAddNewCard(){
        let addNewPaymentOptionVC = AppStoryboard.MyCards.viewController(AddCardViewController.self)
        addNewPaymentOptionVC.shouldBackToBooking = true
        addNewPaymentOptionVC.delegate = self
        let nav = AppSettings.shared.getNavigation(vc: addNewPaymentOptionVC)
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = false
        self.present(nav, animated: true, completion: nil)
    }

    func proceedToPayFor(ride:Ride){
        let payForRide = AppStoryboard.Billing.viewController(PaymentOptionViewController.self)
        payForRide.user = self.user
        payForRide.ride = ride
        self.navigationController?.pushViewController(payForRide, animated: true)
    }
    
    func openRideTracking(ride:Ride){
        let rideTrackingVC = AppStoryboard.History.viewController(RideTrackingViewController.self)
        rideTrackingVC.ride = ride
        rideTrackingVC.fromHistory = false
        self.navigationController?.pushViewController(rideTrackingVC, animated: true)
    }

    func cancelRide(rideID: String,reason: String) -> Void {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }

        RideService.sharedInstance.cancelRide(self.ride.ID, reason: reason) { (success,resRide,message)  in
            if success{
                if let aRide = resRide{
                    self.ride = aRide
                    NotificationCenter.default.post(name: .RIDE_CANCELLED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
                    self.ride = Ride()
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }


        }
    }

    func showChekingNearByDrivers(rideId:String) -> Void {
        let checkingVC = AppStoryboard.Booking.viewController(CheckingRidesViewController.self)
        checkingVC.delegate = self
        checkingVC.rideID = rideId
        checkingVC.pickupAddress = self.pickUpAddress
        self.present(checkingVC, animated: false, completion: {
        })
    }

    func showRideScheduled() -> Void {
        let rideScheduledVC = AppStoryboard.Booking.viewController(RideScheduledViewController.self)
        rideScheduledVC.delegate = self
        self.present(rideScheduledVC, animated: false, completion: {
        })
    }
}





