//
//  AddChildViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 14/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import RSKPlaceholderTextView
protocol AddChildViewControllerDelegate {
    func addChildViewController(viewController:AddChildViewController, didAddChild child:Child)
    func addChildViewController(viewController:AddChildViewController, didUpdateChild child:Child)

}
extension AddChildViewControllerDelegate{
    func addChildViewController(viewController:AddChildViewController, didUpdateChild child:Child){
        //default body
    }
}

class AddChildViewController: UIViewController {
    var isFromSignUp:Bool = false
    var dob: Date!
    var fullName = ""
    var alergies = ""
    var gender: Gender = .male
    var school:String = ""
    var teacher:String = ""
    var child: Child?
    var delegate: AddChildViewControllerDelegate?
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var addButton : UIButton!

    let placeholderArray = ["CHILD NAME","GENDER","DATE OF BIRTH","SCHOOL","TEACHER", "SPECIAL INSTRUCTIONS"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.regiterNIbs()
        self.setupLeftBarButton()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        if let child = self.child{
            self.setupChildDetails(child: child)
            self.navigationItem.title = "UPDATE CHILD"
        }else{
            self.navigationItem.title = "ADD CHILD"
        }
    }

    func setupChildDetails(child:Child){
        self.fullName = child.name
        self.gender = child.gender
        self.dob = CommonClass.getDateFromDateString(child.dateOfBirth)
        self.school = child.school
        self.teacher = child.teacher
        self.alergies = child.alergies
        self.addButton.setTitle("Update", for: .normal)
        self.tableView.reloadData()
    }

    func setupLeftBarButton(){
        if self.isFromSignUp{
            let leftButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
            self.navigationItem.leftBarButtonItem = leftButton
        }else{
            let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back_dark"), style: .plain, target: self, action: #selector(onClickBackButton(_:)))
            leftButton.tintColor = .black
            self.navigationItem.leftBarButtonItem = leftButton
        }
    }

    func regiterNIbs(){
        tableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "TextFieldCell")
        tableView.register(UINib(nibName: "AlergiesCell", bundle: nil), forCellReuseIdentifier: "AlergiesCell")
        tableView.register(UINib(nibName: "DOBCell", bundle: nil), forCellReuseIdentifier: "DOBCell")
        tableView.register(UINib(nibName: "HeaderCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        tableView.register(UINib(nibName: "GenderCell", bundle: nil), forCellReuseIdentifier: "GenderCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickAdd(_ sender: UIButton){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }

//        if self.dob == nil{
//            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter date of birth")
//            return
//        }
        let validation = self.validateParams(fullName:self.fullName, school: self.school, teacher: self.teacher)
        if !validation.success{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: validation.message)
            return
        }

        if let child = self.child{
            self.updateChild(childID:child.id,fullName: self.fullName, gender: self.gender, dob: self.dob, school: self.school, teacher: self.teacher, alergies: self.alergies)
        }else{
            self.addChild(fullName: self.fullName, gender: self.gender, dob: self.dob, school: self.school, teacher: self.teacher, alergies: self.alergies)
        }
    }

    func validateParams(fullName:String, school:String, teacher: String) -> (success:Bool,message:String){

        if fullName.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your child name")
        }

        if self.dob == nil{
            return (false,"Please enter date of birth")
        }

        if school.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter school name")
        }
        if teacher.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter teacher name")
        }

        return (true,"")
    }

    func addChild(fullName:String,gender: Gender, dob:Date, school:String, teacher:String, alergies:String?){
        AppSettings.shared.showLoader(withStatus: "Adding..")
        LoginService.sharedInstance.addChild(name: fullName, gender: gender, dateOfBirth: dob, school: school, teacher: teacher, alergies: alergies) { (success, resChild, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let child = resChild{
                    self.delegate?.addChildViewController(viewController: self, didAddChild: child)
                    if self.isFromSignUp{
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        self.navigationController?.pop(true)
                    }
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }

        }
    }

    func updateChild(childID:String, fullName:String, gender: Gender, dob:Date, school:String, teacher:String, alergies:String?){
        AppSettings.shared.showLoader(withStatus: "Updating..")
        LoginService.sharedInstance.updateChild(childId:childID, name: fullName, gender: gender, dateOfBirth: dob, school: school, teacher: teacher, alergies: alergies) { (success, resChild, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let child = resChild{
                    self.delegate?.addChildViewController(viewController: self, didUpdateChild: child)
                    if self.isFromSignUp{
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        self.navigationController?.pop(true)
                    }
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }

        }
    }
}

extension AddChildViewController: UITableViewDataSource,UITableViewDelegate,GenderCellDelegate,DOBCellDelegate{
    func gender(genderCell cell: GenderCell, didSelectGender gender: Gender) {
        self.gender = gender
        self.tableView.reloadData()
    }
    func dateOfBirth(dobCell cell: DOBCell, didSelectDate date: Date) {
        self.dob = date
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
        header.headerTitleLabel.text = "Child Details"
        return header
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeholderArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == placeholderArray.count-1{
            return 100
        }else{
            return 68
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = placeholderArray[indexPath.row].capitalized
            cell.textField.text = fullName
            cell.textField.delegate = self
            cell.textField.addTarget(self, action: #selector(textFieldDidChangeText(_:)), for: .editingChanged)
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GenderCell", for: indexPath) as! GenderCell
            cell.selectedGender = self.gender
            cell.delegate = self
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DOBCell", for: indexPath) as! DOBCell
            cell.dob = self.dob
            cell.dobTextField.placeholder = placeholderArray[indexPath.row].capitalized
            cell.delegate = self
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = placeholderArray[indexPath.row].capitalized
            cell.textField.text = school
            cell.textField.delegate = self
            cell.textField.addTarget(self, action: #selector(textFieldDidChangeText(_:)), for: .editingChanged)
            return cell
        }else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = placeholderArray[indexPath.row].capitalized
            cell.textField.text = teacher
            cell.textField.delegate = self
            cell.textField.addTarget(self, action: #selector(textFieldDidChangeText(_:)), for: .editingChanged)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AlergiesCell", for: indexPath) as! AlergiesCell
            cell.textView.hint = placeholderArray[indexPath.row].capitalized
            cell.textView.text = alergies
            cell.textView.delegate = self
            return cell
        }
    }

}

extension AddChildViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.tableView){
            switch indexPath.row{
            case 0:
                self.fullName = textField.text ?? ""
            case 3:
                self.school = textField.text ?? ""
            case 4:
                self.teacher = textField.text ?? ""
            default:
                break
            }
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.tableView){
            switch indexPath.row{
            case 0:
                self.fullName = textField.text ?? ""
            case 3:
                self.school = textField.text ?? ""
            case 4:
                self.teacher = textField.text ?? ""
            default:
                break
            }
        }
    }

    @IBAction func textFieldDidChangeText(_ textField: UITextField){
        if let indexPath = textField.tableViewIndexPath(self.tableView){
            switch indexPath.row{
            case 0:
                self.fullName = textField.text ?? ""
            case 3:
                self.school = textField.text ?? ""
            case 4:
                self.teacher = textField.text ?? ""
            default:
                break
            }
        }
    }
}


extension AddChildViewController:UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if let indexPath = textView.tableViewIndexPath(self.tableView){
            switch indexPath.row{
            case 5:
                self.alergies = textView.text
            default:
                break
            }
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        if let indexPath = textView.tableViewIndexPath(self.tableView){
            switch indexPath.row{
            case 5:
                self.alergies = textView.text
            default:
                break
            }
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if let indexPath = textView.tableViewIndexPath(self.tableView){
            switch indexPath.row{
            case 5:
                self.alergies = textView.text
            default:
                break
            }
        }
    }
}







