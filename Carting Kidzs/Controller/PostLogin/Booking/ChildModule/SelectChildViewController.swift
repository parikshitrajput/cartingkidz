//
//  SelectChildViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 13/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
protocol SelectChildViewControllerDelegate {
    func child(_ viewController: SelectChildViewController, didSelectChildren children:Array<Child>)
}

class SelectChildViewController: UIViewController {
    var childrens = Array<Child>()
    var selectedChildren = Array<Bool>()
    var delegate : SelectChildViewControllerDelegate?
    @IBOutlet weak var tableView : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedChildren = Array(repeating: false, count: self.childrens.count)
        tableView.register(UINib(nibName: "ChildCell", bundle: nil), forCellReuseIdentifier: "ChildCell")
        tableView.register(UINib(nibName: "AddChildCell", bundle: nil), forCellReuseIdentifier: "AddChildCell")
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.getchilds()
    }

    @IBAction func onClickDone(_ sender: UIBarButtonItem){
        self.performFinalSelection()
    }

    @IBAction func onClickDoneButton(_ sender: UIButton){
        self.performFinalSelection()
    }

    func performFinalSelection(){
        var selectedArray = Array<Child>()
        for item in 0..<self.selectedChildren.count{
            if self.selectedChildren[item]{
                selectedArray.append(self.childrens[item])
            }
        }
        delegate?.child(self, didSelectChildren: selectedArray)
    }

    @IBAction func onClickClose(_ sender: UIBarButtonItem){
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getchilds(){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue){self.dismiss(animated: true, completion: nil)}
            return
        }
        AppSettings.shared.showLoader(withStatus: "Loading..")
        LoginService.sharedInstance.getchildren { (success, resChildren, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let someChildren = resChildren{
                    self.childrens.append(contentsOf: someChildren)
                    let someSelectedArray = Array(repeating: false, count: someChildren.count)
                    self.selectedChildren.append(contentsOf: someSelectedArray)
                    self.tableView.reloadData()
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }

    }
}

extension SelectChildViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0) ? self.childrens.count : 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddChildCell", for: indexPath) as! AddChildCell
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChildCell", for: indexPath) as! ChildCell
            let child = self.childrens[indexPath.row]
            cell.childImage.sd_setImage(with:URL(string: child.image), placeholderImage: AppSettings.shared.userAvatarImage(username:"\(child.name)"))
            CommonClass.makeViewCircular(cell.childImage)
            cell.childNameLabel.text = child.name
            cell.dobLabel.text = child.dateOfBirth
            cell.selectionTick.isSelected = self.selectedChildren[indexPath.row]
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? ChildCell{
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            CommonClass.makeViewCircular(cell.childImage, borderColor: appColor.red, borderWidth: 1)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            self.selectedChildren[indexPath.row] = !self.selectedChildren[indexPath.row]
            tableView.reloadData()
        }else{
            self.openAddChildVC(fromSignUp: false)
        }
    }

    func openAddChildVC(fromSignUp isFromSignUp:Bool){
        let addChildVC = AppStoryboard.Booking.viewController(AddChildViewController.self)
        addChildVC.isFromSignUp = false
        addChildVC.delegate = self
        self.navigationController?.pushViewController(addChildVC, animated: true)
    }
}

extension SelectChildViewController: AddChildViewControllerDelegate{
    func addChildViewController(viewController: AddChildViewController, didAddChild child: Child) {
        self.childrens.append(child)
        self.selectedChildren.append(false)
        self.tableView.reloadData()
    }
}






