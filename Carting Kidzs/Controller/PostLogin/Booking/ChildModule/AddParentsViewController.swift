//
//  AddParentsViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 16/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import CountryPickerView

protocol AddParentsViewControllerDelegate {
    func addParents(viewController: AddParentsViewController, didUpdateParent parent: Parent)
    func addParents(viewController: AddParentsViewController, didAddNewParent parent: Parent)
}

class AddParentsViewController: UIViewController {
    var isFromSignUp = false
    var countryCode: String?
    var fullName = ""
    var gender: Gender = .male
    var contact:String = ""
    var address:String = ""
    var city:String = ""
    var zipcode:String = ""
    var selectedCountry: Country?
    var myParent: Parent?
    var isAdditionalParent: Bool = false
    var delegate: AddParentsViewControllerDelegate?
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var addButton : UIButton!

    let placeholderArray = ["Parent Name","Contact","Full Address","City","Zipcode"]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        if let parent = self.myParent{
            self.navigationItem.title = "UPDATE PARENTS"
            self.addButton.setTitle("UPDATE", for: UIControlState())
            let cpv = CountryPickerView(frame: CGRect.zero)
            self.selectedCountry = cpv.getCountryByPhoneCode("+"+parent.countryCode)
            self.fullName = parent.name
            self.contact = parent.contact
            self.address = parent.address
            self.city = parent.city
            self.zipcode = parent.zipCode
        }else{

            self.navigationItem.title = isAdditionalParent ? "ADD ADDITIONAL PARENTS" : "ADD PARENTS"
            self.addButton.setTitle("ADD", for: UIControlState())
        }

        self.regiterNIbs()
        self.setupLeftBarButton()
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    func setupLeftBarButton(){
        if self.isFromSignUp{
            let leftButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
            self.navigationItem.leftBarButtonItem = leftButton
        }else{
            let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back_dark"), style: .plain, target: self, action: #selector(onClickBackButton(_:)))
            leftButton.tintColor = .black
            self.navigationItem.leftBarButtonItem = leftButton
        }
    }

    func regiterNIbs(){
        tableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "TextFieldCell")
        tableView.register(UINib(nibName: "AlergiesCell", bundle: nil), forCellReuseIdentifier: "AlergiesCell")
        tableView.register(UINib(nibName: "HeaderCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        tableView.register(UINib(nibName: "GenderCell", bundle: nil), forCellReuseIdentifier: "GenderCell")
        tableView.register(UINib(nibName: "ContactCell", bundle: nil), forCellReuseIdentifier: "ContactCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickAdd(_ sender: UIButton){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }

        let validation = self.validateParams(fullName: self.fullName, phoneNumber: self.contact, address: self.address,city: self.city, zipCode: self.zipcode)
        if !validation.success{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: validation.message)
            return
        }

        let phoneNumber = self.contact
        let countryCode = self.selectedCountry!.phoneCode

        guard let parent = self.myParent else {
            self.addParent(parentName: self.fullName, gender: self.gender, contact: phoneNumber, countryCode: countryCode, address: self.address, city: self.city, zipCode: self.zipcode)
            return
        }
        self.updateParent(id: parent.id, parentName: self.fullName, contact: phoneNumber, countryCode: countryCode, address: self.address, city: self.city, zipCode: self.zipcode)
    }

    func validateParams(fullName:String, phoneNumber: String, address:String, city:String, zipCode:String) -> (success:Bool,message:String){
        guard let country = self.selectedCountry else {
            return (false,"Please select your country")
        }
        if fullName.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your parent name")
        }
        if phoneNumber.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your phone number")
        }

        let phoneValidation = CommonClass.validatePhoneNumber(phoneNumber.trimmingCharacters(in: .whitespaces))
        if !phoneValidation{
            return (false,"Please enter a valid contact number")
        }

        if (phoneNumber.trimmingCharacters(in: .whitespaces).count < country.minDigit){
            return (false,"Contact should not be less than \(country.minDigit) in length")
        }

        if  (phoneNumber.trimmingCharacters(in: .whitespaces).count > country.maxDigit){
            return (false,"Contact number should not be more than \(country.maxDigit) in length")
        }

        if address.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return (false,"Please enter parent address")
        }
        
        if city.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return (false,"Please enter parent city")
        }
        if zipCode.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return (false,"Please enter parent zipcode")
        }
        return (true,"")
    }


    func addParent(parentName:String, gender:Gender, contact:String,countryCode:String, address:String, city:String, zipCode:String){
        AppSettings.shared.showLoader(withStatus: "Adding..")
        LoginService.sharedInstance.addParent(name: parentName, phoneNumber: contact, countryCode: countryCode, address: address, city: city, zipCode: zipCode) { (success, resParent, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let parent = resParent{
                    self.delegate?.addParents(viewController: self, didAddNewParent: parent)
                    if self.isFromSignUp{
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        self.navigationController?.pop(false)
                    }
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }
    
    func updateParent(id:String, parentName:String, contact:String,countryCode:String, address:String, city:String, zipCode:String){
        AppSettings.shared.showLoader(withStatus: "Updating..")
        LoginService.sharedInstance.updateParent(id: id, name: parentName, phoneNumber: contact, countryCode: countryCode, address: address, city: city, zipCode: zipCode) { (success, resParent, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let parent = resParent{
                    self.delegate?.addParents(viewController: self, didUpdateParent: parent)
                    if self.isFromSignUp{
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        self.navigationController?.pop(false)
                    }
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }
}

extension AddParentsViewController: UITableViewDataSource,UITableViewDelegate,GenderCellDelegate,ContactCellDelegate{
    func contact(contactCell cell: ContactCell, didSelectCountry selectedCountry: Country) {
        self.selectedCountry = selectedCountry
        self.countryCode = selectedCountry.phoneCode
        self.tableView.reloadData()
    }

    func gender(genderCell cell: GenderCell, didSelectGender gender: Gender) {
        self.gender = gender
        self.tableView.reloadData()
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
        header.headerTitleLabel.text = isAdditionalParent ? "Additional Parents Details" : "Parents Details"
        return header
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeholderArray.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2{
            return 68
        }else{
            return 68
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = placeholderArray[indexPath.row].capitalized
            cell.textField.text = fullName
            cell.textField.delegate = self
            self.setUpTextColor(textField: cell.textField)
            cell.textField.keyboardType = .default
            cell.textField.addTarget(self, action: #selector(textFieldDidChangeText(_:)), for: .editingChanged)
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactCell
            cell.viewController = self
            cell.phoneNumberTextField.placeholder = placeholderArray[indexPath.row].capitalized
            cell.phoneNumberTextField.text = contact
            cell.phoneNumberTextField.keyboardType = .phonePad
            cell.phoneNumberTextField.delegate = self
            cell.delegate = self
            cell.phoneNumberLabel.textColor = appColor.red
            cell.countryPickerSetUp(with: self.selectedCountry)
            cell.phoneNumberTextField.addTarget(self, action: #selector(textFieldDidChangeText(_:)), for: .editingChanged)
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = placeholderArray[indexPath.row].capitalized
            cell.textField.text = address
            cell.textField.keyboardType = .default
            cell.textField.delegate = self
            self.setUpTextColor(textField: cell.textField)
            cell.textField.addTarget(self, action: #selector(textFieldDidChangeText(_:)), for: .editingChanged)
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = placeholderArray[indexPath.row].capitalized
            cell.textField.text = city
            cell.textField.keyboardType = .default
            cell.textField.delegate = self
            self.setUpTextColor(textField: cell.textField)
            cell.textField.addTarget(self, action: #selector(textFieldDidChangeText(_:)), for: .editingChanged)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = placeholderArray[indexPath.row].capitalized
            cell.textField.text = zipcode
            cell.textField.keyboardType = .numberPad
            cell.textField.delegate = self
            self.setUpTextColor(textField: cell.textField)
            cell.textField.addTarget(self, action: #selector(textFieldDidChangeText(_:)), for: .editingChanged)
            return cell
        }
    }
    
    func setUpTextColor(textField:FloatLabelTextField){
        textField.titleTextColour = appColor.red
        textField.titleActiveTextColour = appColor.red
    }

    func setUpTextColor(textField:FloatLabelTextView){
        textField.titleTextColour = appColor.red
        textField.titleActiveTextColour = appColor.red
    }
}

extension AddParentsViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.tableView){
            switch indexPath.row{
            case 0:
                self.fullName = textField.text ?? ""
            case 1:
                self.contact = textField.text ?? ""
            case 2:
                self.address = textField.text ?? ""
            case 3:
                self.city = textField.text ?? ""
            case 4:
                self.zipcode = textField.text ?? ""
            default:
                break
            }
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.tableView){
            switch indexPath.row{
            case 0:
                self.fullName = textField.text ?? ""
            case 1:
                self.contact = textField.text ?? ""
            case 2:
                self.address = textField.text ?? ""
            case 3:
                self.city = textField.text ?? ""
            case 4:
                self.zipcode = textField.text ?? ""
            default:
                break
            }
        }
    }

    @IBAction func textFieldDidChangeText(_ textField: UITextField){
        if let indexPath = textField.tableViewIndexPath(self.tableView){
            switch indexPath.row{
            case 0:
                self.fullName = textField.text ?? ""
            case 1:
                self.contact = textField.text ?? ""
            case 2:
                self.address = textField.text ?? ""
            case 3:
                self.city = textField.text ?? ""
            case 4:
                self.zipcode = textField.text ?? ""
            default:
                break
            }
        }
    }
}









