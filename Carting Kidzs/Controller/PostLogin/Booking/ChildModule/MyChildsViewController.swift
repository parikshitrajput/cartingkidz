//
//  MyChildsViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 31/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class MyChildsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var childrens = Array<Child>()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ChildListCell", bundle: nil), forCellReuseIdentifier: "ChildListCell")
        tableView.register(UINib(nibName: "AddChildCell", bundle: nil), forCellReuseIdentifier: "AddChildCell")
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.getchilds()
        
    }

    @IBAction func onClickMenu(_ sender: UIBarButtonItem){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickRemoveButton(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.tableView) as IndexPath?{
            let child = childrens[indexPath.row]
            self.askToDeleteChild(child: child, atIndexPath: indexPath)
        }
    }

    func askToDeleteChild(child : Child,atIndexPath indexPath: IndexPath) {
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "Would you really want to delete this child from list?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Delete", style: .destructive){[weak child](action) in
            alert.dismiss(animated: true, completion: nil)
            self.removeChild(child, indexPath: indexPath)
        }

        let cancelAction = UIAlertAction(title: "Nope", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(okayAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }

    func removeChild(_ myChild:Child?,indexPath: IndexPath) -> Void {
        guard let child = myChild else {
            return
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        LoginService.sharedInstance.deleteChild(id: child.id){ (success,message) in
            AppSettings.shared.hideLoader()
            if success{
                self.childrens.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
        }
    }

    func getchilds(){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue){self.dismiss(animated: true, completion: nil)}
            return
        }
        
        AppSettings.shared.showLoader(withStatus: "Loading..")
        LoginService.sharedInstance.getchildren { (success, resChildren, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let someChildren = resChildren{
                    self.childrens.append(contentsOf: someChildren)
                    self.tableView.reloadData()
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }
}

extension MyChildsViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0) ? self.childrens.count : 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddChildCell", for: indexPath) as! AddChildCell
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChildListCell", for: indexPath) as! ChildListCell
            let child = self.childrens[indexPath.row]
            cell.childImage.sd_setImage(with:URL(string: child.image), placeholderImage: AppSettings.shared.userAvatarImage(username:"\(child.name)"))
            CommonClass.makeViewCircular(cell.childImage)
            cell.childNameLabel.text = child.name
            cell.dobLabel.text = child.dateOfBirth
            cell.removeButton.addTarget(self, action: #selector(onClickRemoveButton(_:)), for: .touchUpInside)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? ChildCell{
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            CommonClass.makeViewCircular(cell.childImage, borderColor: appColor.red, borderWidth: 1)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            let child = childrens[indexPath.row]
            self.openUpdateChildVC(fromSignUp: false, child: child)
        }else{
            self.openAddChildVC(fromSignUp: false)
        }
    }

    func openAddChildVC(fromSignUp isFromSignUp:Bool){
        let addChildVC = AppStoryboard.Booking.viewController(AddChildViewController.self)
        addChildVC.isFromSignUp = false
        addChildVC.delegate = self
        self.navigationController?.pushViewController(addChildVC, animated: true)
    }
    func openUpdateChildVC(fromSignUp isFromSignUp:Bool, child:Child){
        let addChildVC = AppStoryboard.Booking.viewController(AddChildViewController.self)
        addChildVC.child = child
        addChildVC.isFromSignUp = false
        addChildVC.delegate = self
        self.navigationController?.pushViewController(addChildVC, animated: true)
    }
}

extension MyChildsViewController: AddChildViewControllerDelegate{
    func addChildViewController(viewController: AddChildViewController, didAddChild child: Child) {
        self.childrens.append(child)
        self.tableView.reloadData()
    }

    func addChildViewController(viewController: AddChildViewController, didUpdateChild child: Child) {
        for (index,mychild) in self.childrens.enumerated(){
            if child.id == mychild.id{
                self.childrens[index] = child
                self.tableView.reloadData()
            }
        }
    }
}
