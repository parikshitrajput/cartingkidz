//
//  ParentListViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 15/10/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class ParentListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var parents = Array<Parent>()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ChildListCell", bundle: nil), forCellReuseIdentifier: "ChildListCell")
        tableView.register(UINib(nibName: "AddChildCell", bundle: nil), forCellReuseIdentifier: "AddChildCell")
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.getParents()
    }

    @IBAction func onClickMenu(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickRemoveButton(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.tableView) as IndexPath?{
            let parent = parents[indexPath.row]
            self.askToDelete(parent: parent, atIndexPath: indexPath)
        }
    }

    func askToDelete(parent : Parent,atIndexPath indexPath: IndexPath) {
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "Would you really want to delete this parent from list?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Delete", style: .destructive){[weak parent](action) in
            alert.dismiss(animated: true, completion: nil)
            self.remove(parent, indexPath: indexPath)
        }

        let cancelAction = UIAlertAction(title: "Nope", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(okayAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }

    func remove(_ myParent:Parent?,indexPath: IndexPath) -> Void {
        guard let parent = myParent else {
            return
        }

        AppSettings.shared.showLoader(withStatus: "Please wait..")
        LoginService.sharedInstance.deleteParent(id: parent.id){ (success,message) in
            AppSettings.shared.hideLoader()
            if success{
                self.parents.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
        }
    }

    func getParents(){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue){self.dismiss(animated: true, completion: nil)}
            return
        }

        AppSettings.shared.showLoader(withStatus: "Loading..")
        LoginService.sharedInstance.getParent { (success, resParents, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let someParents = resParents{
                    self.parents.append(contentsOf: someParents)
                    self.tableView.reloadData()
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }
}

extension ParentListViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0) ? self.parents.count : 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddChildCell", for: indexPath) as! AddChildCell
            cell.messageLabel.text = "Add Additional Parent"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChildListCell", for: indexPath) as! ChildListCell
            let parent = self.parents[indexPath.row]
            cell.childImage.sd_setImage(with:URL(string: parent.image), placeholderImage: AppSettings.shared.userAvatarImage(username:"\(parent.name)"))
            CommonClass.makeViewCircular(cell.childImage)
            cell.childNameLabel.text = parent.name
            cell.dobLabel.text = "+"+parent.countryCode+parent.contact
            cell.removeButton.addTarget(self, action: #selector(onClickRemoveButton(_:)), for: .touchUpInside)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? ChildCell{
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            CommonClass.makeViewCircular(cell.childImage, borderColor: appColor.red, borderWidth: 1)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            let parent = parents[indexPath.row]
            self.openUpdateParentVC(fromSignUp: false, parent: parent)
        }else{
            self.openAddParentVC(fromSignUp: false)
        }
    }

    func openAddParentVC(fromSignUp isFromSignUp:Bool){
        let addParentVC = AppStoryboard.Booking.viewController(AddParentsViewController.self)
        addParentVC.isFromSignUp = false
        addParentVC.delegate = self
        addParentVC.isAdditionalParent = true
        self.navigationController?.pushViewController(addParentVC, animated: true)
    }

    func openUpdateParentVC(fromSignUp isFromSignUp:Bool, parent:Parent){
        let addParentVC = AppStoryboard.Booking.viewController(AddParentsViewController.self)
        addParentVC.myParent = parent
        addParentVC.isFromSignUp = false
        addParentVC.delegate = self
        self.navigationController?.pushViewController(addParentVC, animated: true)
    }
}

extension ParentListViewController: AddParentsViewControllerDelegate{
    func addParents(viewController: AddParentsViewController, didUpdateParent parent: Parent) {
        for (index,myParent) in self.parents.enumerated(){
            if parent.id == myParent.id{
                self.parents[index] = parent
                self.tableView.reloadData()
            }
        }
    }

    func addParents(viewController: AddParentsViewController, didUpdateUser parent: Parent) {

    }

    func addParents(viewController: AddParentsViewController, didAddNewParent parent: Parent) {
        self.parents.append(parent)
        self.tableView.reloadData()
    }
}
