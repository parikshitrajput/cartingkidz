//
//  FareBreakUpsViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 12/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

let tax:Double = 0//10

class FareBreakUpsViewController: UIViewController {
    var fare:Double!
    var category: CarCategory!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var carsNameLabel: UILabel!
    @IBOutlet weak var rideFareLabel: UILabel!
    @IBOutlet weak var taxesLabel: UILabel!
    @IBOutlet weak var totalFareLabel: UILabel!
    @IBOutlet weak var remarkLabel: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoryNameLabel.text = "FARE DETAILS"
        self.carsNameLabel.text = category.carsName.capitalized
        self.rideFareLabel.text = String(format: "$ %0.2f",fare)
        let actualCost = (100*fare)/(100+tax)
        let includingTaxes = fare - actualCost
        self.taxesLabel.text = String(format: "$ %0.2f",includingTaxes)
        self.totalFareLabel.text = String(format: "$ %0.2f",fare)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else{return}
        guard let touchedView = touch.view else{return}
        if touchedView == self.view{
            dismiss(animated: true, completion: nil)
        }
    }



    @IBAction func onClickGotIt(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}
