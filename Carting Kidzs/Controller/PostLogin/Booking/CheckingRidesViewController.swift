//
//  CheckingRidesViewController.swift
//  TaxiApp
//
//  Created by TecOrb on 16/05/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import GoogleMaps
import DGActivityIndicatorView
import Firebase

protocol CheckingRidesViewControllerDelegate {
    func checkingRidesViewController(viewController: CheckingRidesViewController,shouldGotoTrackRideWithRide ride:Ride) -> Void
}

enum CheckingRidesCancelReason:String {
    case fromCloseButton = "from close button"
    case timeExceed = "time exceed"
}


enum RideTrackingKeys:String{
    case isCancelledByDriver = "is_cancelled_by_driver"
    case isReached = "is_reached"
    case isEnded = "is_ride_ended"
    case isStarted = "is_started"
}

class CheckingRidesViewController: UIViewController,DriverDetailOnConfirmRideViewDelegate {
    @IBOutlet weak var mapImageView: UIImageView!

    @IBOutlet weak var driverCheckingView: UIView!
    @IBOutlet weak var noRideView: UIView!
    @IBOutlet weak var bookingConfirmView: UIView!
    @IBOutlet weak var tipLabel: UILabel!

    var activityIndicator : DGActivityIndicatorView!
    var delegate : CheckingRidesViewControllerDelegate?
    var pickupAddress = ""
    var rideID = ""
    var rideRequestStatus = false // please check it while deleting the group i.e. true for accepting and false for not found

    var requestTimer : Timer!
    let quickTips = ["Keep you CARTING KIDZs wallet recharge to enjoy cashless ride","Ride in share, save the money","You ride more, save more","Don't worry about pick and drop of child, we are here!"]

    //firebase component
    var carGroupRef: DatabaseReference!
//    fileprivate lazy var storageRef: StorageReference = Storage.storage().reference(forURL: api.firebaseDatabase.url())
    private var newMessageRefHandle: DatabaseHandle?
    var visibleTipIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator = DGActivityIndicatorView(type: .ballScale, tintColor: appColor.red, size: self.view.frame.size.width*3/4)
        self.driverCheckingView.addSubview(activityIndicator)
        activityIndicator.center = mapImageView.center
        self.driverCheckingView.bringSubview(toFront: activityIndicator)
        activityIndicator.startAnimating()
        self.firebaseHandling()
        self.startTimer()
        self.showDriverChekingView()
        self.showTips()
    }


    func onTap(view: DriverDetailOnConfirmRideView) {
        dismiss(animated: true, completion: nil)
    }

    @objc func showTips(){
        UIView.transition(with: self.tipLabel, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            let item = self.visibleTipIndex % self.quickTips.count
            self.tipLabel.text = self.quickTips[item]
        }) { (done) in
            self.visibleTipIndex = self.visibleTipIndex+1
            self.perform(#selector(self.showTips), with: nil, afterDelay: 4.0)
        }
    }

    func addDriverInfoView(ride:Ride){
        let driverInfoView = DriverDetailOnConfirmRideView.instanceFromNib()
        let height = self.view.frame.size.height*208/1138
        driverInfoView.driverImage.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))
        driverInfoView.registrationAndOTP.text = ride.car.number+"  OTP: \(ride.OTP)"
        driverInfoView.timeLabel.text = ride.createdAt
        driverInfoView.delegate = self
        driverInfoView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: height)
        driverInfoView.alpha = 0.0
        driverInfoView.layoutSubviews()
        self.view.addSubview(driverInfoView)
        driverInfoView.layoutSubviews()
        self.view.bringSubview(toFront: driverInfoView)
        driverInfoView.layoutSubviews()

        let originY:CGFloat = 54
        let frame = CGRect(x: 0, y: originY, width: self.view.frame.size.width, height: height)
        UIView.animate(withDuration: 0.8) {
            driverInfoView.frame = frame
            driverInfoView.alpha = 1.0
            driverInfoView.setNeedsLayout()
            driverInfoView.layoutIfNeeded()
            CommonClass.makeViewCircular(driverInfoView.driverImage, borderColor: appColor.green, borderWidth: 1)
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            NotificationCenter.default.post(name: .CHECK_BOOKING_VIEW_NOTIFICATION, object: nil, userInfo: nil)
            self.delegate?.checkingRidesViewController(viewController: self, shouldGotoTrackRideWithRide: ride)
        }
    }



    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func showDriverChekingView(){
        self.noRideView.isHidden = true
        self.bookingConfirmView.isHidden = true
        self.driverCheckingView.isHidden = false
        self.view.bringSubview(toFront: self.driverCheckingView)
    }

    func showNoRideView(){
        if self.requestTimer != nil{if self.requestTimer.isValid{self.requestTimer.invalidate()}}
        activityIndicator.stopAnimating()
        self.bookingConfirmView.isHidden = true
        self.driverCheckingView.isHidden = true
        self.noRideView.isHidden = false
        self.view.bringSubview(toFront: self.noRideView)
    }

    func showBookingConfirmedRideView(){
        if self.requestTimer != nil{if self.requestTimer.isValid{self.requestTimer.invalidate()}}
        activityIndicator.stopAnimating()

        self.driverCheckingView.isHidden = true
        self.noRideView.isHidden = true
        self.bookingConfirmView.isHidden = false
        self.view.bringSubview(toFront: self.bookingConfirmView)
        self.getRideBookingDetails()
    }

    func getRideBookingDetails(){
        if !AppSettings.isConnectedToNetwork {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }

        RideService.sharedInstance.getRideDetails(rideID) { (success,resRide,message)  in
            if success{
                if let ride = resRide{
                    self.addDriverInfoView(ride: ride)
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
            }
        }
    }




    func startTimer() -> Void {
        self.requestTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(stopTimer), userInfo: nil, repeats: true)
    }

    func cancelRequestBeforeLeave(rideID:String,reason:CheckingRidesCancelReason,completionBlock: @escaping (_ finished : Bool)->Void){
        if reason == .fromCloseButton{
            AppSettings.shared.showLoader(withStatus: "Please wait..")
        }

        RideService.sharedInstance.cancelRide(rideID, reason: reason.rawValue,isForServerUse: true) { (success, resRide, message) in
            AppSettings.shared.hideLoader()
            completionBlock(true)
        }
    }

    @objc func stopTimer() -> Void {
        if self.requestTimer != nil{if self.requestTimer.isValid{self.requestTimer.invalidate()}}
        self.requestTimer = nil
        activityIndicator.stopAnimating()
        self.showNoRideView()
    }

    override func viewDidLayoutSubviews() {
        activityIndicator.center = mapImageView.center
        activityIndicator.frame = mapImageView.frame
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickCloseButton(_ sender: UIButton){
        if self.requestTimer != nil{if self.requestTimer.isValid{self.requestTimer.invalidate()}}
        activityIndicator.stopAnimating()
        self.cancelRequestBeforeLeave(rideID: self.rideID, reason: .fromCloseButton, completionBlock: {(finished) in
            NotificationCenter.default.post(name: .CHECK_BOOKING_VIEW_NOTIFICATION, object: nil, userInfo: nil)

            self.dismiss(animated: false, completion: nil)
        })

    }

    @IBAction func onClickCloseButtonOfNoRideView(_ sender: UIButton){
        if self.requestTimer != nil{if self.requestTimer.isValid{self.requestTimer.invalidate()}}
        activityIndicator.stopAnimating()
        self.cancelRequestBeforeLeave(rideID: self.rideID, reason: .fromCloseButton, completionBlock: {(finished) in
            if finished{
                NotificationCenter.default.post(name: .CHECK_BOOKING_VIEW_NOTIFICATION, object: nil, userInfo: nil)
                self.dismiss(animated: false, completion: nil)
            }
        })

    }

    @IBAction func onClickAnotherRideButtonOfNoRideView(_ sender: UIButton){
        if self.requestTimer != nil{if self.requestTimer.isValid{self.requestTimer.invalidate()}}
        activityIndicator.stopAnimating()
        NotificationCenter.default.post(name: .CHECK_BOOKING_VIEW_NOTIFICATION, object: nil, userInfo: nil)
        self.dismiss(animated: false, completion: nil)
    }


    func firebaseHandling() {
        let path = "Ride_Status_\(self.rideID)"
        weak var weakSelf = self
        let ref = Database.database().reference(withPath: path)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            if !snapshot.exists() { return }
            
            weakSelf?.newMessageRefHandle = ref.observe(.childAdded, with: { (response) in
                guard let blockWeakSelf = weakSelf else{return}
                if let status = response.value as? Int{
                    if status == 1{
                        blockWeakSelf.rideRequestStatus = true
                        blockWeakSelf.showBookingConfirmedRideView()
                        if let handle = weakSelf?.newMessageRefHandle {
                            ref.removeObserver(withHandle: handle)
                        }
                    }
                }
            })
            weakSelf?.newMessageRefHandle = ref.observe(.childChanged, with: { (response) in
                guard let blockWeakSelf = weakSelf else{return}
                if let status = response.value as? Int{
                    if status == 1{
                        blockWeakSelf.rideRequestStatus = true
                        blockWeakSelf.showBookingConfirmedRideView()
                        if let handle = weakSelf?.newMessageRefHandle {
                            ref.removeObserver(withHandle: handle)
                        }
                    }
                }
            })
            
            weakSelf?.newMessageRefHandle = ref.observe(.childRemoved, with: { (response) in
                guard let blockWeakSelf = weakSelf else{return}
                if let handle = weakSelf?.newMessageRefHandle {
                    ref.removeObserver(withHandle: handle)
                }
                blockWeakSelf.showNoRideView()
                
                /*
                 RideService.sharedInstance.getRideDetails(blockWeakSelf.rideID) { (success,resRide,message)  in
                 if success{
                 if let ride = resRide{
                 let rStatus: RideCurrentStatus = ride.getCurrentStatus()
                 if rStatus == .cancelled{
                 blockWeakSelf.showNoRideView()
                 }else{
                 blockWeakSelf.showBookingConfirmedRideView()
                 }
                 }else{
                 NKToastHelper.sharedInstance.showErrorAlert(nil, message: message,completionBlock: {
                 blockWeakSelf.showNoRideView()
                 blockWeakSelf.dismiss(animated: false, completion: nil)
                 })
                 }
                 }else{
                 NKToastHelper.sharedInstance.showErrorAlert(nil, message: message,completionBlock: {
                 blockWeakSelf.showNoRideView()
                 blockWeakSelf.dismiss(animated: false, completion: nil)
                 })
                 }
                 }*/
            })
        })
    }
}









