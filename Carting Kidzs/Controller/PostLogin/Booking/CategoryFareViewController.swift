//
//  CategoryFairViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 11/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class CategoryFareViewController: UIViewController {
    var category: CarCategory!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var carsNameLabel: UILabel!
    @IBOutlet weak var baseFareLabel: UILabel!
    @IBOutlet weak var ratePerKMLabel: UILabel!
    @IBOutlet weak var rideTimeRateLabel: UILabel!
    @IBOutlet weak var remarkLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoryNameLabel.text = category.categoryName.uppercased()
        self.carsNameLabel.text = category.carsName.capitalized
        self.baseFareLabel.text = String(format: "$ %0.2f",category.baseFair)
        self.ratePerKMLabel.text = String(format: "$ %0.2f/mile",category.pricePerMile)
        self.rideTimeRateLabel.text = String(format: "$ %0.2f/min",category.rideFairPerMinute)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else{return}
        guard let touchedView = touch.view else{return}
        if touchedView == self.view{
            dismiss(animated: true, completion: nil)
        }
    }


    @IBAction func onClickGotIt(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}
