//
//  LeftMenuViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 25/06/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

let kDarkLeftMenuCellColor = UIColor(red:46.0/255.0, green:53.0/255.0, blue:67.0/255, alpha:1.0)
let kLightLeftMenuCellColor = UIColor(red:50.0/255.0, green:58.0/255.0, blue:71.0/255, alpha:1.0)


class LeftMenuViewController: UIViewController, UITableViewDataSource,UITableViewDelegate  {
    @IBOutlet weak var tableView: UITableView!
    var slideOutAnimationEnabled : Bool!
    let leftMenuOptions = ["Profile","Book Your Ride","Ride History","My Child","My Payment Method","Settings","Support","Logout"]
    let leftMenuIcons = ["profile","book_ride","ride_history","add_child","add_payment","setting","support","logout"]
    var user : User!

    override func viewDidLoad() {
        slideOutAnimationEnabled = false
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(userDidLoggedIn(_:)), name: .USER_DID_UPDATE_PROFILE_NOTIFICATION, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(slideNavigationDidClose(_:)), name: .CartingKidzsSlideNavigationControllerDidClose, object: nil)
        self.user = User.loadSavedUser()
        SlideNavigationController.sharedInstance().enableShadow = false
        SlideNavigationController.sharedInstance().enableSwipeGesture = true
        SlideNavigationController.sharedInstance().portraitSlideOffset = UIScreen.main.bounds.size.width * 1 / 4
        self.tableView.separatorColor = UIColor.white
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.delegate = self
    }

    deinit{
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if tableView != nil{
            self.tableView.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leftMenuOptions.count;
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screenHeight = self.view.frame.height
        return (indexPath.row == 0) ? (screenHeight*200.0/1138.0) : (screenHeight*120.0/1138.0)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0{
            self.user = User.loadSavedUser()
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuProfileCell", for: indexPath) as! MenuProfileCell
            cell.profileIcon.sd_setImage(with: URL(string:self.user.profileImage), placeholderImage: AppSettings.shared.userAvatarImage(username:"\(self.user.fullName)"))
            cell.nameLabel.text = self.user.fullName
            CommonClass.makeViewCircular(cell.profileIcon, borderColor: appColor.green, borderWidth: 0)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell", for: indexPath) as! LeftMenuCell
            cell.cellIcon.image = UIImage(named:leftMenuIcons[indexPath.row])
            cell.nameLabel.text = leftMenuOptions[indexPath.row]
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        let colorView = UIView()
        colorView.backgroundColor = UIColor.white.withAlphaComponent(0.1)
        cell.selectedBackgroundView = colorView
        var vc : UIViewController!

        switch indexPath.row {
        case 0:
            let profileVC = AppStoryboard.Profile.viewController(ProfileViewController.self)
            vc = profileVC
            cell.selectedBackgroundView = nil
            
        case 1:
            let homeVC = AppStoryboard.Booking.viewController(HomeViewController.self)
            vc = homeVC
            cell.selectedBackgroundView = nil
            NotificationCenter.default.post(name: .CHECK_BOOKING_VIEW_NOTIFICATION, object: nil, userInfo: nil)

        case 2:
            let rideHistoryVC = AppStoryboard.History.viewController(MyRidesViewController.self)
            vc = rideHistoryVC
            cell.selectedBackgroundView = nil
        case 3:
            let myRideVC = AppStoryboard.Booking.viewController(MyChildsViewController.self)
            vc = myRideVC
            cell.selectedBackgroundView = nil
        case 4:
            let paymentMethodVC = AppStoryboard.MyCards.viewController(PaymentsMethodsViewController.self)
            vc = paymentMethodVC
            cell.selectedBackgroundView = nil
        case 5:
            let settingVC = AppStoryboard.Settings.viewController(SettingsViewController.self)
            vc = settingVC
            cell.selectedBackgroundView = nil
        case 6:
            let supportVC = AppStoryboard.Support.viewController(SupportsViewController.self)
            supportVC.isFromMenu = true
            vc = supportVC
            cell.selectedBackgroundView = nil
        case 7:
            if AppSettings.shared.isLoggedIn{
                askForLogout()
            }
            cell.selectedBackgroundView = nil
            return
        default:
            cell.selectedBackgroundView = nil
            return
        }
        cell.selectedBackgroundView = nil
        SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: {[cell,tableView] in
            cell.selectedBackgroundView = nil
            tableView.reloadData()
        })
    }


    func shareApplication(){
        tableView.reloadData()
        SlideNavigationController.sharedInstance().closeMenu {
            let webURL = "https://www.gpdock.com/home"
            let textToShare = "Check out GPDock! A new way to reserve your next sailing adventure!\n\(webURL)"
            var objectsToShare = [Any]()
            objectsToShare.append(textToShare)
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
            activityVC.popoverPresentationController?.sourceView = appDelegate.window?.rootViewController?.view
            appDelegate.window?.rootViewController?.present(activityVC, animated: true, completion: nil)
        }
    }

    func rateTheAppApplication(){
        tableView.reloadData()
        SlideNavigationController.sharedInstance().closeMenu {
            self.rateApp(appId: "1241899105", completion: { (success) in
                if !success{
                }
            })
        }
    }

    func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8&id=" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }


    func askForLogout() {

        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "Do you really want to logout?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Yes", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
            self.logout()
        }
        let cancelAction = UIAlertAction(title: "Nope", style: .cancel){(action) in
            SlideNavigationController.sharedInstance().closeMenu {
                self.tableView.reloadData()
            }
            alert.dismiss(animated: true, completion: nil)
        }

        alert.addAction(okayAction)
        alert.addAction(cancelAction)
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }

    @objc func userDidLoggedIn(_ notification:Notification) {
        if let userInfo = notification.userInfo{
            if let lUser = userInfo["user"] as? User{
                self.user = lUser
                self.tableView.reloadData()
            }
        }
    }

    @objc func slideNavigationDidClose(_ notification:Notification) {
        if self.tableView != nil{
            self.tableView.reloadData()
        }
    }

    func logout() {
        SlideNavigationController.sharedInstance().closeMenu {
            if !AppSettings.isConnectedToNetwork{
                NKToastHelper.sharedInstance.showErrorAlert(nil, message: warningMessage.networkIsNotConnected.rawValue)
                self.tableView.reloadData()
                return
            }

            User.logOut({ (success, resUser, message) in
                if success{
                    AppSettings.shared.isLoggedIn = false
                    AppSettings.shared.proceedToLandingPage()
                    NKToastHelper.sharedInstance.showErrorAlert(nil, message: "You have logged out successfully")
                    AppSettings.shared.proceedToLoginModule()
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
                }
            })

        }
    }

    func logoutFromSocialMedia() {
        let logoutDict = ["user":Dictionary<String,AnyObject>()]
        User().saveUserJSON(JSON(logoutDict))
    }
}






