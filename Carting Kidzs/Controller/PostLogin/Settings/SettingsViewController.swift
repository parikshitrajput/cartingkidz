//
//  SettingsViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 31/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    let settings = [["Change Password","Emergency Contacts","My Parent"],
                    ["Notifications"],
                    ["Privacy Policy","Term of Uses","Version"]
    ]

    let icons = [["change_password","emergency_contact","add_child"],
                    ["notification_setting"],
                    ["privacy_policies","terms_condition","version"]
    ]

    @IBOutlet weak var tableView:UITableView!



    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickMenu(_ sender:UIBarButtonItem){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }

    func registerCells(){
        self.tableView.register(UINib(nibName: "NotificationSettingCell", bundle: nil), forCellReuseIdentifier:"NotificationSettingCell")
        self.tableView.register(UINib(nibName: "GeneralSettingCell", bundle: nil), forCellReuseIdentifier:"GeneralSettingCell")
    }
}

extension SettingsViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.settings.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section == 0) ? 0 : 3
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.settings[section].count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationSettingCell", for: indexPath) as! NotificationSettingCell
            cell.settingIcon.image = UIImage(named:icons[indexPath.section][indexPath.row])
            cell.settingNameLabel.text = settings[indexPath.section][indexPath.row]
            cell.settingSwitch.isOn = AppSettings.shared.isNotificationEnable
            cell.settingSwitch.addTarget(self, action: #selector(notificatonToggle(_:)), for: .valueChanged)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralSettingCell", for: indexPath) as! GeneralSettingCell
            if (indexPath.section == settings.count-1) && (indexPath.row == settings[settings.count-1].count-1){
                cell.settingIcon.image = UIImage(named:icons[indexPath.section][indexPath.row])
                cell.settingNameLabel.text = "Version "+UIApplication.appVersion()
                cell.accIcon.isHidden = true
            }else{
                cell.settingIcon.image = UIImage(named:icons[indexPath.section][indexPath.row])
                cell.settingNameLabel.text = settings[indexPath.section][indexPath.row]
                cell.accIcon.isHidden = false
            }
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.section,indexPath.row) {
        case (0,0):
            self.openResetPassword()
        case (0,1):
            self.openEmergencyContact()
        case (0,2):
            self.openParentsList()
        case (2,0):
            self.openPrivacyPolicy()
        case (2,1):
            self.openTermsOfUses()
        default:
            return
        }
    }
    func openResetPassword(){
        let resetPasswordVC = AppStoryboard.Settings.viewController(SetupPasswordViewController.self)
        self.navigationController?.pushViewController(resetPasswordVC, animated: true)
    }

    func openEmergencyContact(){
        let emergencyContactVC = AppStoryboard.Settings.viewController(EmergencyContactViewController.self)
        self.navigationController?.pushViewController(emergencyContactVC, animated: true)
    }

    func openParentsList(){
        let emergencyContactVC = AppStoryboard.Booking.viewController(ParentListViewController.self)
        self.navigationController?.pushViewController(emergencyContactVC, animated: true)
    }

    func openPrivacyPolicy(){
        let privacyPolicyVC = AppStoryboard.Settings.viewController(PrivacyPolicyViewController.self)
        privacyPolicyVC.isPrivacyPolicy = true
        self.navigationController?.pushViewController(privacyPolicyVC, animated: true)
    }

    func openTermsOfUses(){
        let termsOfUses = AppStoryboard.Settings.viewController(PrivacyPolicyViewController.self)
        termsOfUses.isPrivacyPolicy = false
        self.navigationController?.pushViewController(termsOfUses, animated: true)
    }

    @IBAction func notificatonToggle(_ sender: UISwitch){
        AppSettings.shared.isNotificationEnable = sender.isOn
    }

}





