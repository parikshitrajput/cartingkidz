//
//  EmergencyContactViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 31/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class EmergencyContactViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var contacts = Array<Contact>()
    var user: User!
    var isLoading:Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        tableView.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier: "NoDataCell")
        tableView.register(UINib(nibName: "EmergencyContactCell", bundle: nil), forCellReuseIdentifier: "EmergencyContactCell")
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.getContact()
    }

    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }


    @IBAction func onClickAddContactButton(_ sender: UIButton){
        if self.contacts.count == 5{
            NKToastHelper.sharedInstance.showErrorAlert(nil, message: "Sorry! You can add only 5 contacts that you have done")

            return
        }
        self.openContactPicker()
    }

    func addBGViewFor(_ shouldAdd:Bool){
        if shouldAdd{
            let frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height-55)
            let bgView = EmergencyContactBGView.instanceFromNib()
            bgView.frame = frame
            self.tableView.backgroundView = bgView
        }else{
            self.tableView.backgroundView = nil
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickRemoveButton(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.tableView) as IndexPath?{
            let contact = self.contacts[indexPath.row]
            self.askToDeleteContact(contact: contact, atIndexPath: indexPath)
        }
    }

    func askToDeleteContact(contact : Contact, atIndexPath indexPath: IndexPath) {
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "Would you really want to delete this contact from the list?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Delete", style: .destructive){[weak contact](action) in
            alert.dismiss(animated: true, completion: nil)
            self.removeContact(contact, indexPath: indexPath)
        }

        let cancelAction = UIAlertAction(title: "Nope", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(okayAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }

    func removeContact(_ myContact:Contact?,indexPath: IndexPath) -> Void {
        guard let contact = myContact else {
            return
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        EmergencyContactsService.sharedInstance.removeContact(contactID: contact.id){ (success,removed,message) in
            AppSettings.shared.hideLoader()
            if success{
                self.contacts.remove(at: indexPath.row)
                if self.contacts.count == 0{
                    self.tableView.reloadData()
                }else{
                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                }
                self.addBGViewFor(self.contacts.count == 0)
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
            }
        }
    }

    func getContact(){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue){self.dismiss(animated: true, completion: nil)}
            return
        }
        self.isLoading = true
        self.tableView.reloadData()
        AppSettings.shared.showLoader(withStatus: "Loading..")
        EmergencyContactsService.sharedInstance.userContactList(userID: self.user.ID) { (success, resContacts, message) in
            AppSettings.shared.hideLoader()
            self.isLoading = false
            if success{
                if let someContacts = resContacts{
                    self.contacts.append(contentsOf: someContacts)
                    self.tableView.reloadData()
                    self.addBGViewFor(self.contacts.count == 0)
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

}


extension EmergencyContactViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contacts.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmergencyContactCell", for: indexPath) as! EmergencyContactCell
        let contact = self.contacts[indexPath.row]
        let name = contact.firstName + " "+contact.lastName
        cell.contactImage.sd_setImage(with:URL(string: contact.image), placeholderImage: AppSettings.shared.userAvatarImage(username:"\(name.uppercased())"))
        CommonClass.makeViewCircular(cell.contactImage)
        cell.nameLabel.text = name.capitalized
        cell.phoneNumberLabel.text = contact.contact
        cell.removeButton.addTarget(self, action: #selector(onClickRemoveButton(_:)), for: .touchUpInside)
        return cell

    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? EmergencyContactCell{
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            CommonClass.makeViewCircular(cell.contactImage, borderColor: appColor.red, borderWidth: 1)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.contacts.count == 0{return}
        let contact = contacts[indexPath.row]
        self.dialPhone(phoneNumber: contact.contact.replacingOccurrences(of: " ", with: ""))
    }

    func dialPhone(phoneNumber:String){
        let dial = "telprompt://\(phoneNumber)"
        self.showPhoneApp(dial: dial)
    }

    func showPhoneApp(dial: String)  {
        guard let url = URL(string:dial) else{
            return
        }
        if (UIApplication.shared.canOpenURL(url)){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }else{
            NKToastHelper.sharedInstance.showErrorAlert(nil, message: "Cann't able to call right now\r\nPlease try later!")
        }
    }

    func openContactPicker(){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        self.present(contactPicker, animated: true, completion: nil)
    }

    func openAddContactVC(name: String, phoneNumber:String){
        let addContactVC = AppStoryboard.Settings.viewController(AddContactViewController.self)
        addContactVC.delegate = self
        addContactVC.name = name
        addContactVC.contact = phoneNumber
        addContactVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let nav = AppSettings.shared.getNavigation(vc: addContactVC)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        self.present(nav, animated: true, completion: nil)
    }

}



extension EmergencyContactViewController: CNContactPickerDelegate,AddContactViewControllerDelegate{
    func addContactViewController(viewController: AddContactViewController, didAddContact contact: Contact) {
        self.contacts.append(contact)
        self.tableView.reloadData()
        self.addBGViewFor(self.contacts.count == 0)
        viewController.dismiss(animated: true, completion: nil)
    }

    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        picker.dismiss(animated: false, completion: nil)
        var theContactName = contactProperty.contact.givenName
        if theContactName.isEmpty{
            theContactName = contactProperty.contact.familyName
        }
        if let thePhoneNumber = contactProperty.value as? CNPhoneNumber{
            let pickedPhone = thePhoneNumber.stringValue.trimmingCharacters(in: .whitespaces)
            self.openAddContactVC(name: theContactName, phoneNumber: pickedPhone)
        }
    }
}
