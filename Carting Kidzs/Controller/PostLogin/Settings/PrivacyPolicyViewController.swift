//
//  PrivacyPolicyViewController.swift
//  TipNTap
//
//  Created by TecOrb on 22/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!
    var isPrivacyPolicy : Bool = true
    var isFromSignUp : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpLeftBarButton()
        self.webView.delegate = self
        self.webView.backgroundColor = appColor.brown
        self.navigationItem.title = isPrivacyPolicy ? "Privacy Policy".uppercased() :"Terms of Uses".uppercased()
        let urlPath = isPrivacyPolicy ? "http://cartingkidzs.iwebmobileapp.com/privacy/policies" : "http://cartingkidzs.iwebmobileapp.com/user/terms"
        if let targetUrl = URL(string: urlPath){
            self.webView.delegate = self
            self.webView.scalesPageToFit = false
            let request = URLRequest(url: targetUrl)
            self.webView.loadRequest(request)
        }else{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.functionalityPending.rawValue)
        }
    }

    func setUpLeftBarButton() {
        let leftbutton = UIBarButtonItem(image: isFromSignUp ? #imageLiteral(resourceName: "cancel") : #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(onClickBackButton(_:)))
        leftbutton.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = leftbutton
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
        if self.isFromSignUp{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.pop(true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        if AppSettings.shared.isLoaderOnScreen{
            AppSettings.shared.hideLoader()
        }
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {

        if AppSettings.shared.isLoaderOnScreen{
            AppSettings.shared.hideLoader()
        }
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        if !AppSettings.shared.isLoaderOnScreen{
            AppSettings.shared.showLoader(withStatus: "Loading")
        }
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            return false
        }
        return true
    }

}
