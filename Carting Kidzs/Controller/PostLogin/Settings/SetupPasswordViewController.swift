//
//  SetupPasswordViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 31/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class SetupPasswordViewController: UIViewController {
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordSeparator: UIView!
    @IBOutlet weak var doneButton: UIButton!
    var user : User!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
    }

    override func viewDidLayoutSubviews() {
        self.doneButton.applyGradient(withColours: [appColor.red,appColor.red], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func onClickDoneButton(_ sender: UIButton){
        //hit service to reset password using token
        guard let oldPassword = oldPasswordTextField.text else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.enterOldPassword.rawValue)
            return
        }

        if oldPassword.count == 0{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.enterOldPassword.rawValue)
            return
        }
        if oldPassword.count < 6{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.validPassword.rawValue)
            return
        }

        guard let newPassword = newPasswordTextField.text else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.enterNewPassword.rawValue)
            return
        }
        if newPassword.count == 0{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.enterNewPassword.rawValue)
            return
        }
        if newPassword.count < 6{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.validPassword.rawValue)
            return
        }

        guard let confirmPassword = confirmedPasswordTextField.text else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.confirmPassword.rawValue)
            return
        }

        if confirmPassword.count == 0{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.confirmPassword.rawValue)
            return
        }

        if confirmPassword != newPassword{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.passwordDidNotMatch.rawValue)
            return
        }

        self.updatePassword(oldPassword: oldPassword, newPassword: newPassword)
    }


    func updatePassword(oldPassword:String,newPassword:String) {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        LoginService.sharedInstance.setupPassword(self.user.contact, oldPassword: oldPassword, withNewPassword: newPassword) { (success, message) in
            if success{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message, completionBlock: {
                    self.navigationController?.pop(true)
                })
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }


    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }


    @IBAction func textFieldDidChangeText(_ sender: UITextField){
        if sender == self.newPasswordTextField || sender == self.confirmedPasswordTextField{
            if let cnfrmPassword = self.confirmedPasswordTextField.text{
                if let newpswrd = self.newPasswordTextField.text{
                    // self.doneButton.isEnabled = (newpswrd == cnfrmPassword)
                    self.confirmedPasswordSeparator.backgroundColor = (newpswrd == cnfrmPassword) ? UIColor.groupTableViewBackground : appColor.red
                }
            }
        }
    }

}
