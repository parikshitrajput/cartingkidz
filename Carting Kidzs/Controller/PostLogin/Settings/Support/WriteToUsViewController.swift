//
//  WriteToUsViewController.swift
//  GPDock
//
//  Created by TecOrb on 10/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import RSKPlaceholderTextView

class WriteToUsViewController: UITableViewController {
    var user : User!
    var ride:Ride?
    @IBOutlet var messageTextView : RSKPlaceholderTextView!
    @IBOutlet weak var scnShotbutton : UIButton!
    @IBOutlet weak var userEmailLabel : UILabel!
    @IBOutlet weak var phoneNumberLabel : UILabel!
    @IBOutlet weak var removeScreenShotbutton : UIButton!
    @IBOutlet weak var submitButton : UIButton!

    var screenShot : UIImage?
    var imagePickerController : UIImagePickerController!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundView?.backgroundColor = appColor.brown
        self.tableView.backgroundColor = appColor.brown

        self.title = "Write to Us".uppercased()
        self.user = User.loadSavedUser()
        self.clearsSelectionOnViewWillAppear = true
        self.userEmailLabel.text = self.user.fullName
        self.phoneNumberLabel.text = "+"+self.user.contact
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        if let myRide = self.ride{
            self.messageTextView.text = "Hi, I am facing issue with ride Id: \(myRide.ID)"
        }
    }

    override func viewWillLayoutSubviews() {
        CommonClass.makeViewCircular(self.scnShotbutton, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircular(self.removeScreenShotbutton, borderColor: UIColor.clear, borderWidth: 0)
    }

    func refreshScreenShotButton() {
        if let img = self.screenShot{
            self.scnShotbutton.setImage(img, for: UIControlState())
            self.removeScreenShotbutton.isHidden = false
        }else{
            self.scnShotbutton.setImage(#imageLiteral(resourceName: "screenshot"), for: UIControlState())
            self.removeScreenShotbutton.isHidden = true
        }
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSubmit(_ sender: UIButton){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        if self.messageTextView.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter the issue you are facing")
            return
        }

        if self.messageTextView.text.isEmpty{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter the issue you are facing")
            return
        }

        if self.user.ID.isEmpty{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Your session has expired. Please login again")
            return
        }
        self.sendEmailToSupport(self.user.ID, message: self.messageTextView.text, screenShot: self.screenShot)
    }

    @IBAction func onClickRemoveScreenShot(_ sender: UIButton){
        self.screenShot = nil
        self.refreshScreenShotButton()
    }
    @IBAction func onClickAddScreenShot(_ sender: UIButton){
        self.showAlertToChooseAttachmentOption()
    }

    func sendEmailToSupport(_ userID:String, message: String,screenShot: UIImage?){
        AppSettings.shared.showLoader(withStatus: "Sending..")
        RideService.sharedInstance.sendEmailToSupport(userID, message: message, image: screenShot) { (success, resSupport,message) in
            AppSettings.shared.hideLoader()
            if success{
                if resSupport != nil{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: "Your message has been received and responded to as soon as possible",completionBlock: {
                        self.navigationController?.pop(true)
                    })
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1{
            if UIDevice.isIphoneX{
                return (self.view.frame.size.height-202)/3
            }else{
                return (self.view.frame.size.height-182)/3
            }
        }else{
            return 44
        }
    }
}

extension WriteToUsViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showAlertToChooseAttachmentOption(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)

        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.refreshScreenShotButton()
        picker.dismiss(animated: true, completion: nil)
    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String :Any]){

        if let tempImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.screenShot = tempImage
            self.refreshScreenShotButton()
        }else if let tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.screenShot = tempImage
            self.refreshScreenShotButton()
        }
        picker.dismiss(animated: true, completion: nil)
    }


}
