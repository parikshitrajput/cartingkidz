//
//  AppSettings.swift
//  TipNTap
//
//  Created by TecOrb on 29/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//
import CoreFoundation
import UIKit
import SwiftyJSON
import SVProgressHUD
import SystemConfiguration
import Alamofire
import CoreTelephony

class AppSettings {

    static let shared = AppSettings()
    fileprivate init() {}


    func getNavigation(vc:UIViewController) -> UINavigationController{
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isOpaque = false
        nav.navigationBar.isTranslucent = true
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: fonts.OpenSans.semiBold.font(.xXLarge),NSAttributedStringKey.foregroundColor:appColor.red]
        nav.clearShadowLine()
        nav.navigationBar.backgroundColor = .white
        return nav
    }
    func prepareHeader(withAuth:Bool) -> Dictionary<String,String>{
        let accept = "application/json"
        let currentVersion = UIApplication.appVersion()//+"."+UIApplication.appBuild()
        var header = Dictionary<String,String>()
        if withAuth{
            let user = User.loadSavedUser()
            let userToken = user.apiKey
            header.updateValue(userToken, forKey: "accessToken")
        }

        header.updateValue(currentVersion, forKey: "currentVersion")
        header.updateValue("ios", forKey: "currentDevice")
        header.updateValue(accept, forKey: "Accept")
        header.updateValue(TimeZone.current.identifier, forKey: "timezone")

        return header
    }

    class var isConnectedToNetwork: Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)

        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {

            return false
        }
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }


//    var shouldAskToSetTouchID:Bool{
//        return BioMetricAuthenticator.canAuthenticate() && self.interestedInLoginWithTouchID && (!self.isLoginWithTouchIDEnable)
//    }
//
//    var shouldAskToLoginWithTouchID:Bool{
//        return BioMetricAuthenticator.canAuthenticate() && self.isLoginWithTouchIDEnable
//    }
    
    var isPaymentViewController: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsPaymentViewController) as Bool?{
                result = r
            }
            return result
        }
        
        set(newIsPaymentViewController){
            kUserDefaults.set(newIsPaymentViewController, forKey: kIsPaymentViewController)
        }
    }
    

    var isLoggedIn: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsLoggedIN) as Bool?{
                result = r
            }
            return result
        }

        set(newIsLoggedIn){
            kUserDefaults.set(newIsLoggedIn, forKey: kIsLoggedIN)
        }
    }

    var isRegistered: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsRegistered) as Bool?{
                result = r
            }
            return result
        }
        set(newIsRegistered){
            kUserDefaults.set(newIsRegistered, forKey: kIsRegistered)
        }
    }

    var loginCount: Int{
        get {
            var result = 0
            if let r = kUserDefaults.integer(forKey:kLoginCount) as Int?{
                result = r
            }
            return result
        }

        set(newLoginCount){
            kUserDefaults.set(newLoginCount, forKey: kLoginCount)
        }
    }

    var isFirstTimeLogin: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsFirstTimeLogin) as Bool?{
                result = r
            }
            return result
        }
        set(newIsFirstTimeLogin){
            kUserDefaults.set(newIsFirstTimeLogin, forKey: kIsFirstTimeLogin)
        }
    }

    var interestedInLoginWithTouchID: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kInterestedInLoginWithTouchID) as Bool?{
                result = r
            }
            return result
        }
        set(newInterestedInLoginWithTouchID){
            kUserDefaults.set(newInterestedInLoginWithTouchID, forKey: kInterestedInLoginWithTouchID)
        }
    }

    var isLoginWithTouchIDEnable: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsLoginWithTouchIDEnable) as Bool?{
                result = r
            }
            return result
        }
        set(newLoginWithTouchIDEnable){
            kUserDefaults.set(newLoginWithTouchIDEnable, forKey: kIsLoginWithTouchIDEnable)
        }
    }
    var isNotificationEnable: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsNotificationsEnable) as Bool?{
                result = r
            }
            return result
        }
        set(newIsNotificationEnable){
            kUserDefaults.set(newIsNotificationEnable, forKey: kIsNotificationsEnable)
        }
    }

    var isIntroShown: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsIntroShown) as Bool?{
                result = r
            }
            return result
        }
        set(newIsIntroShown){
            kUserDefaults.set(newIsIntroShown, forKey: kIsIntroShown)
        }
    }

    var currentCountryCode:String{
        let networkInfo = CTTelephonyNetworkInfo()
        if let carrier = networkInfo.subscriberCellularProvider{
            let countryCode = carrier.isoCountryCode
            return countryCode ?? "US"
        }else{
            return Locale.autoupdatingCurrent.regionCode ?? "US"
        }
    }

    var previousRatedVersion : String {
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kPreviousRatedVersion) as? String{
                result = r
            }
            return result
        }
        set(newpreviousRatedVersion){
            kUserDefaults.set(newpreviousRatedVersion, forKey: kPreviousRatedVersion)
        }
    }

    var isFirstLaunchAfterReset : Bool {
        get {
            var result = true
            if let r = kUserDefaults.value(forKey:kFirstLaunchAfterReset) as? String{
                if r.isEmpty || r.count == 0{
                    result = true
                }else{
                    result = false
                }
            }else{
                result = true
            }
            return result
        }
    }




    var shouldShowRatingPopUp : Bool {
        get {
            var result = true

            if let r = kUserDefaults.bool(forKey:kShouldShowRatingPopUp) as Bool?{
                result = r
            }
            return result
        }
        set(newShouldShowRatingPopUp){
            kUserDefaults.set(newShouldShowRatingPopUp, forKey: kShouldShowRatingPopUp)
        }
    }

//cedential

    var passwordEncrypted : String {
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kPasswordEncrypted) as? String{
                result = r
            }
            return result
        }
        set(newPasswordEncrypted){

            kUserDefaults.set(newPasswordEncrypted, forKey: kPasswordEncrypted)
        }
    }

    var phoneNumber : String {
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kPhoneNumber) as? String{
                result = r
            }
            return result
        }
        set(newPhoneNumber){
            kUserDefaults.set(newPhoneNumber, forKey: kPhoneNumber)
        }
    }


    var countryCode : String {
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kCountryCode) as? String{
                result = r
            }
            return result
        }
        set(newCountryCode){
            kUserDefaults.set(newCountryCode, forKey: kCountryCode)
        }
    }




    func resetOnFirstAppLaunch(){
        self.isLoginWithTouchIDEnable = false
        self.interestedInLoginWithTouchID = true
        self.isLoggedIn = false
        self.isRegistered = false
        self.isNotificationEnable = true
       // self.isIntroShown = false
        kUserDefaults.set("\(Date.timeIntervalSinceReferenceDate)", forKey:kFirstLaunchAfterReset)
    }

    func resetOnLogout() {
        self.isLoggedIn = false
        self.isFirstTimeLogin = false
        self.isIntroShown = true
        //self.isLoginWithTouchIDEnable = false
        User().saveUserJSON(JSON.init(Data()))
    }

    func proceedToHome(completionBlock :(() -> Void)? = nil){
        let leftMenu = AppStoryboard.Main.viewController(LeftMenuViewController.self)
        var navigationController : SlideNavigationController!
        if let nav = AppDelegate.getAppDelegate().getWindowNavigation() as? SlideNavigationController{
            navigationController = nav
        }else {
            let newNav = AppStoryboard.Booking.viewController(SlideNavigationController.self)
            navigationController = newNav
        }
        navigationController.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: fonts.OpenSans.semiBold.font(.xLarge),NSAttributedStringKey.foregroundColor:appColor.red]
        navigationController.clearShadowLine()
        navigationController.leftMenu = leftMenu
        navigationController.enableSwipeGesture=true
        AppDelegate.getAppDelegate().window?.rootViewController = navigationController
        guard let handler = completionBlock else{return}
        handler()
    }

    func proceedToLoginModule(completionBlock :(() -> Void)? = nil){
        self.isLoggedIn = false
        let navigationController = AppStoryboard.Main.viewController(MainNavigationController.self)
        AppDelegate.getAppDelegate().window?.rootViewController = navigationController
        guard let handler = completionBlock else{return}
        handler()
    }
    
    func getFullError(errorString:String,andresponse data:Data?) -> String{
        var message:String = errorString
        if let somedata = data, let serverStr = String(data: somedata, encoding: String.Encoding.utf8){
            message = message+"\n"+serverStr
        }
        return message
    }


    func showSessionExpireAndProceedToLandingPage(){
        self.hideLoader()
        NKToastHelper.sharedInstance.showErrorAlert(nil, message: warningMessage.sessionExpired.rawValue, completionBlock: {
            self.proceedToLoginModule()
        })
    }

    func showForceUpdateAlert(){
        self.hideLoader()
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: warningMessage.updateVersion.rawValue, preferredStyle: .alert)
        let updateAction = UIAlertAction(title: "Update Now", style: .cancel) {[alert] (action) in
            if let url = URL(string: "itms-apps://itunes.apple.com/app/id\(appID)"),
                UIApplication.shared.canOpenURL(url)
            {
                UIApplication.shared.open(url, options: [:], completionHandler: {[alert] (done) in
                    alert.dismiss(animated: false, completion: nil)
                })
            }
        }
        alert.addAction(updateAction)
        let toastShowingVC = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController
        toastShowingVC?.present(alert, animated: true, completion: nil)
    }


    func proceedToLandingPage(){
//        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
//        appDelegate.openLandingPage()
    }



    //=====Loader showing methods==========//
    func showLoader()
    {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setForegroundColor(appColor.red)
        SVProgressHUD.setMinimumSize(loaderSize)
        SVProgressHUD.show()
    }


    var isLoaderOnScreen: Bool
    {
        return SVProgressHUD.isVisible()
    }
    func showError(withStatus status: String)
    {
        SVProgressHUD.showError(withStatus: status)
    }
    func showSuccess(withStatus status: String)
    {
        SVProgressHUD.showSuccess(withStatus: status)
    }


    func showLoader(withStatus status: String)
    {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setBackgroundLayerColor(UIColor.white.withAlphaComponent(0.9))
        SVProgressHUD.setBackgroundColor(UIColor.white.withAlphaComponent(0.9))
        SVProgressHUD.setForegroundColor(appColor.red)
        SVProgressHUD.setMinimumSize(loaderSize)
        SVProgressHUD.show(withStatus: status)
        SVProgressHUD.setImageViewSize(loaderSize)
    }
    func updateLoader(withStatus status: String)
    {
        SVProgressHUD.setStatus(status)
        SVProgressHUD.setMinimumSize(loaderSize)
    }

    func hideLoader()
    {
        if SVProgressHUD.isVisible(){
            SVProgressHUD.dismiss()
        }
    }

    func clearAllPendingRequests(){
        Alamofire.SessionManager.default.session.getAllTasks { tasks in
            tasks.forEach { $0.cancel()
            }
        }
    }

    func clearLastPendingRequests(){
        Alamofire.SessionManager.default.session.getAllTasks { tasks in
            if let lastTask = tasks.last{
                lastTask.cancel()
            }
        }
    }

//====To open location setting of the application
    func openLocationSetting(){
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
            })
        }
    }


    func getNavigationDefaultHeight() -> CGFloat{
        if UIDevice.isIphoneX{
            return 105
        }else{
            return 64
        }
    }
    //==========User Avatar Image
    func userAvatarImage(username:String) -> UIImage{
        if username.count < 2 {
            return UIImage(named:"profile")!
        }else{
        
        let configuration = LetterAvatarBuilderConfiguration()

        configuration.username = (username.trimmingCharacters(in: .whitespaces).count == 0) ? "NA" : username
        configuration.lettersColor = appColor.red
        configuration.singleLetter = false
        configuration.lettersFont = fonts.OpenSans.semiBold.font(.xXLarge)
        configuration.backgroundColors = [UIColor.white,UIColor.white,UIColor.white,UIColor.white,UIColor.white,UIColor.white,UIColor.white,UIColor.white,UIColor.white,UIColor.white,UIColor.white,UIColor.white]
        return UIImage.makeLetterAvatar(withConfiguration: configuration) ?? UIImage(named:"profile")!
        }
    }

//==========Keys=========//
    let kIsLoggedIN = "is_logged_in"
    let kIsRegistered = "isRegistered"
    let kIsFirstTimeLogin = "isFirstTimeLogin"
    let kLoginCount = "loginCount"
    let kIsAuthourisedForSession = "isAuthourisedForSession"
    let kShouldShowRatingPopUp = "shouldShowRatingPopUp"
    let kPreviousRatedVersion = "previousRatedVersion"
    let kPasswordEncrypted = "passwordEncrypted"
    let kPhoneNumber = "phoneNumber"
    let kCountryCode = "countryCode"
    let kInterestedInLoginWithTouchID = "interestedInLoginWithTouchID"
    let kIsLoginWithTouchIDEnable = "isLoginWithTouchIDEnable"
    let kIsNotificationsEnable = "isNotificationsEnable"
    let kIsIntroShown = "isIntroShown"
    let kIsPaymentViewController = "isPaymentViewController"
//==========End of Keys=========//
}




