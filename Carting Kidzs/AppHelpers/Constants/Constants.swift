//
//  Constants.swift
//  TinNTap
//
//  Created by TecOrb on 16/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Firebase

//to set the environment
let isDebugEnabled = true
//to set logs
let isLogEnabled = false



struct mapZoomLevel{
    static let max:Float = 19.0
    static let min:Float = 11.0
}

struct appColor{
    static let gradientRedStart = UIColor(red: 255.0/255.0, green: 88.0/255.0, blue: 98.0/255.0, alpha: 1.0)
    static let gradientRedEnd = UIColor(red: 255.0/255.0, green: 88.0/255.0, blue: 137.0/255.0, alpha: 1.0)
    static let redShadow = UIColor(red: 255.0/255.0, green: 89.0/255.0, blue: 138.0/255.0, alpha: 1.0)
    static let bgColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
    static let green = UIColor.color(r: 133, g: 194, b: 38)
    static let white = UIColor.color(r: 255.0/255.0, g: 255.0/255.0, b: 255.0/255.0)
    static let brown = UIColor(red: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)

    static let gradientStart = UIColor(red: 44.0/255.0, green: 190.0/255.0, blue: 167.0/255.0, alpha: 1.0)
    static let gradientEnd = UIColor(red: 54.0/255.0, green: 72.0/255.0, blue: 216.0/255.0, alpha: 1.0)
    static let blue = UIColor.color(r: 51, g: 74, b: 219)
    static let gray = UIColor.color(r: 169, g: 167, b: 168)
    static let boxGray = UIColor.color(r: 148, g: 148, b: 148)
    static let splash = UIColor.color(r: 42, g: 121, b: 195)
    static let payoutRed = UIColor.color(r: 248, g: 24, b: 73)
    static let payoutBlue = UIColor.color(r: 69, g: 111, b: 245)
    static let lightGray = UIColor.groupTableViewBackground

    static let red = UIColor.color(r:221, g:18, b:123)
}





struct gradientTextColor{
    static let start = UIColor(red: 35.0/255.0, green: 97.0/255.0, blue: 248.0/255.0, alpha: 1.0)
    static let middle = UIColor(red: 42.0/255.0, green: 65.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    static let end = UIColor(red: 55.0/255.0, green: 32.0/255.0, blue: 208.0/255.0, alpha: 1.0)
}



let warningMessageShowingDuration = 1.25

extension UIColor{
    class func color(r:CGFloat,g:CGFloat,b:CGFloat,alpha:CGFloat? = nil) -> UIColor{
        if let alp = alpha{
            return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: alp)
        }else{
            return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
        }
    }
}


/*============== NOTIFICATIONS ==================*/
extension NSNotification.Name {
    public static let USER_DID_ADD_NEW_CARD_NOTIFICATION = NSNotification.Name("UserDidAddNewCardNotification")
    public static let USER_DID_UPDATE_PROFILE_NOTIFICATION = NSNotification.Name("UserDidUpdateProfileNotification")
    public static let FIRInstanceIDTokenRefreshNotification = NSNotification.Name.MessagingRegistrationTokenRefreshed
    public static let DROP_OFF_ADDRESS_DID_SET_NOTIFICATION = NSNotification.Name("DropOffAddressDidSelected")
    public static let TIME_UP_NOTIFICATION = NSNotification.Name("TimeUpNotification")
    public static let STOP_CHECKING_NEAR_BY_DRIVER_NOTIFICATION = NSNotification.Name("StopCheckingNearByDriver")
    public static let RIDE_ACCEPTED_NOTIFICATION = NSNotification.Name("RideAcceptNotification")
    public static let RIDE_CANCELLED_NOTIFICATION = NSNotification.Name("RideCancelledNotification")
    public static let RIDE_START_NOTIFICATION = NSNotification.Name("RideStartedNotification")
    public static let RIDE_END_NOTIFICATION = NSNotification.Name("RideEndedNotification")
    public static let RIDE_REJECTED_NOTIFICATION = NSNotification.Name("RideRejectedNotification")
    public static let DRIVER_REACHED_NOTIFICATION = NSNotification.Name("DriverReachedNotification")
    public static let DISMISS_PAYMENT_MODE_NOTIFICATION = NSNotification.Name("DismissPaymentModeNotification")
    public static let SELECT_PAYMENT_MODE_NOTIFICATION = NSNotification.Name("SelectPaymentMode")
    public static let BACK_TO_PAYMENT_MODE_NOTIFICATION = NSNotification.Name("BackToPaymentMode")
    public static let PAYMENT_SUCCESS_BY_USER_NOTIFICATION = NSNotification.Name("PaymentSuccessfullPayedByUser")
    public static let AFTER_PAYMENT_SUCCESS_BY_USER_NOTIFICATION = NSNotification.Name("AfterPaymentSuccessfullPayedByUser")
    public static let CartingKidzsSlideNavigationControllerDidClose = NSNotification.Name("SlideNavigationControllerDidClose")
    public static let RIDE_UPDATED_NOTIFICATION = NSNotification.Name("RideUpdatedNotification")
    public static let RIDE_CANCELLED_BY_DRIVER_NOTIFICATION = NSNotification.Name("RideCancelledByDriverNotification")
    
    public static let CHECK_BOOKING_VIEW_NOTIFICATION = NSNotification.Name("checkBookingViewNotification")

    public static let SCHEDULE_RIDE_ACCEPTED_NOTIFICATION = NSNotification.Name("scehdule_ride_accepted")

//

}


let kNotificationType = "notification_type"
let kRideAcceptedNotification = "ride_accepted"
let kRideRequestNotification = "ride_request"
let kRideCancelledNotification = "cancel_ride"
let kRideStartedNotification = "ride_started"
let kRideEndedNotification = "ride_ended"
let kDriverReachedNotification = "driver_reached"
let kDriverCancelledNotification = "driver_cancelled"
let kScheduleRideAccept = "scehdule_ride_accepted"




enum NotificationType:String{
    case bookingCreatedByCustomerNotification = "customer_create_booking"
    case bookingCreatedByBusinessNotification = "business_create_booking"
    case bookingCancelledByBusinessNotification = "booking_cancel_by_business"
    case bookingCancelledByCustomerNotification = "booking_cancel_by_customer"
    case bookingCompletedByNotification = "booking_complete_by_business"
}



let kUserDefaults = UserDefaults.standard

/*========== SOME GLOBAL VARIABLE FOR USERS ==============*/




let kDeviceToken = "DeviceToken"
let kFirstLaunchAfterReset = "firstLaunchAfterReset"

let kSupportEmail = "nakulsharma.1296@gmail.com"
let kReportUsEmail = "nakulsharma.1296@gmail.com"

let tollFreeNumber = "1-888-202-3625"
let appID = "1429864761"
//let adminCommission = 0.04

let rountingNumberDigit = 9
let accountNumberDigit = 12
let ssnDigit = 9


enum ErrorCode:Int{
    case success
    case failure
    case forceUpdate
    case normalUpdate
    case sessionExpire
    case previousDues

    init(rawValue:Int) {
        if rawValue == 102{
            self = .forceUpdate
        }else if rawValue == 101{
            self = .normalUpdate
        }else if rawValue == 345{
            self = .sessionExpire
        }else if rawValue == 303{
            self = .previousDues
        }
        else if ((rawValue >= 200) && (rawValue < 300)){
            self = .success
        }else{
            self = .failure
        }
    }
}


enum PayoutStatus:String{
    case paid
    case pending

    func getColor() -> UIColor{
        if self == .paid{
            return appColor.green
        }else{
            return appColor.payoutRed
        }
    }
    
    init(rawValue:String) {
        if rawValue.lowercased() == "paid"{
            self = .paid
        }else{
            self = .pending
        }
    }
}



/*================== API URLs ====================================*/
let inviteLinkUrl = "http://cartingkidzs.iwebmobileapp.com/"
let appLink = isDebugEnabled ? "http://cartingkidzs.iwebmobileapp.com/" : "http://cartingkidzs.iwebmobileapp.com/"
let WEBSITE_URL = isDebugEnabled ? "http://cartingkidzs.iwebmobileapp.com/" : "http://cartingkidzs.iwebmobileapp.com/"
let apiVersion = "1"
//http://cartingkidzs.iwebmobileapp.com/
let BASE_URL = isDebugEnabled ? "http://cartingkidzs.iwebmobileapp.com/" : "http://ec2-54-89-202-210.compute-1.amazonaws.com/"

enum api: String {
    case base
    case website
    case aboutUs
    case login
    case forgotPassword
    case signUp
    case socialAuth
    case searchCarByCategory
    case allCarCategory
    case rateDriver
    case userProfile
    case userRideHistory
    case editProfile
    case allCars
    case rideRequest
    case changeRideRequest
    case rideDetails
    case addRatings
    case costEstimation
    case preCostEstimation
    case emergencyContact
    case driverLocation
    case termsAndConditions
    case userCancelRide
    case logout
    case allContact
    case removeContact
    case resetPassword
    case addCard
    case getCards
    case removeCard
    case payRide
    case newSearchUrl
    case newUserCancelRide
    case cancelRideForServer
    case newRideRequestUrl
    case rideLater
    case updateDeviceToken
    case isExistingContact
    case firebaseDatabase

    case getChildren
    case addChild
    case updateChild
    case deleteChild

    case addParent
    case updateParent
    case deleteParent
    case getParent

    case FAQ
    case addSupportQuery
    case mailInvoiceForRide
//    case editUserDetails
    case cancelReasons
    case userOnGoingRide
    case defaultCard



    func url() -> String {
        switch self {
        case .base: return BASE_URL
        case .website: return WEBSITE_URL
        case .aboutUs: return "http://www.tecorb.com/"
        case .login: return "\(BASE_URL)login"
        case .forgotPassword: return "\(BASE_URL)forgot_password"
        case .signUp: return "\(BASE_URL)signup"
        case .socialAuth: return "\(BASE_URL)social"
        case .searchCarByCategory: return "\(BASE_URL)search_car"
        case .allCarCategory: return "\(BASE_URL)car_categories"
        case .rateDriver: return "\(BASE_URL)add_rating"
        case .userProfile: return  "\(BASE_URL)profile"
        case .userRideHistory: return "\(BASE_URL)customer_booking_history"
        case .editProfile: return "\(BASE_URL)edit_profile"
        case .allCars: return "\(BASE_URL)all_cars"
        case .rideRequest: return "\(BASE_URL)ride_request"
        case .changeRideRequest: return "\(BASE_URL)change_ride_request"
        case .rideDetails: return "\(BASE_URL)ride_details"
        case .addRatings: return "\(BASE_URL)add_rating"
        case .costEstimation: return "\(BASE_URL)fair_price"
        case .preCostEstimation: return "\(BASE_URL)estimated_cost"
        case .emergencyContact: return "\(BASE_URL)emergency_contact"
        case .driverLocation: return "\(BASE_URL)driver_location"
        case .termsAndConditions: return "\(BASE_URL)term_condition"
        case .userCancelRide: return "\(BASE_URL)user_cancel_ride"
        case .logout: return "\(BASE_URL)logout"
        case .allContact: return "\(BASE_URL)all_contacts"
        case .removeContact: return "\(BASE_URL)remove/contacts"
        case .resetPassword: return "\(BASE_URL)change/password"
        case .addCard: return "\(BASE_URL)add/card"
        case .getCards: return "\(BASE_URL)get/cards"
        case .removeCard: return "\(BASE_URL)remove/card"
        case .payRide: return "\(BASE_URL)pay/ride"
        case .newSearchUrl: return "\(BASE_URL)api/v\(apiVersion)/search/car"
        case .newUserCancelRide: return "\(BASE_URL)api/v\(apiVersion)/user/cancel/ride"
        case .cancelRideForServer: return "\(BASE_URL)api/v\(apiVersion)/server/cancel/ride"
        case .newRideRequestUrl: return "\(BASE_URL)api/v\(apiVersion)/ride/request"
        case .updateDeviceToken: return "\(BASE_URL)update/device/token"
        case .isExistingContact: return "\(BASE_URL)existing/contact"
        case .getChildren: return "\(BASE_URL)api/v\(apiVersion)/child/show"
        case .addChild: return "\(BASE_URL)api/v\(apiVersion)/add/child"
        case .updateChild: return "\(BASE_URL)api/v\(apiVersion)/edit/child"
        case .deleteChild: return "\(BASE_URL)api/v\(apiVersion)/delete/child/"

        case .addParent: return "\(BASE_URL)api/v\(apiVersion)/add/parent/details"
        case .updateParent: return "\(BASE_URL)api/v\(apiVersion)/edit/parent/detail"
        case .deleteParent: return "\(BASE_URL)api/v\(apiVersion)/delete/parent/"
        case .getParent: return "\(BASE_URL)api/v\(apiVersion)/parent/details"

        case .FAQ: return "\(BASE_URL)api/v\(apiVersion)/faq/list"
        case .addSupportQuery: return "\(BASE_URL)api/v\(apiVersion)/add/support"
        case .mailInvoiceForRide: return "\(BASE_URL)api/v\(apiVersion)/ride/invoice"
        case .rideLater: return "\(BASE_URL)api/v\(apiVersion)/ride/later"
//        case .editUserDetails: return "\(BASE_URL)api/v\(apiVersion)/edit/user/details"
        case .cancelReasons: return "\(BASE_URL)api/v\(apiVersion)/cancel/ride/reasons"
        case .userOnGoingRide: return "\(BASE_URL)api/v\(apiVersion)/user/booked/rides"
        case .defaultCard: return "\(BASE_URL)default/card"

            
        case .firebaseDatabase: return isDebugEnabled ? "https://cartingkidzandroid.firebaseio.com" : "https://cartingkidzandroid-7799b.firebaseio.com/"
        }
    }
}



/*================== SOCIAL LOGIN TYPE ====================================*/
enum SocialLoginType: String {
    case facebook = "Facebook"
    case google = "Google"
}

/*======================== CONSTANT MESSAGES ==================================*/
enum warningMessage : String{
    case updateVersion = "You are using a version of Carting Kidzs that\'s no longer supported. Please upgrade your app to the newest app version to use Carting Kidzs. Thanks!"
    case title = "Important Message"
    case setUpPassword = "Your password has updated successfully"
    case invalidPassword = "Please enter a valid password"
    case invalidPhoneNumber = "Please enter a valid phone number"
    case invalidFirstName = "Please enter a valid first name"
    case invalidLastName = "Please enter a valid last name"
    case invalidEmailAddress = "Please enter a valid email address"

    case emailCanNotBeEmpty = "Email address cann't be empty."
    case restYourPassword = "An email was sent to you to rest your password"
    case changePassword = "Your password has been changed successfully"
    case logoutMsg = "You've been logged out successfully"
    case networkIsNotConnected = "Network is not connected"
    case functionalityPending = "Under Development. Please ignore it"
    case enterPassword = "Please enter your password"
    case validPassword = "Please enter a valid password. Passwords should be 6-20 characters long."

    case enterOldPassword = "Please enter your current password"
    case validOldPassword = "Please enter a valid current password. Passwords should be 6-20 characters long."
    case enterNewPassword = "Please enter your new password"
    case validNewPassword = "Please enter a valid new password. Passwords should be 6-20 characters long."
    case confirmPassword = "Please confirm your password"
    case passwordDidNotMatch = "Please enter matching passwords"
    case cardDeclined = "The card was declined. Please reenter the payment details"
    case enterCVV = "Please enter the CVV"
    case enterValidCVV = "Please enter a valid CVV"
    case cardHolderName = "Please enter the card holder's name"
    case expMonth = "Please enter the exp. month"
    case expYear = "Please enter the exp. year"
    case validExpMonth = "Please enter a valid exp. month"
    case validExpYear = "Please enter a valid exp. year"
    case validCardNumber = "Please enter a valid card number"
    case cardNumber = "Please enter the card number"
    case sessionExpired = "Your session has expired, Please login again"
}


/*============== SOCIAL MEDIA URL SCHEMES ==================*/

let FACEBOOK_URL_SCHEME = "fb1432514256876534"
let SELF_URL_SCHEME = "com.tecorb.Carting-Kidzs"
let SELF_IDENTIFIER = "CartingKidzs"
let stripeKey =  isDebugEnabled ? "pk_test_TTf0XSX09LrgJPWS5dq0jKuS" : "pk_live_BqWwBmwPzCRDMHil8K3juskr"
let GOOGLE_URL_SCHEME = "com.googleusercontent.apps.785300245432-332hussr1g7t8519t7384eon17sff1p8"
let GOOGLE_API_KEY = "AIzaSyCW3xc1mDVoOEkCIUWTpdf4D9ReZfCFuMA"
let kGoogleClientId = "785300245432-332hussr1g7t8519t7384eon17sff1p8.apps.googleusercontent.com"
let firebaseDatabaseUrl = isDebugEnabled ? "https://cartingkidzandroid.firebaseio.com" : "https://cartingkidzandroid-7799b.firebaseio.com/"
/*============== PRINTING IN DEBUG MODE ==================*/
func print_debug <T>(_ object : T){
    if isLogEnabled{
        print(object)
    }
}

func print_log <T>(_ object : T){
//    NSLog("\(object)")
}



enum fontSize : CGFloat {
    case small = 12.0
    case smallMedium = 14.0
    case medium = 15.0
    case large = 17.0
    case xLarge = 18.0
    case xXLarge = 20.0
    case xXXLarge = 26.0
    case xXXXLarge = 32.0
}

enum fonts {
    enum OpenSans : String {
        case regular = "OpenSans-Regular"
        case semiBold = "OpenSans-Semibold"
        case light = "OpenSans-Light"
        func font(_ size : fontSize) -> UIFont {
            return UIFont(name: self.rawValue, size: size.rawValue) ?? UIFont.systemFont(ofSize: size.rawValue)
        }
    }
}

//uses of fonts
//let fo = fonts.OpenSans.regular.font(.xXLarge)


/*==============================================*/






