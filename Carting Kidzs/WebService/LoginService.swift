//
//  LoginService.swift
//  Bloom Trade
//
//  Created by TecOrb on 05/09/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Firebase
import FirebaseMessaging

let kUserJSON = "UserJSON"

class LoginService {
    static let sharedInstance = LoginService()
    fileprivate init() {}

    func updateDeviceTokeOnServer(){

        /*
         "device_type": "ios",
         "device_id": "xxdfsdafzxsdfsd2342erwerqw423423wrqwer3423442"
         */
        var params = Dictionary<String,String>()
        let user = User.loadSavedUser()
        params.updateValue(user.ID, forKey: "user_id")
        if user.ID.trimmingCharacters(in: .whitespaces).count == 0{return}
        params.updateValue("ios", forKey: "device_type")
        if let deviceID = kUserDefaults.value(forKey: kDeviceToken) as? String {
            params.updateValue(deviceID, forKey: "device_id")
        }else{
            params.updateValue("--", forKey: "device_id")
        }
        var shouldUpdate :Bool = true
        for (_, value) in params{
            if value == "" || value == "--"{
                shouldUpdate = false
                break
            }
        }
        if !shouldUpdate{
            return
        }

        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.updateDeviceToken.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.updateDeviceToken.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("update device token json is:\n\(json)")
                    if let responseCode = json["code"].int{
                        if responseCode == 345{
                            AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                        }else if responseCode == 102{
                            AppSettings.shared.showForceUpdateAlert()
                        }
                    }
                }
            case .failure(let error):
                print_debug(error.localizedDescription)
                // self.retryUpdateDeviceTokeOnServer()
            }
        }

    }


    private func retryUpdateDeviceTokeOnServer() {
        self.updateDeviceTokeOnServer()
    }


    func loginWith(_ contact:String,password:String,completionBlock:@escaping (_ success:Bool, _ user: User?, _ message: String) -> Void){

        var params = ["contact":contact.removingWhitespaces(),"password":password]
        params.updateValue("ios", forKey: "device_type")
        params.updateValue("user", forKey: "role")
        if let deviceID = kUserDefaults.value(forKey: kDeviceToken) as? String {
            params.updateValue(deviceID, forKey: "device_id")
        }else{
            params.updateValue("--", forKey: "device_id")
        }

        let head =  AppSettings.shared.prepareHeader(withAuth: false)
        print_debug("hitting \(api.login.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.login.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("sign in json is:\n\(json)")
                    let userParser = UserParser(json: json)
                    if userParser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        if userParser.errorCode == .success{
                            AppSettings.shared.isLoggedIn = true
                            AppSettings.shared.isRegistered = true
                            userParser.user.saveUserJSON(json)
                        }
                        completionBlock((userParser.errorCode == .success), userParser.user, userParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }

    }

    func forgotPasswordForEmail(_ email: String,completionBlock:@escaping (_ success:Bool, _ message:String)-> Void){
        let param = ["email":email,"role":"user"]
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        Alamofire.request(api.forgotPassword.url(), method: .post, parameters: param,  headers: ["Accept": "application/json"]).responseJSON { response in

            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("forgotpassword json is:\n\(json)")
                    if let responseCode = json["code"].int as Int?
                    {
                        if responseCode == 200{
                            completionBlock(true,json["message"].string ?? "")
                        }
                        else{
                            completionBlock(false,json["message"].string ?? "")
                        }
                    }else{
                        completionBlock(false,json["message"].string ?? "")
                    }
                }
            case .failure(let error):
                print_debug(error)
                completionBlock(false,error.localizedDescription)
            }
        }
    }
    
    func updateProfileWith(_ fullName:String, email:String, attachmentImage: UIImage?,completionBlock:@escaping (_ success:Bool, _ user: User?, _ message: String) -> Void){
        
        var params = Dictionary<String,String >()
        params.updateValue("user", forKey: "role")
        params.updateValue(fullName, forKey: "full_name")
        params.updateValue(email, forKey: "email")

        let head =  AppSettings.shared.prepareHeader(withAuth: true)

        print_debug("hitting \(api.editProfile.url()) with param \(params) and headers :\(head)")
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let pimage = attachmentImage{
                    if let data = UIImageJPEGRepresentation(pimage, 0.9) as Data?{
                        multipartFormData.append(data, withName: "file_source", fileName: "file_source", mimeType: "image/jpg")
                    }
                }
                for (key, value) in params {
                    multipartFormData.append((value).data(using: .utf8)!, withName: key)
                }
        },
            to: api.editProfile.url(),method:.post,
            headers: head,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                        case .success:
                            if let value = response.result.value {
                                let json = JSON(value)
                                print_debug("update profile in json is:\n\(json)")
                                let userParser = UserParser(json: json)
                                if userParser.errorCode == .forceUpdate{
                                    AppSettings.shared.showForceUpdateAlert()
                                }else if userParser.errorCode == .sessionExpire{
                                    AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                                }else{
                                    if userParser.errorCode == .success{
                                        AppSettings.shared.isLoggedIn = true
                                        userParser.user.saveUserJSON(json)
                                    }
                                    completionBlock((userParser.errorCode == .success), userParser.user, userParser.responseMessage)
                                }
                            }else{
                                completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                            }
                        case .failure(let error):
                            completionBlock(false,nil,error.localizedDescription )
                        }
                    }
                case .failure(let encodingError):
                    completionBlock(false,nil,encodingError.localizedDescription )
                }
        })
    }
    

    func registerUserWith(_ fullName:String, mobile:String, password:String,completionBlock:@escaping (_ success:Bool, _ user: User?, _ message: String) -> Void){
        var params = Dictionary<String,String >()
        params.updateValue(fullName, forKey: "full_name")
        params.updateValue(password, forKey: "password")
        params.updateValue(mobile.removingWhitespaces(), forKey: "contact")
        params.updateValue("true", forKey: "is_contact_verified")
        params.updateValue("ios", forKey: "device_type")
        params.updateValue("user", forKey: "role")
        if let deviceID = kUserDefaults.value(forKey: kDeviceToken) as? String{
            params.updateValue(deviceID, forKey: "device_id")
        }else{
            params.updateValue("--", forKey: "device_id")
        }

        let head =  AppSettings.shared.prepareHeader(withAuth: false)
        print_debug("hitting \(api.signUp.url()) with param \(params) and headers :\(head)")

        Alamofire.request(api.signUp.url(), method: .post, parameters: params, headers: head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("sign up json is:\n\(json)")
                    let userParser = UserParser(json: json)
                    if userParser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if userParser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        if userParser.errorCode == .success{
                            AppSettings.shared.isLoggedIn = true
                            AppSettings.shared.isRegistered = true
                            userParser.user.saveUserJSON(json)
                        }
                        completionBlock((userParser.errorCode == .success), userParser.user, userParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription )
            }
        }
    }


    func logOut( userID: String, completionBlock:@escaping ( _ success:Bool, _ user: User?, _ message: String) -> Void){
        let params = ["user_id":userID,"role":"user"]
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.logout.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.logout.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    let userParser = UserParser(json: json)
                    completionBlock((userParser.errorCode == .success), userParser.user, userParser.responseMessage)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }


    func getTokenToResetPasswordAfterForgot( contact: String, completionBlock:@escaping ( _ success:Bool,_ isExists:Bool,  _ token: String?, _ message: String) -> Void){
        let params = ["contact":contact,"role":"user"]
        let head = AppSettings.shared.prepareHeader(withAuth: false)
        print_debug("hitting \(api.isExistingContact.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.isExistingContact.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("validation json is:\n\(json)")
                    let parser = ForgotPasswordParser(json: json)
                    completionBlock((parser.code == 409),(parser.code == 409),parser.token,parser.responseMessage)
                }else{
                    completionBlock(false,false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,false,nil,error.localizedDescription)
            }
        }
    }

    
    func resetPassword( contact: String, accessToken:String,password:String, completionBlock:@escaping ( _ success:Bool,_ done:Bool, _ message: String) -> Void){
        var params = ["contact":contact,"role":"user"]
        params.updateValue(password, forKey: "password")
        var head = AppSettings.shared.prepareHeader(withAuth: false)
        head.updateValue(accessToken, forKey: "securityToken")

        print_debug("hitting \(api.forgotPassword.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.forgotPassword.url(),method: .post ,parameters: params, headers:head).responseJSON { response in

            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("reset password json is:\n\(json)")
                    let parser = ForgotPasswordParser(json: json)
                    if parser.code == 102{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.code == 345{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        AppSettings.shared.hideLoader()
                        completionBlock((parser.code == 200),(parser.code == 200),parser.responseMessage)
                    }
                }else{
                    AppSettings.shared.hideLoader()
                    completionBlock(false,false,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                AppSettings.shared.hideLoader()
                completionBlock(false,false,error.localizedDescription)
            }
        }
    }

    func setupPassword(_ contact:String, oldPassword : String, withNewPassword newPassword: String,completionBlock:@escaping (_ success: Bool,_ message:String)-> Void){
        var param = Dictionary<String,String>()
        param.updateValue(oldPassword, forKey: "old_password")
        param.updateValue(newPassword, forKey: "new_password")
        param.updateValue(contact, forKey: "contact")
        param.updateValue("user", forKey: "role")
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.resetPassword.url()) with param \(param) and headers :\(head)")
        AppSettings.shared.showLoader(withStatus: "Setting up..")
        Alamofire.request(api.resetPassword.url(), method: .post, parameters: param,  headers: head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("reset password json is:\n\(json)")
                    let parser = ForgotPasswordParser(json: json)
                    if parser.code == 102{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.code == 345{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        AppSettings.shared.hideLoader()
                        completionBlock((parser.code == 200),parser.responseMessage)
                    }
                }else{
                    AppSettings.shared.hideLoader()
                    completionBlock(false,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                AppSettings.shared.hideLoader()
                completionBlock(false,error.localizedDescription)
            }
        }
    }



    func addParent( name: String, phoneNumber:String,countryCode:String, address:String, city:String, zipCode:String, completionBlock:@escaping ( _ success:Bool,_ parent:Parent?, _ message: String) -> Void){
        var params = ["role":"user"]
        let contact = phoneNumber.removingWhitespaces()
        let country = countryCode.removingWhitespaces()
        params.updateValue(name, forKey: "name")
        params.updateValue(contact, forKey: "contact")
        params.updateValue(address, forKey: "address")
        params.updateValue(country, forKey: "country_code")
        params.updateValue(city, forKey: "city")
        params.updateValue(zipCode, forKey: "zipcode")

        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.addParent.url()) with param \(params) and headers :\(head)")

        Alamofire.request(api.addParent.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("parent json is:\n\(json)")
                    let parser = ParentParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.parent,parser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }


    func updateParent(id:String, name: String, phoneNumber:String,countryCode:String, address:String, city:String, zipCode:String, completionBlock:@escaping ( _ success:Bool,_ parent:Parent?, _ message: String) -> Void){
        var params = ["role":"user"]
        let contact = phoneNumber.removingWhitespaces()
        let country = countryCode.removingWhitespaces()
        params.updateValue(id, forKey: "parent_id")
        params.updateValue(name, forKey: "name")
        params.updateValue(contact, forKey: "contact")
        params.updateValue(address, forKey: "address")
        params.updateValue(country, forKey: "country_code")
        params.updateValue(city, forKey: "city")
        params.updateValue(zipCode, forKey: "zipcode")

        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.updateParent.url()) with param \(params) and headers :\(head)")

        Alamofire.request(api.updateParent.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("updates parent json is:\n\(json)")
                    let parser = ParentParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.parent,parser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }



    func getParent(completionBlock:@escaping ( _ success:Bool,_ parents:Array<Parent>?, _ message: String) -> Void){
        let params = ["role":"user"]
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.getParent.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.getParent.url(),method: .get ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Parents json is:\n\(json)")
                    let parser = ParentParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.parents,parser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }


    func deleteParent( id: String, completionBlock:@escaping ( _ success:Bool, _ message: String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.deleteParent.url()+id) and headers :\(head)")
        Alamofire.request(api.deleteParent.url()+id,method: .get,headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("delete parent json is:\n\(json)")
                    let parser = ChildParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.message)
                    }
                }else{
                    completionBlock(false,"Oops! Something went wrong")
                }
            case .failure(let error):
                completionBlock(false,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }


    func addChild( name: String, gender:Gender, dateOfBirth:Date, school:String, teacher: String, alergies:String?, completionBlock:@escaping ( _ success:Bool,_ child:Child?, _ message: String) -> Void){
        var params = ["role":"user"]
        let date = CommonClass.sharedInstance.formattedDateWith(dateOfBirth, format: "YYYY-MM-dd")
        if let alergy = alergies{
            params.updateValue(alergy, forKey: "alergies")
        }

        params.updateValue(date, forKey: "date_of_birth")
        params.updateValue(name, forKey: "full_name")
        params.updateValue(gender.rawValue, forKey: "gender")
        params.updateValue(school, forKey: "school_name")
        params.updateValue(teacher, forKey: "teacher_name")
        params.updateValue("", forKey: "image")


        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.addChild.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.addChild.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("child json is:\n\(json)")
                    let parser = ChildParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.child,parser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    func updateChild(childId:String, name: String, gender:Gender, dateOfBirth:Date, school:String, teacher: String, alergies:String?, completionBlock:@escaping ( _ success:Bool,_ child:Child?, _ message: String) -> Void){
        var params = ["role":"user"]
        let date = CommonClass.sharedInstance.formattedDateWith(dateOfBirth, format: "YYYY-MM-dd")
        if let alergy = alergies{
            params.updateValue(alergy, forKey: "alergies")
        }
        params.updateValue(childId, forKey: "child_id")
        params.updateValue(date, forKey: "date_of_birth")
        params.updateValue(name, forKey: "full_name")
        params.updateValue(gender.rawValue, forKey: "gender")
        params.updateValue(school, forKey: "school_name")
        params.updateValue(teacher, forKey: "teacher_name")
        params.updateValue("", forKey: "image")
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.updateChild.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.updateChild.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("child json is:\n\(json)")
                    let parser = ChildParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.child,parser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    func deleteChild( id: String, completionBlock:@escaping ( _ success:Bool, _ message: String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.deleteChild.url()+id) and headers :\(head)")
        Alamofire.request(api.deleteChild.url()+id,method: .get,headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("delete child json is:\n\(json)")
                    let parser = ChildParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.message)
                    }
                }else{
                    completionBlock(false,"Oops! Something went wrong")
                }
            case .failure(let error):
                completionBlock(false,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }


    func getchildren(childId id: String?=nil, completionBlock:@escaping ( _ success:Bool,_ children:Array<Child>?, _ message: String) -> Void){
        var params = ["role":"user"]
        if let childId = id{
            params.updateValue(childId, forKey: "child_id")
        }

        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.getChildren.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.getChildren.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("child json is:\n\(json)")
                    let parser = ChildParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.children,parser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    
    
    func contactVerifyBeforeSinUp( contact: String, completionBlock:@escaping ( _ success:Bool,_ isExists:Bool,  _ token: String?, _ message: String) -> Void){
        let params = ["contact":contact,"role":"user"]
        let head = AppSettings.shared.prepareHeader(withAuth: false)
        print_debug("hitting \(api.isExistingContact.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.isExistingContact.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("validation json is:\n\(json)")
                    let parser = ForgotPasswordParser(json: json)
                    completionBlock((parser.code == 404),(parser.code == 404),parser.token,parser.responseMessage)
                }else{
                    completionBlock(false,false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,false,nil,error.localizedDescription)
            }
        }
    }
}


