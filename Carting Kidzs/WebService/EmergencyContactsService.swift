//
//  AddContactService.swift
//  TaxiApp
//
//  Created by tecorb on 3/14/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EmergencyContactsService {
    static let sharedInstance = EmergencyContactsService()
    fileprivate init() {}
    
    func addEmergencyContact(_ firstName:String, lastName:String, mobile:String,profileImage: UIImage?,userID:String,completionBlock:@escaping (_ success:Bool, _ user: Contact?, _ message: String) -> Void){

        var params = Dictionary<String,String >()
        params.updateValue(firstName, forKey: "first_name")
        params.updateValue(lastName, forKey: "last_name")
        params.updateValue(mobile, forKey: "contact")
        params.updateValue(userID, forKey: "driver_id")
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let pimage = profileImage{
                    if let data = UIImageJPEGRepresentation(pimage, 0.7) as Data?{
                        multipartFormData.append(data, withName: "image", fileName: "image", mimeType: "image/jpg")
                    }
                }
                
                for (key, value) in params {
                    multipartFormData.append((value).data(using: .utf8)!, withName: key)
                }
        },
            to: api.emergencyContact.url(),method:.post,
            headers: head,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                        case .success:
                            if let value = response.result.value {
                                let json = JSON(value)
                                let parser = EmergencyContactParser(json: json)
                                if parser.errorCode == .forceUpdate{
                                    AppSettings.shared.hideLoader()
                                    AppSettings.shared.showForceUpdateAlert()
                                }else if parser.errorCode == .sessionExpire{
                                    AppSettings.shared.hideLoader()
                                    AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                                }else{
                                    completionBlock((parser.errorCode == .success),parser.contact,parser.message)
                                }
                            }else{
                                completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                            }
                            case .failure(let error):
                                completionBlock(false,nil,error.localizedDescription)
                        }
                    }
                case .failure(let encodingError):
                    completionBlock(false,nil,encodingError.localizedDescription)
                }
        })

    }

func userContactList(userID: String,completionBlock:@escaping (_ success:Bool, _ user: Array<Contact>?, _ message: String) -> Void){

    let params = ["driver_id":userID]
    let head =  AppSettings.shared.prepareHeader(withAuth: true)
    Alamofire.request(api.allContact.url(),method: .post ,parameters: params,headers:head).responseJSON { response in
        AppSettings.shared.hideLoader()
        switch response.result {
        case .success:
            if let value = response.result.value {
                let json = JSON(value)
                let parser = EmergencyContactParser(json: json)
                if parser.errorCode == .forceUpdate{
                    AppSettings.shared.hideLoader()
                    AppSettings.shared.showForceUpdateAlert()
                }else if parser.errorCode == .sessionExpire{
                    AppSettings.shared.hideLoader()
                    AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                }else{
                    completionBlock((parser.errorCode == .success),parser.contacts,parser.message)
                }
            }else{
                completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
            }
        case .failure(let error):
            completionBlock(false,nil,error.localizedDescription)
        }
    }

}
    
    
    func removeContact(contactID: String, completionBlock: @escaping(_ success:Bool, _ didRemoved: Bool, _ message: String) -> Void){
        let params = ["contact_id":contactID]
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        Alamofire.request(api.removeContact.url(),method: .post ,parameters: params,headers:head).responseJSON { (response) in
            AppSettings.shared.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Remove contact in json is:\n\(json)")
                    let parser = EmergencyContactParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),(parser.errorCode == .success), parser.message)
                    }
                }else{
                    completionBlock(false,false,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,false,error.localizedDescription)
            }
        }
    }
    
    
}
