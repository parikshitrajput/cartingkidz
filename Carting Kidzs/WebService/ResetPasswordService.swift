//
//  ResetPasswordService.swift
//  TaxiApp
//
//  Created by tecorb on 3/14/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ResetPasswordService {
    static let sharedInstance = ResetPasswordService()
    fileprivate init() {}

    func resetPasswordForEmail(userID: String,oldPassword:String,newPassword:String,completionBlock:@escaping (_ success:Bool)-> Void){
        
        var param = ["user_id":userID,"role":"user"]
        param.updateValue(oldPassword, forKey: "old_password")
        param.updateValue(newPassword, forKey: "new_password")
        Alamofire.request(api.resetPassword.url(), method: .post, parameters: param,  headers: ["Accept": "application/json"]).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug(json)
                    if let responseCode = json["code"].int as Int?,responseCode == 200{
                        completionBlock(true)
                    }else{
                        completionBlock(false)
                    }
                }else{
                    completionBlock(false)
                }
            case .failure://(let error):
                completionBlock(false)
            }
        }
    }

}
