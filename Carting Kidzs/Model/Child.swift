//
//  Child.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 13/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

func ==(lhs: Child, rhs: Child) -> Bool {
    return lhs.id == rhs.id
}

class Child: NSObject{
/*
    "school_name" : "ABC",
    "teacher_name" : "Ana"
*/
    enum keys: String, CodingKey {
        case id = "id"
        case name = "name"
        case dateOfBirth = "date_of_birth"
        case alergies = "alergies"
        case image = "image"
        case isActive = "is_active"
        case gender = "child_gender"
        case teacher = "teacher_name"
        case school = "school_name"
    }

    //values
    var id = ""
    var name : String = ""
    var dateOfBirth = ""
    var alergies = ""
    var image = ""
    var isActive : Bool = true
    var gender: Gender = .male
    var teacher = ""
    var school = ""

    override init() {
        super.init()
    }
    init(dictionary:Dictionary<String,AnyObject>) {
        if let _id = dictionary[keys.id.stringValue] as? String{
            self.id = _id
        }else if let _id = dictionary[keys.id.stringValue] as? Int{
            self.id = "\(_id)"
        }

        if let _name = dictionary[keys.name.stringValue] as? String{
            self.name = _name
        }
        if let _dob = dictionary[keys.dateOfBirth.stringValue] as? String{
            self.dateOfBirth = _dob
        }
        if let _image = dictionary[keys.image.stringValue] as? String{
            self.image = _image
        }
        if let _alergies = dictionary[keys.alergies.stringValue] as? String{
            self.alergies = _alergies
        }

        if let _isActive = dictionary[keys.isActive.stringValue] as? Bool{
            self.isActive = _isActive
        }
        if let _gender = dictionary[keys.gender.stringValue] as? String{
            self.gender = Gender(rawValue: _gender)
        }
        if let _teacher = dictionary[keys.teacher.stringValue] as? String{
            self.teacher = _teacher
        }
        if let _school = dictionary[keys.school.stringValue] as? String{
            self.school = _school
        }

        super.init()
    }
}

class ChildParser : NSObject{
    enum keys:String,CodingKey {
        case code = "code"
        case message = "message"
        case children = "childs"
        case child = "child"
        case user = "user"
    }


    var errorCode :ErrorCode = .failure
    var message = ""
    var children = [Child]()
    var child = Child()

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[keys.code.stringValue].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _rMessage = json[keys.message.stringValue].string as String?{
            self.message = _rMessage
        }



        if let _childDict = json[keys.user.stringValue].dictionaryObject as [String:AnyObject]?{
            if let _childrens  = _childDict[keys.children.stringValue] as? [[String: AnyObject]]{
                self.children.removeAll()
                for _child in _childrens{
                    let child = Child(dictionary: _child)
                    self.children.append(child)
                }
            }

            if let _child  = _childDict[keys.child.stringValue] as? [String: AnyObject]{
                self.child = Child(dictionary: _child)
            }
        }


        if let _childDict = json[keys.child.stringValue].dictionaryObject as [String:AnyObject]?{
            self.child = Child(dictionary: _childDict)
        }
        
        if let _childrens  = json[keys.children.stringValue].arrayObject as? [[String: AnyObject]]{
            self.children.removeAll()
            for _child in _childrens{
                let child = Child(dictionary: _child)
                self.children.append(child)
            }
        }
        super.init()
    }

}


