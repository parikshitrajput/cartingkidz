//
//  Card.swift
//  TaxiApp
//
//  Created by tecorb on 3/14/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class CardParser: NSObject {

    var errorCode = ErrorCode.failure
    var responseMessage = ""
    var cards = Array<Card>()
    var card = Card()

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _code = json["code"].int{
            self.errorCode = ErrorCode(rawValue: _code)
        }

        if let _msg = json["message"].string as String?{
            self.responseMessage = _msg
        }

        if let _cards = json["cards"].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for _cardDict in _cards{
                let card = Card(params: _cardDict)
                self.cards.append(card)
            }
        }

        if let _card = json["card"].dictionaryObject as Dictionary<String,AnyObject>?{
            self.card = Card(params: _card)
        }

    }
}

class Card:NSObject{
    let kID = "id"
    let kCardToken = "card_token"
    let kExpMonth = "exp_month"
    let kExpYear = "exp_year"
    let kLast4 = "last4"
    let kBrand = "brand"
    let kFundingType = "funding_type"
    let kIsDefault = "is_default"


    var ID = ""
    var cardToken : String = ""
    var expMonth: UInt = 0
    var expYear : UInt = 0
    var last4 : String = ""
    var brand : String = ""
    var fundingType :String = ""
    var isDefault : Bool = false

    override init() {
        super.init()
    }

    init(params:Dictionary<String,AnyObject>) {

        if let _ID = params[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = params[kID] as? String{
            self.ID = _ID
        }

        if let _token = params[kCardToken] as? Int{
            self.cardToken = "\(_token)"
        }else if let _token = params[kCardToken] as? String{
            self.cardToken = _token
        }

        if let _expMonth = params[kExpMonth] as? UInt{
            self.expMonth = _expMonth
        }else if let _expMonth = params[kExpMonth] as? String{
            self.expMonth = UInt(_expMonth) ?? 0
        }

        if let _expYear = params[kExpYear] as? UInt{
            self.expYear = _expYear
        }else if let _expYear = params[kExpYear] as? String{
            self.expYear = UInt(_expYear) ?? 0
        }

        if let _last4 = params[kLast4] as? String{
            self.last4 = _last4
        }
        if let _brand = params[kBrand] as? String{
            self.brand = _brand
        }
        if let _fundingType = params[kFundingType] as? String{
            self.fundingType = _fundingType
        }
        if let _isDefault = params[kIsDefault] as? Bool{
            self.isDefault = _isDefault
        }
        super.init()
    }
}
