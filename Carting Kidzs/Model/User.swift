//
//  User.swift
//  MyLaundryApp
//
//  Created by TecOrb on 15/12/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
    //key
    let kID = "id"
    let kFullName = "first_name"

    let kEmail = "email"
    let kContact = "contact"
    let kProfileImage = "image"
    let kUserRole = "role"
    let kUserName = "user_name"
    let kDescription = "description"
    let kApiKey = "api_key"

    let kAddress = "address"
    let card = "Card"
    let kParentName = "guardian_name"
    let kParentContact = "guardian_contact"
    let kParentAddress = "guardian_address"
    let kParentCC = "guardian_country_code"

    let kAvgRating = "avg_rating"
    let kLocation = "location"

    //properties
    var ID : String = ""
    var userName: String = ""
    var fullName : String = ""
    var email : String = ""
    var contact : String = ""
    var role : String = ""
    var profileImage : String = ""

    var addCard = Card()
    var apiKey: String = ""

    var address = ""
    var parentName = ""
    var parentContact = ""
    var parentAddress = ""
    var parentCountryCode = ""
    var rating:Double = 0
    var location = RideLocationModel()

    override init() {
        super.init()
    }

    init(json : JSON){
        if let userId = json[kID].int as Int?{
            self.ID = "\(userId)"
        }else if let userId = json[kID].string as String?{
            self.ID = userId
        }

        if let _pCC = json[kParentCC].int as Int?{
            self.parentCountryCode = "\(_pCC)"
        }else if let _pCC = json[kParentCC].string as String?{
            self.parentCountryCode = _pCC
        }

        if let fName = json[kFullName].string as String?{
            self.fullName = fName
        }
        if let _apiKey = json[kApiKey].string as String?{
            self.apiKey = _apiKey
        }
        
        if let _userName = json[kUserName].string as String?{
            self.userName = _userName
        }

        if let userEmail = json[kEmail].string as String?{
            self.email = userEmail
        }
        if let _card = json[card].dictionaryObject as [String:AnyObject]?{
            self.addCard = Card(params: _card)
        }
        if let userContact = json[kContact].string as String?{
            self.contact = userContact
        }

        if let _userRole = json[kUserRole].string as String?{
            self.role = _userRole
        }

        if let userProfileImage = json[kProfileImage].string as String?{
            self.profileImage = userProfileImage
        }

        if let _parentName = json[kParentName].string as String?{
            self.parentName = _parentName
        }
        if let _parentContact = json[kParentContact].string as String?{
            self.parentContact = _parentContact
        }
        if let _parentAddress = json[kParentAddress].string as String?{
            self.parentAddress = _parentAddress
        }
        if let _address = json[kAddress].string as String?{
            self.address = _address
        }
        
        if let _avgRating = json[kAvgRating].string{
            self.rating = Double(_avgRating) ?? 0
        }else if let _avgRating = json[kAvgRating].double{
            self.rating = _avgRating
        }

        if let _location = json[kLocation].dictionary as Dictionary<String,AnyObject>?{
            self.location = RideLocationModel(dictionary: _location)
        }
        super.init()
    }

    init(dictionary : [String:AnyObject]){

        if let userId = dictionary[kID] as? NSInteger{
            self.ID = "\(userId)"
        }else if let userId = dictionary[kID] as? String{
            self.ID = userId
        }

        if let _pCC = dictionary[kParentCC] as? NSInteger{
            self.parentCountryCode = "\(_pCC)"
        }else if let _pCC = dictionary[kParentCC] as? String{
            self.parentCountryCode = _pCC
        }

        if let fName = dictionary[kFullName] as? String{
            self.fullName = fName
        }
        
        if let _apiKey = dictionary[kApiKey] as? String{
            self.apiKey = _apiKey
        }

        if let _userName = dictionary[kUserName] as? String{
            self.userName = _userName
        }

        if let userEmail = dictionary[kEmail] as? String{
            self.email = userEmail
        }

        if let userContact = dictionary[kContact] as? String{
            self.contact = userContact
        }
        if let _card = dictionary[card] as? [String:AnyObject]{
            self.addCard = Card(params: _card)
        }

        if let _userRole = dictionary[kUserRole] as? String{
            self.role = _userRole
        }

        if let userProfileImage = dictionary[kProfileImage] as? String{
            self.profileImage = userProfileImage
        }

        if let _parentName = dictionary[kParentName] as? String{
            self.parentName = _parentName
        }
        if let _parentContact = dictionary[kParentContact] as? String{
            self.parentContact = _parentContact
        }
        if let _parentAddress = dictionary[kParentAddress] as? String{
            self.parentAddress = _parentAddress
        }
        if let _address = dictionary[kAddress] as? String{
            self.address = _address
        }

        if let _avgRating = dictionary[kAvgRating] as? String{
            self.rating = Double(_avgRating) ?? 0
        }else if let _avgRating = dictionary[kAvgRating] as? Double{
            self.rating = _avgRating
        }

        if let _location = dictionary[kLocation] as? Dictionary<String,AnyObject>{
            self.location = RideLocationModel(dictionary: _location)
        }

        super.init()
    }

    func saveUserJSON(_ json:JSON) {
        if let userInfo = json["user"].dictionaryObject as [String:AnyObject]?{
            let documentPath = NSHomeDirectory() + "/Documents/"
            do {
                let data = try JSON(userInfo).rawData(options: [.prettyPrinted])
                let path = documentPath + "user"
                try data.write(to: URL(fileURLWithPath: path), options: .atomic)
            }catch{
                print_debug("error in saving userinfo")
            }
            UserDefaults.standard.synchronize()
        }else if let userInfo = json["User"].dictionaryObject as [String:AnyObject]?{
            let documentPath = NSHomeDirectory() + "/Documents/"
            do {
                let data = try JSON(userInfo).rawData(options: [.prettyPrinted])
                let path = documentPath + "user"
                try data.write(to: URL(fileURLWithPath: path), options: .atomic)
            }catch{
                print_debug("error in saving userinfo")
            }
            UserDefaults.standard.synchronize()
        }
    }

    func isParentAddedd() -> Bool{
        return ((self.parentName.trimmingCharacters(in: .whitespaces).count != 0) || (self.parentContact.trimmingCharacters(in: .whitespaces).count != 0) || (self.parentAddress.trimmingCharacters(in: .whitespaces).count != 0))
    }


    class func loadSavedUser() -> User {
        let documentPath = NSHomeDirectory() + "/Documents/"
        let path = documentPath + "user"
        var data = Data()
        var json : JSON
        do{
            data = try Data(contentsOf: URL(fileURLWithPath: path))
            json = try JSON(data: data)
        }catch{
            json = JSON.init(data)
            print_debug("error in getting userinfo")
        }

        let user = User(json: json)
        return user
    }

    class func logOut(_ completionBlock:@escaping (_ success:Bool,_ user:User?,_ message:String) -> Void){
        let user = User.loadSavedUser()
        LoginService.sharedInstance.logOut(userID: user.ID) { (success, user, message) in
            completionBlock(success, user, message)
        }
    }

}



class UserParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kUsers = "user"
    let kEmergencyContact = "emergency_contact"
    let kContacts = "Contacts"
    let kPayment = "payment"

    var responseMessage = ""
    var users = [User]()
    var contacts = [User]()
    var user = User()
    //var payment =
    var errorCode: ErrorCode = .failure
    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _message = json[kResponseMessage].string{
            self.responseMessage = _message
        }
        
        if let _user =  json[kUsers.capitalized].dictionaryObject as Dictionary<String,AnyObject>?{
            self.user = User(dictionary: _user)
        }else if let _user =  json[kUsers].dictionaryObject as Dictionary<String,AnyObject>?{
            self.user = User(dictionary: _user)
        }
        if let _user =  json[kEmergencyContact].dictionaryObject as Dictionary<String,AnyObject>?{
            self.user = User(dictionary: _user)
        }

//        if let _payment =  json[kPayment].dictionaryObject as Dictionary<String,AnyObject>?{
//            self.user = User(dictionary: _payment)
//        }

        if let usrs  = json[kUsers].arrayObject as? [[String: AnyObject]]{
            users.removeAll()
            for u in usrs{
                let usr = User(dictionary: u)
                users.append(usr)
            }
        }else if let usrs  = json[kContacts].arrayObject as? [[String: AnyObject]]{
            users.removeAll()
            for u in usrs{
                let usr = User(dictionary: u)
                contacts.append(usr)
            }
        }
    }
    
}





class ForgotPasswordParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kToken = "securityToken"
    let kIsVarified = "is_varified"

    var responseMessage = ""
    var code: Int = 0
    var token = ""
    var isVarified = false
    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.code = _rCode
        }

        if let _message = json[kResponseMessage].string{
            self.responseMessage = _message
        }

        if let _token = json[kToken].string{
            self.token = _token
        }else if let _token = json[kToken].int{
            self.token = "\(_token)"
        }
    }

}



class PaymentParser: NSObject {
    let kCode = "code"
    let kMessage = "message"
    let kPayment = "ride"

    var message = ""
    var errorCode: ErrorCode = .failure
    var payment = Payment()
    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[kCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _message = json[kMessage].string{
            self.message = _message
        }

        if let _payment = json[kPayment].dictionaryObject as Dictionary<String,AnyObject>?{
            self.payment = Payment(dictionary: _payment)
        }
    }
}

class Payment: NSObject {
    enum keys:String,CodingKey {
        case amount = "amount"
        case id = "id"
        case ride = "ride_detail"
        case status = "status"
        case trxnNumber = "transaction_number"
    }

    var amount: Double = 0.0
    var id:String = ""
    var ride = Ride()
    var status:Bool = false
    var trxnNumber:String = ""

    override init() {
        super.init()
    }

    init(dictionary: Dictionary<String,AnyObject>) {
        if let _id = dictionary[keys.id.stringValue] as? Int{
            self.id = "\(_id)"
        }else if let _id = dictionary[keys.id.stringValue] as? String{
            self.id = _id
        }

        if let _amount = dictionary[keys.amount.stringValue] as? Double{
            self.amount = _amount
        }else if let _amount = dictionary[keys.amount.stringValue] as? String{
            self.amount = Double(_amount) ?? 0.0
        }

        if let _ride = dictionary[keys.ride.stringValue] as? Dictionary<String,AnyObject>{
            self.ride = Ride(dictionary: _ride)
        }

        if let _status = dictionary[keys.status.stringValue] as? Bool{
            self.status = _status
        }
        if let _trxn = dictionary[keys.trxnNumber.stringValue] as? String{
            self.trxnNumber = _trxn
        }
        super.init()
    }

}






//MARK:- Energency contact parser

class EmergencyContactParser: NSObject {
    enum keys:String, CodingKey{
        case code = "code"
        case message = "message"
        case contact = "emergency_contact"
        case contacts = "contacts"

    }


    var errorCode: ErrorCode = .failure
    var message = ""
    var contact = Contact()
    var contacts = Array<Contact>()
    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[keys.code.stringValue].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _message = json[keys.message.stringValue].string{
            self.message = _message
        }

        if let _contact = json[keys.contact.stringValue].dictionaryObject as Dictionary<String,AnyObject>?{
            self.contact = Contact(dictionary: _contact)
        }

        if let _contacts = json[keys.contacts.stringValue].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for contactDict in _contacts{
                let contact = Contact(dictionary: contactDict)
                self.contacts.append(contact)
            }
        }
    }
}



class Contact: NSObject {
    enum keys:String,CodingKey {
        case id = "id"
        case userID = "user_id"
        case firstName = "first_name"
        case lastName = "last_name"
        case contact = "contact"
        case image = "image"
    }

    var id = ""
    var userID = ""
    var firstName = ""
    var lastName = ""
    var contact = ""
    var image = ""

    override init() {
        super.init()
    }

    init(dictionary: Dictionary<String,AnyObject>) {
        if let _id = dictionary[keys.id.stringValue] as? Int{
            self.id = "\(_id)"
        }else if let _id = dictionary[keys.id.stringValue] as? String{
            self.id = _id
        }

        if let _id = dictionary[keys.userID.stringValue] as? Int{
            self.userID = "\(_id)"
        }else if let _id = dictionary[keys.userID.stringValue] as? String{
            self.userID = _id
        }

        if let _fName = dictionary[keys.firstName.stringValue] as? String{
            self.firstName = _fName
        }

        if let _lName = dictionary[keys.lastName.stringValue] as? String{
            self.lastName = _lName
        }
        if let _contact = dictionary[keys.contact.stringValue] as? String{
            self.contact = _contact
        }
        if let _image = dictionary[keys.image.stringValue] as? String{
            self.image = _image
        }
        super.init()
    }

}


class ParentParser: NSObject {
    enum keys:String, CodingKey{
        case code = "code"
        case message = "message"
        case parents = "contacts"
        case parent = "parent"

    }


    var errorCode: ErrorCode = .failure
    var message = ""
    var parent = Parent()
    var parents = Array<Parent>()
    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[keys.code.stringValue].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _message = json[keys.message.stringValue].string{
            self.message = _message
        }

        if let parent = json[keys.parent.stringValue].dictionaryObject as Dictionary<String,AnyObject>?{
            self.parent = Parent(dictionary: parent)
        }

        if let parents = json[keys.parents.stringValue].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for parentDict in parents{
                let parent = Parent(dictionary: parentDict)
                self.parents.append(parent)
            }
        }
    }
}


class Parent: NSObject {
    enum keys:String,CodingKey {
        case id = "id"
        case name = "name"
        case countryCode = "country_code"
        case contact = "contact"
        case email = "email"
        case city = "city"
        case address = "address"
        case zipCode = "zipcode"
        case image = "image"
    }

    var id = ""
    var name = ""
    var countryCode = ""
    var contact = ""
    var email = ""
    var city = ""
    var address = ""
    var zipCode = ""
    var image = ""


    override init() {
        super.init()
    }

    init(dictionary: Dictionary<String,AnyObject>) {
        if let _id = dictionary[keys.id.stringValue] as? Int{
            self.id = "\(_id)"
        }else if let _id = dictionary[keys.id.stringValue] as? String{
            self.id = _id
        }
        if let name = dictionary[keys.name.stringValue] as? String{
            self.name = name
        }
        if let countryCode = dictionary[keys.countryCode.stringValue] as? String{
            self.countryCode = countryCode
        }
        if let contact = dictionary[keys.contact.stringValue] as? String{
            self.contact = contact
        }
        if let email = dictionary[keys.email.stringValue] as? String{
            self.email = email
        }
        if let city = dictionary[keys.city.stringValue] as? String{
            self.city = city
        }

        if let address = dictionary[keys.address.stringValue] as? String{
            self.address = address
        }

        if let zipCode = dictionary[keys.zipCode.stringValue] as? String{
            self.zipCode = zipCode
        }

        super.init()
    }

}






