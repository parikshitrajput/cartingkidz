//
//  DriverMarker.swift
//  TaxiApp
//
//  Created by TecOrb on 10/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import GoogleMaps


class DriverMarker: GMSMarker {
    var driver : Driver!
    init(driver: Driver) {
        self.driver = driver
        super.init()
        self.position = CLLocationCoordinate2D(latitude: driver.location.latitude, longitude: driver.location.longitude)
        self.icon = UIImage(named: "car")
        groundAnchor = CGPoint(x: 0.5, y: 1)
    }
}


class AddressMarker: GMSMarker {
    var markerImage: UIImage!
    init(image: UIImage) {
        self.markerImage = image
        super.init()
        self.icon = image
    }
}
