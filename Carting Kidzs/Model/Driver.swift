//
//  Driver.swift
//  TaxiApp
//
//  Created by tecorb on 3/28/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMaps

class TimeMarker: GMSMarker {
    var time: String!
    init(time:String) {
        self.time = time
        super.init()
        self.icon = imageForMarinaMarker()
    }

    func  imageForMarinaMarker() -> UIImage {
        let myview = MarkerView.instanceFromNib()
        myview.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        myview.priceLabel.text = time
        myview.setNeedsLayout()
        myview.layoutIfNeeded()
        return imageWithView(view: myview,scale: UIScreen.main.scale)
    }

    func imageWithView(view : UIView,scale:CGFloat) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let myimg : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return myimg
    }
}







class Driver: NSObject {
    let kLocation  = "l"
    let kPLocation  = "l"
    let driverID = ""
    
    var location = Coordinate()
    var pLocation = PreviousCoordinate()
    var id : String = ""
    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _location = json[kLocation].dictionaryObject as [String:AnyObject]?{
            self.location = Coordinate(dictionary: _location)
        }
        if let _pLocation = json[kPLocation].dictionaryObject as [String:AnyObject]?{
            self.pLocation = PreviousCoordinate(dictionary: _pLocation)
        }
        if let _id = json[driverID].string as String?{
            self.id = _id
        }
        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _location = dictionary[kLocation] as? [String:AnyObject]{
            self.location = Coordinate(dictionary: _location)
        }
        if let _pLocation = dictionary[kPLocation] as? [String:AnyObject]{
            self.pLocation = PreviousCoordinate(dictionary: _pLocation)
        }
        if let _id = dictionary[driverID] as? String{
            self.id = _id
        }
        super.init()
    }
}

class Coordinate: NSObject {
    let kLat = "0"
    let kLong = "1"
    
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _latitude = json[kLat].double as Double?{
            self.latitude = _latitude
        }else if let _latitude = json[kLat].string as String?{
            self.latitude = Double(_latitude) ?? 0.0
        }
        if let _longitude = json[kLong].double as Double?{
            self.longitude = _longitude
        }else if let _longitude = json[kLong].string as String?{
            self.longitude = Double(_longitude) ?? 0.0
        }
        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _latitude = dictionary[kLat] as? Double{
            self.latitude = _latitude
        }else if let _latitude = dictionary[kLat] as? String{
            self.latitude = Double(_latitude) ?? 0.0
        }
        if let _longitude = dictionary[kLong] as? Double{
            self.longitude = _longitude
        }else if let _longitude = dictionary[kLong] as? String{
            self.longitude = Double(_longitude) ?? 0.0
        }
        super.init()
    }
}

class PreviousCoordinate: NSObject {
    let kLat = "0"
    let kLong = "1"
    
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _latitude = json[kLat].double as Double?{
            self.latitude = _latitude
        }else if let _latitude = json[kLat].string as String?{
            self.latitude = Double(_latitude) ?? 0.0
        }
        if let _longitude = json[kLong].double as Double?{
            self.longitude = _longitude
        }else if let _longitude = json[kLong].string as String?{
            self.longitude = Double(_longitude) ?? 0.0
        }
        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _latitude = dictionary[kLat] as? Double{
            self.latitude = _latitude
        }else if let _latitude = dictionary[kLat] as? String{
            self.latitude = Double(_latitude) ?? 0.0
        }
        if let _longitude = dictionary[kLong] as? Double{
            self.longitude = _longitude
        }else if let _longitude = dictionary[kLong] as? String{
            self.longitude = Double(_longitude) ?? 0.0
        }
        super.init()
    }
}


