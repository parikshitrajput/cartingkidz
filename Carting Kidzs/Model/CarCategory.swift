//
// CarCategory.swift
//  MyLaundryApp
//
//  Created by TecOrb on 21/12/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON



class CarCategory: NSObject {

    let kID = "id"
    let kCategoryName = "category_name"
    let kBaseFair = "base_fair"
    let kBaseMile = "base_mile"
    let kPricePerMile = "price_per_mile"
    let kWaitingPricePerMile = "waiting_price_per_min"

    let kCancellationFee = "cancellation_fee"
    let kImage = "image"
    let kTime = "time"
    let kDistance = "distance"
    let kMaxSpeed = "maxspeed"
    let kRideFairPerMinute = "ride_time_fair"

    let kNearestCarLatitude = "nearest_car_latitude"
    let kNearestCarLongitude = "nearest_car_longitude"
    let kCarsName = "cars_name"
    let kCapecity = "capacity"

    var ID : String = ""
    var categoryName: String = ""
    var baseFair : Double = 0.0
    var baseMile:Double = 0.0
    var pricePerMile : Double = 0.0
    var waitingPricePerMinute :Double = 0.0


    var cancellationFee = 0.0
    var image = ""
    var maxSpeed = 0.0
    var rideFairPerMinute = 0.0


    var time : String = ""
    var distance: String = ""
    var nearestCarLatitude : Double = 0.0
    var nearestCarLongitude: Double = 0.0
    var carsName = ""
    var capecity: Int = 3


    override init() {
        super.init()
    }

    init(dictionary : [String:AnyObject]){

        if let _Id = dictionary[kID] as? Int{
            self.ID = "\(_Id)"
        }else if let _Id = dictionary[kID] as? String{
            self.ID = _Id
        }

        if let _name = dictionary[kCategoryName] as? String{
            self.categoryName = _name
        }

        if let _carsname = dictionary[kCarsName] as? String{
            self.carsName = _carsname
        }

        if let _baseFair = dictionary[kBaseFair] as? String{
            self.baseFair = Double(_baseFair) ?? 0.0
        }else if let _baseFair = dictionary[kBaseFair] as? Double{
            self.baseFair = _baseFair
        }

        if let _baseMile = dictionary[kBaseMile] as? String{
            self.baseMile = Double(_baseMile) ?? 0.0
        }else if let _baseMile = dictionary[kBaseMile] as? Double{
            self.baseMile = _baseMile
        }

        if let _cancellationFee = dictionary[kCancellationFee] as? String{
            self.cancellationFee = Double(_cancellationFee) ?? 0.0
        }else if let _cancellationFee = dictionary[kCancellationFee] as? Double{
            self.cancellationFee = _cancellationFee
        }
        if let _image = dictionary[kImage] as? String{
            self.image = _image
        }
        if let _maxSpeed = dictionary[kMaxSpeed] as? String{
            self.maxSpeed = Double(_maxSpeed) ?? 0.0
        }else if let _maxSpeed = dictionary[kMaxSpeed] as? Double{
            self.maxSpeed = _maxSpeed
        }

        if let _rideTimeFair = dictionary[kRideFairPerMinute] as? String{
            self.rideFairPerMinute = Double(_rideTimeFair) ?? 0.0
        }else if let _rideTimeFair = dictionary[kRideFairPerMinute] as? Double{
            self.rideFairPerMinute = _rideTimeFair
        }


        if let _pricePerMile = dictionary[kPricePerMile] as? String{
            self.pricePerMile = Double(_pricePerMile) ?? 0.0
        }else if let _pricePerMile = dictionary[kPricePerMile] as? Double{
            self.pricePerMile = _pricePerMile
        }

        if let _waitingPricePerMile = dictionary[kWaitingPricePerMile] as? String{
            self.waitingPricePerMinute = Double(_waitingPricePerMile) ?? 0.0
        }else if let _waitingPricePerMile = dictionary[kWaitingPricePerMile] as? Double{
            self.waitingPricePerMinute = _waitingPricePerMile
        }



        if let _time = dictionary[kTime] as? String{
            self.time = _time
        }else if let _time = dictionary[kTime] as? Double{
            self.time = "\(_time)"
        }

        if let _distance = dictionary[kDistance] as? String{
            self.distance = _distance
        }else if let _distance = dictionary[kDistance] as? Double{
            self.distance = "\(_distance)"
        }


        //nearest Car location
        if let _lat = dictionary[kNearestCarLatitude] as? String{
            self.nearestCarLatitude = Double(_lat) ?? 0.0
        }else if let _lat = dictionary[kNearestCarLatitude] as? Double{
            self.nearestCarLatitude = _lat
        }

        if let _longitude = dictionary[kNearestCarLongitude] as? String{
            self.nearestCarLongitude = Double(_longitude) ?? 0.0
        }else if let _longitude = dictionary[kNearestCarLongitude] as? Double{
            self.nearestCarLongitude = _longitude
        }

        if let _capecity = dictionary[kCapecity] as? Int{
            self.capecity = _capecity
        }else if let _capecity = dictionary[kCapecity] as? String{
            self.capecity = Int(_capecity) ?? 4
        }


        super.init()
    }

     init(json: JSON) {

        if let _Id = json[kID].int as Int?{
            self.ID = "\(_Id)"
        }else if let _Id = json[kID].string as String?{
            self.ID = _Id
        }
        if let _capecity = json[kCapecity].int{
            self.capecity = _capecity
        }else if let _capecity = json[kCapecity].string{
            self.capecity = Int(_capecity) ?? 4
        }

        if let _name = json[kCategoryName].string as String?{
            self.categoryName = _name
        }

        if let _carsname = json[kCarsName].string as String?{
            self.carsName = _carsname
        }

        if let _baseFair = json[kBaseFair].string as String?{
            self.baseFair = Double(_baseFair) ?? 0.0
        }else if let _baseFair = json[kBaseFair].double as Double?{
            self.baseFair = _baseFair
        }

        if let _baseMile = json[kBaseMile].string as String?{
            self.baseMile = Double(_baseMile) ?? 0.0
        }else if let _baseMile = json[kBaseMile].double as Double?{
            self.baseMile = _baseMile
        }

        if let _pricePerMile = json[kPricePerMile].string as String?{
            self.pricePerMile = Double(_pricePerMile) ?? 0.0
        }else if let _pricePerMile = json[kPricePerMile].double as Double?{
            self.pricePerMile = _pricePerMile
        }

        if let _waitingPricePerMile = json[kWaitingPricePerMile].string as String?{
            self.waitingPricePerMinute = Double(_waitingPricePerMile) ?? 0.0
        }else if let _waitingPricePerMile = json[kWaitingPricePerMile].double as Double?{
            self.waitingPricePerMinute = _waitingPricePerMile
        }

        if let _time = json[kTime].string as String?{
            self.time = _time
        }else if let _time = json[kTime].double as Double?{
            self.time = "\(_time)"
        }

        if let _distance = json[kDistance].string as String?{
            self.distance = _distance
        }else if let _distance = json[kDistance].double as Double?{
            self.distance = "\(_distance)"
        }


        if let _cancellationFee = json[kCancellationFee].string as String?{
            self.cancellationFee = Double(_cancellationFee) ?? 0.0
        }else if let _cancellationFee = json[kCancellationFee].double as Double?{
            self.cancellationFee = _cancellationFee
        }
        if let _image = json[kImage].string as String?{
            self.image = _image
        }
        if let _maxSpeed = json[kMaxSpeed].string as String?{
            self.maxSpeed = Double(_maxSpeed) ?? 0.0
        }else if let _maxSpeed = json[kMaxSpeed].double as Double?{
            self.maxSpeed = _maxSpeed
        }

        if let _rideTimeFair = json[kRideFairPerMinute].string as String?{
            self.rideFairPerMinute = Double(_rideTimeFair) ?? 0.0
        }else if let _rideTimeFair = json[kRideFairPerMinute].double as Double?{
            self.rideFairPerMinute = _rideTimeFair
        }


        //nearest Car location
        if let _lat = json[kNearestCarLatitude].string as String?{
            self.nearestCarLatitude = Double(_lat) ?? 0.0
        }else if let _lat = json[kNearestCarLatitude].double as Double?{
            self.nearestCarLatitude = _lat
        }

        if let _longitude = json[kNearestCarLongitude].string as String?{
            self.nearestCarLongitude = Double(_longitude) ?? 0.0
        }else if let _longitude = json[kNearestCarLongitude].double as Double?{
            self.nearestCarLongitude = _longitude
        }
        super.init()
    }

}

class CarCategoryParser: NSObject {
    let kResponseMessage = "message"
    let kResponseCode = "code"
    let kCategories = "result"

    var responseMessage = ""
    var responseCode: Int = 0
    var categories = [CarCategory]()
    override init() {
        super.init()
    }

    var errorCode : ErrorCode = .failure
    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }

        if let _responseMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _responseMessage
        }
        
        if let _categoryArray = json[kCategories].arrayObject as? [[String:AnyObject]]{
            self.categories.removeAll()
            for _cate in _categoryArray{
                let cate = CarCategory(dictionary: _cate)
                self.categories.append(cate)
            }
        }
        super.init()
    }

}






